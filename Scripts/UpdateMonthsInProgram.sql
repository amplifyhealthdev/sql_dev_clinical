
begin transaction

update stdbilling
set yearxmonthx=(yearx*100)+monthx
where yearxmonthx is null

update a
set a.monthsInProgram=0
from
stdbilling a
join
(
select ptid,MIN(yearxmonthx) as minYrMnth
from stdbilling
group by ptID
) b
on
a.ptID=b.ptID and
a.yearxmonthx=b.minYrMnth

commit


create table #ttab1
(
ptId uniqueidentifier,
yearxmonthx int,
monthsInProgram int
)
create table #ttab2
(
ptId uniqueidentifier,
yearxmonthx int,
monthsInProgram int,
minYrMn int
)
create clustered index c1_ttab1 on #ttab1(ptID,yearxmonthx)
create clustered index c1_ttab1 on #ttab2(ptID,yearxmonthx)
create index i1_ttab2 on #ttab2(ptID,minYrMn)


while exists(select top 1 1 from stdbilling where monthsInProgram is null)
begin
	insert into #ttab1
	(ptID,yearxmonthx)
	select
	ptID,max(yearxmonthx)
	from stdbilling
	where monthsInProgram is not null
	group by ptID
		
	update a
	set a.monthsInProgram=b.monthsInProgram
	from
	#ttab1 a
	join stdbilling b
	on a.ptID=b.ptID and a.yearxmonthx=b.yearxmonthx
	
	insert into #ttab2
	(ptID,yearxmonthx,monthsInProgram,minYrMn)	
	select u.ptID,u.yearxmonthx,u.monthsInProgram,min(v.yearxmonthx) as minYrMn
	from
	#ttab1 u
	join stdbilling v
	on u.ptID=v.ptID and u.yearxmonthx<v.yearxmonthx
	group by u.ptId,u.yearxmonthx,u.monthsInProgram
	
	
	update a
	set a.monthsInProgram=b.monthsInProgram+1
	from
	stdbilling a
	join #ttab2 b
	on a.ptID=b.ptID and a.yearxmonthx=b.minYrMn
	
	delete from #ttab1
	delete from #ttab2
end



/*
drop table #ttab1
drop table #ttab2
*/


 
 
 
 



begin transaction
update a
set a.monthsInProgram=0
from
stdbilling a
join
(
select ptid,MIN(yearxmonthx) as minYrMnth
from stdbilling
group by ptID
) b
on
a.ptID=b.ptID and
a.yearxmonthx=b.minYrMnth