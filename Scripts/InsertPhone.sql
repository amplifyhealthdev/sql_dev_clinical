
insert into ptphone
(ptID,phonenumber)
select
a.personId,b.attvalue
from
metaDB.dbo.PersonXidentifier a
join metaDB.dbo.MattributeSetValue b
on a.identifier=b.identifier
and b.attribute='phone'
left join ptphone c
on a.PersonID=c.ptID and
b.attvalue=c.phonenumber
where
c.ptID is null
