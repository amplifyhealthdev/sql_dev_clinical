

begin transaction
	update a
	set a.ahptID=b.personID
	from patients a
	join metaDB.dbo.ptkeyset b
	on a.[Patient ID]=b.key1 
	where
	b.sourceSubtype='Enterprise' and b.sourceType='EHR' and
	a.ahPtID!=b.personId
	
	
	update a
	set a.ahptID=b.personID
	from patients a
	join metaDB.dbo.ptkeyset b
	on a.[Patient ID]=b.key1 
	where
	b.sourceSubtype='Enterprise' and b.sourceType='EHR' and
	a.ahPtID is null
	
	update a
	set a.ahptid=null
	from patients a
	left join metaDB.dbo.ptkeyset b
	on a.[Patient ID]=b.key1 and b.sourceSubtype='Enterprise' and b.sourceType='EHR'
	where
	b.personId is null
	

	
	
	update a
	set a.ahptid=b.ahptid
	from apptsProvider a
	join patients b
	on a.[Patient ID]=b.[Patient ID]
	where
	a.ahPtID!=b.ahPtID
	or b.ahPtID is null
	
	
	update a
	set a.ahptid=b.ahptid
	from
	diagCodes a
	join patients b
	on a.[Patient ID]=b.[Patient ID]
	where a.ahPtID!=b.ahPtID
	or b.ahPtID is null
	
	
	update a
	set a.ahptid=b.ahptid
	from
	problemList a
	join patients b
	on a.[Patient ID]=b.[Patient ID]
	where a.ahPtID!=b.ahPtID
	or b.ahPtID is null
	
	
	
	update a
	set a.ahptid=b.ahptid
	from
	procCodeAnalysis a
	join patients b
	on a.[Patient ID]=b.[Patient ID]
	where a.ahPtID!=b.ahPtID
	or b.ahPtID is null
	
	
	update a
	set a.ahptid=b.ahptid
	from
	referrals a
	join patients b
	on a.[Patient ID]=b.[Patient ID]
	where a.ahPtID!=b.ahPtID
	or b.ahPtID is null
	
	
	update a
	set a.ahptid=b.ahptid
	from
	vitals a
	join patients b
	on a.[Patient ID]=b.[Patient ID]
	where a.ahPtID!=b.ahPtID
	or b.ahPtID is null
	
	
	update a
	set a.ahptid=b.ahptid
	from
	immunizations a
	join patients b
	on a.[Patient Account No]=b.[Patient ID]
	where a.ahPtID!=b.ahPtID
	or b.ahPtID is null
	
	
	
	begin transaction 
	update a
	set a.ahptid=b.ahptid
	from labs a
	join patients b
	on a.[Patient Account Number]=b.[Patient Account Number]
	where
	a.ahPtID!=b.ahPtID
	or b.ahPtID is null
	
	commit
	
	select top 10 
[Encounter ID],ahencid,[Patient ID],ahPtID
 from apptsProvider 

select * from metaDB.dbo.ptkeyset
where key1='9131'
and sourceSubtype='enterprise'

begin transaction

	
		update a
		set a.ptID=b.ahptid
		from clinical2.dbo.ptencounter a
		join apptsProvider b
		on a.encounterID=b.ahEncID
		where
		a.ptID!=b.ahPtID
		
		update a
		set a.ptID=b.ahptid
		from clinical2.dbo.ptencounter a
		join labs b
		on a.encounterID=b.ahEncID
		where
		a.ptID!=b.ahPtID
		
		update a
		set a.ptID=b.ahptid
		from clinical2.dbo.ptencounter a
		join immunizations b
		on a.encounterID=b.ahEncID
		where
		a.ptID!=b.ahPtID
		
		update a
		set a.ptID=b.ahptid
		from clinical2.dbo.ptlab a
		join labs b
		on a.encounterID=b.ahEncID
		where
		a.ptID!=b.ahPtID
				
		update a
		set a.ptID=b.ahptid
		from clinical2.dbo.ptimmunization a
		join immunizations b
		on a.encounterID=b.ahEncID
		where
		a.ptID!=b.ahPtID
				
		update a
		set a.ptID=b.ahptid
		from clinical2.dbo.ptreferral a
		join referrals b
		on a.encounterID=b.ahEncID
		where
		a.ptID!=b.ahPtID
				
		update a
		set a.ptID=b.ahptid
		from clinical2.dbo.ptvital a
		join vitals b
		on a.encounterID=b.ahEncID
		where
		a.ptID!=b.ahPtID
		
				
		
		commit
		