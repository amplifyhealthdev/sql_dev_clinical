declare
	@fileset_id uniqueidentifier,
	@sourceType varchar(256),
	@sourceSubtype varchar(256)
	
	

begin transaction
	select top 1 @fileset_id=fileset_id from apptsProviderCurrent 
	
	select @sourceType=sourceType,@sourceSubtype=sourceSubtype
	from metaDB.dbo.filesetDetail
	where fileset_id=@fileset_id
	
/*
create table srcencmap2
(
encID uniqueidentifier,
srcencId varchar(40),
src_system varchar(256),
loadId int,
create_date datetime default getdate(),
obsolete_date datetime,
audit_id uniqueidentifier not null default newid()
)
*/


insert into clinical.dbo.srcencmap2
(
encID,srcencID,sourceType,sourceSubtype,fileset_id
)
select
newid(),a.[Encounter ID],@sourceType,@sourceSubtype,@fileset_id
from
(select distinct [Encounter ID] from apptsproviderCurrent
where 
[Encounter ID]!='0'
and ahptId is not null) a
left join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
where
b.srcencId is null

update a
set a.ahencID=b.encID
from
apptsproviderCurrent a
join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

insert into clinical.dbo.srcencmap2
(
encID,srcencID,sourceType,sourceSubtype,fileset_id
)
select
newid(),a.[Encounter ID],@sourceType,@sourceSubtype,@fileset_id
from
(select distinct [Encounter ID] from diagCodesCurrent where [Encounter ID]!='0') a
left join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
where
b.srcencId is null


update a
set a.ahencID=b.encID
from
diagCodesCurrent a
join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

insert into clinical.dbo.srcencmap2
(
encID,srcencID,sourceType,sourceSubtype,fileset_id
)
select
newid(),a.[Encounter ID],@sourceType,@sourceSubtype,@fileset_id
from
(select distinct [Encounter ID] from immunizationsCurrent where [Encounter ID]!='0') a
left join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
where
b.srcencId is null


update a
set a.ahencID=b.encID
from
immunizationsCurrent a
join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

insert into clinical.dbo.srcencmap2
(
encID,srcencID,sourceType,sourceSubtype,fileset_id
)
select
newid(),a.[Encounter ID],@sourceType,@sourceSubtype,@fileset_id
from
(select distinct [Encounter ID] from labsCurrent where [Encounter ID]!='0') a
left join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
where
b.srcencId is null


update a
set a.ahencID=b.encID
from
labsCurrent a
join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

insert into clinical.dbo.srcencmap2
(
encID,srcencID,sourceType,sourceSubtype,fileset_id
)
select
newid(),a.[Encounter ID],@sourceType,@sourceSubtype,@fileset_id
from
(select distinct [Encounter ID] from problemListCurrent where [Encounter ID]!='0') a
left join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
where
b.srcencId is null


update a
set a.ahencID=b.encID
from
problemListCurrent a
join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype


insert into clinical.dbo.srcencmap2
(
encID,srcencID,sourceType,sourceSubtype,fileset_id
)
select
newid(),isnull(a.[Referral Encounter ID],0),@sourceType,@sourceSubtype,@fileset_id
from
(select distinct [Referral Encounter ID] from referralsCurrent where [Referral Encounter ID]!='0') a
left join clinical.dbo.srcencmap2 b
on isnull(a.[Referral Encounter ID],0)=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
where
b.srcencId is null


update a
set a.ahencID=b.encID
from
referralsCurrent a
join clinical.dbo.srcencmap2 b
on isnull(a.[Referral Encounter ID],0)=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype


insert into clinical.dbo.srcencmap2
(
encID,srcencID,sourceType,sourceSubtype,fileset_id
)
select
newid(),a.[Encounter ID],@sourceType,@sourceSubtype,@fileset_id
from
(select distinct [Encounter ID] from vitalsCurrent where [Encounter ID]!='0') a
left join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
where
b.srcencId is null


update a
set a.ahencID=b.encID
from
VitalsCurrent a
join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype


insert into clinical.dbo.srcencmap2
(
encID,srcencID,sourceType,sourceSubtype,fileset_id
)
select
newid(),a.[Encounter ID],@sourceType,@sourceSubtype,@fileset_id
from
(select distinct [Encounter ID] from procCodeAnalysisCurrent where [Encounter ID]!='0') a
left join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
where
b.srcencId is null


update a
set a.ahencID=b.encID
from
procCodeAnalysisCurrent a
join clinical.dbo.srcencmap2 b
on a.[Encounter ID]=b.srcencID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

/*
insert into clinical.dbo.srcencmap2
(
encID,srcencID,src_system,load_id
)
select
b.encID,a.[Encounter ID],'apptsProvider',@loadID
from
apptsProviderCurrent a
join Clinical.dbo.srcencmap b
on a.[Encounter ID]=b.srcencID_monavie
left join encounterXref c
on a.[Encounter ID]=c.srcencounterid
where
c.encounterID is null


insert into encounterXref
(srcencounterID,encounterID,src_tbl,load_id)
select distinct a.[Encounter ID],b.encID,'diagCodesCurrent',@loadID
from diagCodesCurrent a
join Clinical.dbo.srcencmap b
on a.[Encounter ID]=b.srcencID_monavie
join Clinical.dbo.ptencounter d
on b.encID=d.encounterID
left join encounterXref c
on b.encID=c.encounterID
where  c.encounterID is null
and isnull(a.[Encounter ID],0)!=0

insert into encounterXref
(srcencounterID,encounterID,src_tbl,load_id)
select distinct a.[Encounter ID],b.encID,'immunizationsCurrent',@loadID
from immunizationsCurrent a
join Clinical.dbo.srcencmap b
on a.[Encounter ID]=b.srcencID_monavie
join Clinical.dbo.ptencounter d
on b.encID=d.encounterID
left join encounterXref c
on b.encID=c.encounterID
where c.encounterID is null
and isnull(a.[Encounter ID],0)!=0

insert into encounterXref
(srcencounterID,encounterID,src_tbl,load_id)
select distinct a.[Encounter ID],b.encID,'labsCurrent',@loadID
from labsCurrent a
join Clinical.dbo.srcencmap b
on a.[Encounter ID]=b.srcencID_monavie
join Clinical.dbo.ptencounter d
on b.encID=d.encounterID
left join encounterXref c
on b.encID=c.encounterID
where c.encounterID is null
and isnull(a.[Encounter ID],0)!=0


insert into encounterXref
(srcencounterID,encounterID,src_tbl,load_id)
select distinct a.[Encounter ID],b.encID,'problemListCurrent',@loadID
from problemListCurrent a
join Clinical.dbo.srcencmap b
on a.[Encounter ID]=b.srcencID_monavie
join Clinical.dbo.ptencounter d
on b.encID=d.encounterID
left join encounterXref c
on b.encID=c.encounterID
where c.encounterID is null
and isnull(a.[Encounter ID],0)!=0


insert into encounterXref
(srcencounterID,encounterID,src_tbl,load_id)
select distinct a.[Referral Encounter ID],b.encID,'referralsCurrent',@loadID
from referralsCurrent a
join Clinical.dbo.srcencmap b
on a.[Referral Encounter ID]=b.srcencID_monavie
join Clinical.dbo.ptencounter d
on b.encID=d.encounterID
left join encounterXref c
on b.encID=c.encounterID
where c.encounterID is null
and isnull(a.[Referral Encounter ID],0)!=0


insert into encounterXref
(srcencounterID,encounterID,src_tbl,load_id)
select distinct a.[Encounter ID],b.encID,'vitalsCurrent',@loadID
from vitalsCurrent a
join Clinical.dbo.srcencmap b
on a.[Encounter ID]=b.srcencID_monavie
join Clinical.dbo.ptencounter d
on b.encID=d.encounterID
left join encounterXref c
on b.encID=c.encounterID
where c.encounterID is null
and isnull(a.[Encounter ID],0)!=0

select top 100 [Encounter ID],ahencid from diagcodescurrent


select * from clinical.dbo.srcencmap2
where fileset_id='94BC5FD9-0368-4A2C-9CB6-C4CD2429B597'

rollback


*/

--commit

