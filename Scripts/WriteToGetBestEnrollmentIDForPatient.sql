/*
This script mimics the function GetBestEnrollmentIDForPatient in EMMA
The function takes patient_id as input parameter and spits out the best enrollment ID.

the script writes all the IDs for the patient in the descending order of period_end
*/

use staging
go
drop table staging.dbo.GetBestEnrollmentIDForPatient
go
SET QUOTED_IDENTIFIER ON

	SELECT ep.patient_id,
		   ep.enrollment_period_id,
		   ep.period_start,
		   ep.period_end,
		   row_number() over (partition by patient_id order by ISNULL(ep.period_end , '2999-01-01') DESC, ep.period_start DESC, ep.enrollment_period_id DESC) as rownum
	into staging.dbo.GetBestEnrollmentIDForPatient
	FROM    emma.dbo.enrollment_period AS ep

