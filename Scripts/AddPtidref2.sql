insert into clinical.dbo.ptidref2
(ptid)
select
a.ptid 
from clinical.dbo.patient a
left join clinical.dbo.ptidref2 b
on a.ptid=b.ptid
where b.ptid is null
