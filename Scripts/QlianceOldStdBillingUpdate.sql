
declare
	@monthx int,
	@yearx int,
	@sdt datetime,
	@edt datetime,
	@lmonthx int,
	@lyearx int,
    @yearxmonthx int,
    @unknown uniqueidentifier
	
select @lyearx=DATEPART(yyyy,getdate())
select @lmonthx=DATEPART(mm,getdate())

select @unknown=employerId from clinical.dbo.employer where employerName='unknown'

if @lmonthx=0
	select @lmonthx=12,@lyearx=@lyearx-1
	
select @yearx=2010
while @yearx<=2014
begin
	select @monthx=1
	while @monthx<=12
	begin
		select @sdt=CAST(cast(@yearx as varchar(4))+'/'+right('00'+cast(@monthx as varchar),2)+'/01' as DATE)
		select @edt=dateadd(ss,-1,DATEADD(mm,1,@sdt))
		
		select @yearxmonthx=(@yearx*100)+@monthx

		print @monthx
		print @yearx
		print @sdt
		print @edt
		print '-------'
               
		/*                                                 
		insert into clinical.dbo.stdbilling
		(ptID,monthx,yearx,yearxmonthx,employerID,loadID,billed)
		select
		a.mbrId,@monthx,@yearx,@yearxmonthx,a.ahemployer_id,1,1
		from
		staging.dbo.emmastage a
		join staging.dbo.staging_employer a1
		on a.ahemployer_id=a1.ahemployer_id
		left join 
		(select mbrID from staging.dbo.emmastage
		group by mbrID 
		having COUNT(*)>1
		) b on a.mbrid=b.mbrID
		left join clinical.dbo.stdbilling c
		on a.mbrID=c.ptID and c.yearxmonthx=@yearxmonthx
		where
		CAST(employee_elibility_start as date)<=@edt
		and
		case 
			when employee_elibility_end!='' then cast(employee_elibility_end as datetime) 
			else 
				case deleted_date
					when '' then @edt
					else CAST(deleted_date as DATEtime) 
					end 
			end >=@sdt
		
		and
		b.mbrID is null
		and 
		c.ptID is null
		*/
		
		insert into clinical.dbo.stdbilling
		(ptID,monthx,yearx,yearxmonthx,billed,employerId,loadID)
		select
		distinct
		b.mbrID,@monthx,@yearx,@yearxmonthx,1,coalesce(d.ahemployer_id,b.ahemployer_id,@unknown),1
		from
		emma.dbo.membership a
		join emma.dbo.member a1
		on a.membership_id=a1.membership_id
		join Staging.dbo.ql_patient_demographic b
		on a1.patient_id=b.individual_id
		left join clinical.dbo.stdbilling c
		on b.mbrID=c.ptID and c.monthx=@monthx and c.yearx=@yearx
		left join staging.dbo.ql_employer d
		on a.charge_to_id=d.individual_id
		left join staging.dbo.stdbilling_dups f
		on b.mbrID=f.ptid and f.yearxmonthx=@yearxmonthx
		where
		a.start_date<@edt and
		(a.expiry_date is null or a.expiry_date>=@sdt)
		and c.ptId is null
        and b.obsolete_date is null
        and f.ptid is null
        and isnull(d.ahemployer_id,b.ahemployer_id)!='2D96F605-E237-4366-B3A6-CB89C1D07C16'
        
        insert into clinical.dbo.stdbilling
		(ptID,monthx,yearx,yearxmonthx,billed,employerId,loadID)
		select
		distinct
		b.mbrID,@monthx,@yearx,@yearxmonthx,1,coalesce(d.ahemployer_id,b.ahemployer_id,@unknown),1
		from
		emma.dbo.membership a
		join emma.dbo.member a1
		on a.membership_id=a1.membership_id
		join Staging.dbo.ql_patient_demographic b
		on a1.patient_id=b.individual_id
		left join clinical.dbo.stdbilling c
		on b.mbrID=c.ptID and c.monthx=@monthx and c.yearx=@yearx
		left join staging.dbo.ql_employer d
		on a.charge_to_id=d.individual_id
		left join staging.dbo.stdbilling_dups f
		on b.mbrID=f.ptid and f.yearxmonthx=@yearxmonthx
		join emma.dbo.membership_status g
		on a.membership_status_id=g.membership_status_id
		where
		a.start_date<@edt and
		g.membership_status_name!='Cancelled' and (a.expiry_date is null or a.expiry_date>=@sdt)
		and c.ptId is null
        and b.obsolete_date is null
        and f.ptid is null
        and isnull(d.ahemployer_id,b.ahemployer_id)='2D96F605-E237-4366-B3A6-CB89C1D07C16'
 
        insert into clinical.dbo.stdbilling
		(ptID,monthx,yearx,yearxmonthx,billed,employerId,loadID)
		select
		distinct
		b.mbrID,@monthx,@yearx,@yearxmonthx,1,coalesce(d.ahemployer_id,b.ahemployer_id,@unknown),1
		from
		emma.dbo.membership a
		join emma.dbo.member a1
		on a.membership_id=a1.membership_id
		join Staging.dbo.ql_patient_demographic b
		on a1.patient_id=b.individual_id
		left join clinical.dbo.stdbilling c
		on b.mbrID=c.ptID and c.monthx=@monthx and c.yearx=@yearx
		left join staging.dbo.ql_employer d
		on a.charge_to_id=d.individual_id
		left join staging.dbo.stdbilling_dups f
		on b.mbrID=f.ptid and f.yearxmonthx=@yearxmonthx
		join emma.dbo.membership_status g
		on a.membership_status_id=g.membership_status_id
		where
		a.start_date<@edt and
		g.membership_status_name='Cancelled' and coalesce(a.expiry_date,a.last_charge_date,a.start_date)>=@sdt
		and c.ptId is null
        and b.obsolete_date is null
        and f.ptid is null
        and isnull(d.ahemployer_id,b.ahemployer_id)='2D96F605-E237-4366-B3A6-CB89C1D07C16'
		            
      				
		if @yearx=@lyearx and @monthx=@lmonthx
			select @monthx=14
		else
			select @monthx=@monthx+1
	end
	select @yearx=@yearx+1
end



update a
set a.pcpID=b.pcpID
from 
clinical.dbo.stdbilling a
join clinical.dbo.patient b
on a.ptid=b.ptid
where
a.pcpid is null and b.pcpid is not null



update a
set a.insurancePlan=c.insurance_plan_name
from
clinical.dbo.stdbilling a
join staging.dbo.ql_patient_demographic b
on a.ptID=b.mbrID  
join emma.dbo.insurance_plan c
on b.insurance_plan_id=c.insurance_plan_id
where
(a.insurancePlan is null or
a.insurancePlan='Unknown')
and b.obsolete_date is null

update a
set a.insuranceCompany=c.insurance_company_name
from
clinical.dbo.stdbilling a
join Staging.dbo.ql_patient_demographic b
on a.ptID=b.mbrID
join emma.dbo.insurance_company c
on b.insurance_company_id=c.individual_id
where 
(
a.insuranceCompany is null or a.insuranceCompany='Unknown')
and b.obsolete_date is null

update a
set a.companyOffice=c.office_name
from
clinical.dbo.stdbilling a
join Staging.dbo.ql_patient_demographic b
on a.ptID=b.mbrID
join emma.dbo.company_office c
on b.company_office_id=c.individual_id
where
(a.companyOffice is null  or
a.companyOffice='Unknown')
and b.obsolete_date is null

update clinical.dbo.stdbilling
set companyOffice='Unknown'
where companyOffice is null

update clinical.dbo.stdbilling
set insuranceCompany='Unknown'
where insuranceCompany is null

update clinical.dbo.stdbilling
set insurancePlan='Unknown'
where insurancePlan is null




		
		
		
