
select a.ptid,a.gender,c.sex,c.individual_id
from clinical.dbo.patient a
join staging.dbo.ql_patient_demographic b
on a.ptid=b.mbrID and b.obsolete_date is null
join emma.dbo.individual c
on b.individual_id=c.individual_id
where
a.gender!=c.sex
and a.gender!='U'

select a.ptid,a.birthDate,c.dob,c.individual_id
from clinical.dbo.patient a
join staging.dbo.ql_patient_demographic b
on a.ptid=b.mbrID and b.obsolete_date is null
join emma.dbo.individual c
on b.individual_id=c.individual_id
where
a.birthDate!=c.dob


select a.ptid,a.gender,b.sex,b.individual_id,c.sex,c.individual_id
from clinical.dbo.patient a
join staging.dbo.ql_patient_demographic b
on a.ptid=b.mbrID and b.obsolete_date is null
join staging.dbo.ql_patient_demographic c
on a.ptid=c.mbrID and c.obsolete_date is null and b.sex!=c.sex
where
a.gender!=c.sex

select a.ptid,a.gender,b.dob,b.individual_id,c.dob,c.individual_id
from clinical.dbo.patient a
join staging.dbo.ql_patient_demographic b
on a.ptid=b.mbrID and b.obsolete_date is null
join staging.dbo.ql_patient_demographic c
on a.ptid=c.mbrID and c.obsolete_date is null and b.sex!=c.sex
where
a.birthDate!=c.dob