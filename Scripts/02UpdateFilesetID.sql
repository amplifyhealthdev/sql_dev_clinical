declare
	@fileset_id uniqueidentifier
	
	select 	@fileset_id='C5453C68-2E59-49F4-BC26-5A44EE064DE1'
	
update demoRendPrvdCurrent set fileset_id=@fileset_id
update demoPCPCurrent set fileset_id=@fileset_id
update apptsProviderCurrent set fileset_id=@fileset_id
update apptCodesCurrent set fileset_id=@fileset_id
update vitalsCurrent set fileset_id=@fileset_id
update resProviderCurrent set fileset_id=@fileset_id
update referralsCurrent set fileset_id=@fileset_id
update procCodeAnalysisCurrent set fileset_id=@fileset_id
update problemListCurrent set fileset_id=@fileset_id
update practiceProvidersCurrent set fileset_id=@fileset_id
update labsCurrent set fileset_id=@fileset_id
update insurancesCurrent set fileset_id=@fileset_id
update immunizationsCurrent set fileset_id=@fileset_id
update patientsCurrent set fileset_id=@fileset_id
update diagCodesCurrent set fileset_id=@fileset_id