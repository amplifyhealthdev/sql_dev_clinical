
select
a.individual_id as patient_id,
case when isnull(b.enrollment_period_id,0)=0 then 'Inactive' else 'Active' end as patient_status
from
emma.dbo.patient a
left join staging.dbo.[GetBestEnrollmentIDForPatient] b
on a.individual_id=b.patient_id and b.rownum=1 and ISNULL(b.period_end , '2999-01-01')>'2015-09-30'
