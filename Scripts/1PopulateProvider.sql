
	begin transaction
	declare 
		@fileset_id uniqueidentifier,
		@sourceType varchar(256),
		@sourceSubtype varchar(256)
		
	
	select top 1 @fileset_id=fileset_id from demoRendPrvdCurrent
	
	select @sourceType=sourceType,@sourceSubtype=sourceSubtype from metadb.dbo.filesetDetail where fileset_id=@fileset_id
	
	update a
	set a.ahprovID=b.provID
	from
	demoRendPrvdCurrent a
	join clinical.dbo.srcprovmap2 b
	on a.[Demographics Rendering Provider ID]=b.srcprovID
	where 
	b.sourceType=@sourceType and sourceSubtype=@sourceSubtype

	
	
	update a
	set a.ahprovID=c.ahprovID
	from
	demoRendPrvdCurrent a
	join
	(
	select dprovID,NEWID() as ahprovID
	from
	(
	select distinct [Demographics Rendering Provider ID] as dprovID
	from demoRendPrvdCurrent
	where ahProvID is null and [Demographics Rendering Provider First Name] not in ('Data','test')
	and LEN([Demographics Rendering Provider ID])>0
	) b
	) c
	on a.[Demographics Rendering Provider ID]=c.dprovID
	where a.ahprovID is null
	
	insert into clinical.dbo.srcprovmap2 
	(provID,srcprovID,sourceType,sourceSubtype)
	select
	a.ahprovId,a.[Demographics Rendering Provider ID],@sourceType,@sourceSubtype
	from
	demoRendPrvdCurrent a
	left join clinical.dbo.srcprovmap2 b
	on a.[Demographics Rendering Provider ID]=b.srcprovID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype
	where
	a.ahprovID is not null and b.provId is null	
		
	insert into clinical.dbo.provider
	(
	provID,provFirst,provLast,provMiddle,provTitle,provSuffix,specialtyType,
	address1,address2,city,zip,state,officePhone,fax,UPIN,NPI,DEANumber,fileset_id)
	select DISTINCT s.ahprovid, rtrim(ltrim(s.[Demographics Rendering Provider First Name])), 
	                             rtrim(ltrim(s.[Demographics Rendering Provider Last Name])), 
	                             rtrim(ltrim(s.[Demographics Rendering Provider Middle Initial])), 
	                             rtrim(ltrim(s.[Demographics Rendering Provider Prefix])), 
	                             rtrim(ltrim(s.[Demographics Rendering Provider Suffix])),  
	                             sp.specialtyCode, 
	                             rtrim(ltrim(s.[Demographics Rendering Provider Address Line 1])),
	                             rtrim(ltrim(s.[Demographics Rendering Provider Address Line 2])), 
	                             rtrim(ltrim(s.[Demographics Rendering Provider City])),
	                             substring(rtrim(ltrim(s.[Demographics Rendering Provider Zip Code])),1,5), 
	                             substring(rtrim(ltrim(s.[Demographics Rendering Provider State])),1,2), 
	                             refdb.dbo.phoneInt(s.[Demographics Rendering Provider Phone Number]),            
	                             refdb.dbo.phoneInt(s.[Demographics Rendering Provider Fax No]), 
	                             rtrim(ltrim(s.[Demographics Rendering Provider UPIN])), 
	                             left(rtrim(ltrim(s.[Demographics Rendering Provider NPI])),10), 
	                             rtrim(ltrim(s.[Demographics Rendering Provider DEA No])),
	                             s.fileset_id
	from demoRendPrvdCurrent s
	left join refdb.dbo.ahspecialties sp on s.[Demographics Rendering Provider Speciality]= sp.Description
	left join clinical.dbo.provider c
	on s.ahprovID=c.provID
	where
	c.provID is null
	and s.ahprovID is not null
	
		

commit
