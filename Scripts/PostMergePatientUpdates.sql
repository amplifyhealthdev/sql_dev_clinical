
begin transaction
	insert into DeletedPatient
	select distinct a.*,'Merged with '+CAST(c.ptId as varchar(40)) as DeletedReason,c.ptId as newPtID
	from
	patient a,metaDB.dbo.PersonXidentifier b,patient c
	where
	a.ptID=b.PersonID and b.newPersonId=c.ptId
	and a.ptID!=c.ptId

	update a
	set a.ptID=b.newptID
	from
	ptLab a ,DeletedPatient b
	where a.ptId=b.ptID
	
	update a
	set a.ptID=b.newptID
	from
	ptdiagnosis a,DeletedPatient b
	where a.ptId=b.ptID

	update a
	set a.ptID=b.newptID
	from
	ptimmunization a,DeletedPatient b
	where a.ptId=b.ptID
		
	update a
	set a.ptID=b.newptID
	from
	ptallergy a,DeletedPatient b
	where a.ptId=b.ptID

	update a
	set a.ptID=b.newptID
	from
	ptemployer a,DeletedPatient b
	where a.ptId=b.ptID

	update a
	set a.ptID=b.newptID
	from
	ptdisease a,DeletedPatient b
	where a.ptId=b.ptID

	update a
	set a.ptID=b.newptID
	from
	ptvital a,DeletedPatient b
	where a.ptId=b.ptID
	
	update a
	set a.ptID=b.newptID
	from
	ptencounter a,DeletedPatient b
	where a.ptId=b.ptID
	
	update a
	set a.ptID=b.newptID
	from
	ptreferral a,DeletedPatient b
	where a.ptId=b.ptID
	
	delete a
	from
	ptphone a,DeletedPatient b
	where a.ptId=b.ptID
	
	delete a
	from
	ptaddress a,DeletedPatient b
	where a.ptId=b.ptID
	
	delete a
	from
	patient a,DeletedPatient b
	where a.ptId=b.ptID
	
	
commit
	