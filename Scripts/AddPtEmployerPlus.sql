--ptemployer
--qliance claims employer data
use metaDB
go
insert into clinical.dbo.ptemployer
(ptid,employerId)
select distinct
a.personID,
b.employerId
from
personXidentifier a
join sourceSubtypeEmployer b
on a.sourceSubtype=b.sourceSubtype
left join clinical.dbo.ptemployer c
on a.personID=c.ptid
join clinical.dbo.patient d
on a.personID=d.ptid
where
c.ptid is null
--
--ptidref2
insert into clinical.dbo.ptidref2
(ptid)
select
a.ptid 
from clinical.dbo.patient a
left join clinical.dbo.ptidref2 b
on a.ptid=b.ptid
where b.ptid is null
--
--
exec clinical.dbo.uspUpdateRptGroup
--
--