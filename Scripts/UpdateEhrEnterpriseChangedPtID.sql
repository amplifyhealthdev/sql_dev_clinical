
begin transaction
	update a
	set a.ahptID=c.PersonID
	from
	apptsProvider a
	join metaDB.dbo.PersonXidentifierRelinked b
	on a.ahPtID=b.PersonID and b.sourceType='EHR' and b.sourceSubtype='Enterprise'
	join metaDB.dbo.PersonXidentifier c
	on b.newPersonID=c.PersonID
	join metaDB.dbo.MattributeSetValue d
	on b.identifier=d.identifier and d.attribute='enterprise_ehrid'
	where
	a.[Patient ID] = d.attvalue
	
	update a
	set a.ahptID=b.ahptId
	from diagCodes a
	join apptsProvider b
	on a.ahEncID=b.ahEncID
	where
	a.ahPtID!=b.ahPtID

	update a
	set a.ahptID=b.ahptId
	from immunizations a
	join apptsProvider b
	on a.ahEncID=b.ahEncID
	where
	a.ahPtID!=b.ahPtID
	
	update a
	set a.ahptID=b.ahptId
	from labs a
	join apptsProvider b
	on a.ahEncID=b.ahEncID
	where
	a.ahPtID!=b.ahPtID
	
	update a
	set a.ahptID=b.ahptId
	from problemList a
	join apptsProvider b
	on a.ahEncID=b.ahEncID
	where
	a.ahPtID!=b.ahPtID
	
	update a
	set a.ahptID=b.ahptId
	from procCodeAnalysis a
	join apptsProvider b
	on a.ahEncID=b.ahEncID
	where
	a.ahPtID!=b.ahPtID	
	
	update a
	set a.ahptID=b.ahptId
	from referrals a
	join apptsProvider b
	on a.ahEncID=b.ahEncID
	where
	a.ahPtID!=b.ahPtID	
	
	update a
	set a.ahptID=b.ahptId
	from vitals a
	join apptsProvider b
	on a.ahEncID=b.ahEncID
	where
	a.ahPtID!=b.ahPtID	
		
commit
		