declare
	@fileset_id uniqueidentifier,
	@sourceType varchar(256),
	@sourceSubtype varchar(256)
	
	

begin transaction
	select top 1 @fileset_id=fileset_id from apptsProviderCurrent 
	
	select @sourceType=sourceType,@sourceSubtype=sourceSubtype
	from metaDB.dbo.filesetDetail
	where fileset_id=@fileset_id
	
	
	if @sourceSubtype='Serigraph'
		update apptsProviderCurrent
		set ahprovID='4F3AE4D2-EE69-4C47-9C63-185E6ECC19D4'
	else
		update a
		set a.ahprovID=b.provID
		from apptsProviderCurrent a
		join  clinical.dbo.srcprovmap2 b
		on a.[Appointment Provider ID]=b.srcprovID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

		

	if @sourceSubtype='Serigraph'
		update labscurrent
		set ahprovID='4F3AE4D2-EE69-4C47-9C63-185E6ECC19D4'
	else			
		update a
		set a.ahprovID=b.provID
		from labscurrent a
		join clinical.dbo.srcprovmap2 b
		on a.[Appointment Provider ID]=b.srcprovID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

		
	update a
	set a.ahprovID=b.ProvID
	from
	practiceProvidersCurrent a
	join clinical.dbo.srcprovmap2 b
		on a.[Practice Provider ID]=b.srcprovID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

		
	
	if @sourceSubtype='Serigraph'
		update ProblemListCurrent
		set ahprovID='4F3AE4D2-EE69-4C47-9C63-185E6ECC19D4'
	else	
		update a
		set a.ahprovID=b.ProvID
		from
		problemListCurrent a
		join clinical.dbo.srcprovmap2 b
		on a.[Appointment Provider ID]=b.srcprovID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

		
	
	update a
	set a.ahprovID=b.ahprovID
	from immunizationsCurrent a
	join apptsProviderCurrent b
	on a.[Encounter ID]=b.[Encounter ID]
	

	update a
	set a.ahprovID=b.provID
	from referralsCurrent a
	left join clinical.dbo.srcprovmap2 b
	on a.[Referral Assigned Provider ID]=b.srcprovID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype

	update a
	set a.ahToprovID=b.provID
	from referralsCurrent a
	left join clinical.dbo.srcprovmap2 b
	on a.[Referral To Provider ID]=b.srcprovID and b.sourceType=@sourceType and b.sourceSubtype=@sourceSubtype


	update a
	set a.ahprovID=b.ahprovID
	from procCodeAnalysisCurrent a
	join apptsProviderCurrent b
	on a.[Encounter ID]=b.[Encounter ID]	

--commit
