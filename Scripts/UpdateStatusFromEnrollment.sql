begin transaction
declare
	@dt date

select @dt=dateadd(dd,0-datepart(dd,getdate()),getdate())

set rowcount 1000
/*
while exists(select top 1 1 from clinical.dbo.patient where ptStatus is not null)
begin
	update clinical.dbo.patient
	set ptstatus=null where ptStatus is not null
end
*/
while exists(select top 1 1 
			from clinical.dbo.patient a
			join staging.dbo.ql_patient_demographic b
			on a.ptid=b.mbrID and b.obsolete_date is null
			join staging.dbo.[GetBestEnrollmentIDForPatient] c
			on b.individual_id=c.patient_id
			join emma.dbo.individual d
			on b.individual_id=d.individual_id
			join emma.dbo.billable_status e
			on d.billable_status_id=e.billable_status_id
			where
			c.rownum=1 and ISNULL(c.period_end , '2999-01-01')>@dt
			and
			(
			a.ptStatus!='Active'
			or
			a.ptStatus is null
			)
			and 
			e.billable_status_name='Active'
			and
			d.status='A'
			)
begin
	update a
	set a.ptStatus='Active'
	from clinical.dbo.patient a
	join staging.dbo.ql_patient_demographic b
	on a.ptid=b.mbrID and b.obsolete_date is null
	join staging.dbo.[GetBestEnrollmentIDForPatient] c
	on b.individual_id=c.patient_id
	join emma.dbo.individual d
	on b.individual_id=d.individual_id
	join emma.dbo.billable_status e
	on d.billable_status_id=e.billable_status_id
	where
	c.rownum=1 and ISNULL(c.period_end , '2999-01-01')>@dt
	and
	(
	a.ptStatus!='Active'
	or
	a.ptStatus is null
	)
	and 
	e.billable_status_name='Active'
	and
	d.status='A'
end

while exists(select top  1 1 
from clinical.dbo.patient a
join staging.dbo.ql_patient_demographic b
on a.ptid=b.mbrID and b.obsolete_date is null
left join staging.dbo.[GetBestEnrollmentIDForPatient] c
on b.individual_id=c.patient_id
and
c.rownum=1 and ISNULL(c.period_end , '2999-01-01')>@dt
join emma.dbo.individual d
on b.individual_id=d.individual_id
left join emma.dbo.billable_status e
on d.billable_status_id=e.billable_status_id
where
(a.ptStatus!='Active'
or a.ptStatus is null)
and
(
a.ptStatus!='Inactive'
or
a.ptStatus is null
)
and
(
c.patient_id is null
or
(
e.billable_status_name is null
or 
e.billable_status_name !='Active'
)
or
(
d.status is null
or
d.status!='A'
)
)
begin
	update a
	set a.ptStatus='InActive'
	from clinical.dbo.patient a
	join staging.dbo.ql_patient_demographic b
	on a.ptid=b.mbrID and b.obsolete_date is null
	left join staging.dbo.[GetBestEnrollmentIDForPatient] c
	on b.individual_id=c.patient_id
	and
	c.rownum=1 and ISNULL(c.period_end , '2999-01-01')>@dt
	join emma.dbo.individual d
	on b.individual_id=d.individual_id
	left join emma.dbo.billable_status e
	on d.billable_status_id=e.billable_status_id
	where
	(a.ptStatus!='Active'
	or a.ptStatus is null)
	and
	(
	a.ptStatus!='Inactive'
	or
	a.ptStatus is null
	)
	and
	(
	c.patient_id is null
	or
	(
	e.billable_status_name is null
	or 
	e.billable_status_name !='Active'
	)
	or
	(
	d.status is null
	or
	d.status!='A'
	)
	)
end
--commit


--select ptstatus,count(*) from patient group by ptstatus
