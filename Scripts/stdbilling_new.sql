use Staging
go
create table enrollment_period_detail
(
patient_id int,
employer_id int,
enrolled_on datetime
)
create table #dt
(
dt datetime
)

declare
	@mindt datetime,
	@curdt datetime
	
select @mindt=min(period_start) from emma.dbo.enrollment_period

select @curdt=getdate()

while @mindt<=@curdt
begin
	insert into #dt
	(dt)
	values
	(@mindt)
	
	select @mindt=dateadd(dd,1,@mindt)
end


declare
	@curdt datetime
	

select @curdt=getdate()

insert into enrollment_period_detail
(patient_id,employer_id,enrolled_on)
select
distinct
a.patient_id,a.employer_id,b.dt
from
emma.dbo.enrollment_period a
join #dt b
on b.dt between a.period_start and isnull(a.period_end,@curdt)


create clustered index c1_dt on #dt(dt)


create clustered index c1_enrollment_period_detail
on enrollment_period_detail(patient_id,employer_id,enrolled_on)

alter table enrollment_period_detail
add
sflag bit,
eflag bit


update a
set a.sflag=1
from
enrollment_period_detail a
left join enrollment_period_detail b
on a.patient_id=b.patient_id and a.employer_id=b.employer_id
and b.enrolled_on=dateadd(d,-1,a.enrolled_on)
where
b.patient_id is null

update a
set a.eflag=1
from
enrollment_period_detail a
left join enrollment_period_detail b
on a.patient_id=b.patient_id and a.employer_id=b.employer_id
and b.enrolled_on=dateadd(d,1,a.enrolled_on)
where
b.patient_id is null


create table enrollment_period_collapsed
(
patient_id int,
employer_id int,
period_start datetime,
period_end datetime
)

insert into enrollment_period_collapsed
(patient_id,employer_id,period_start,period_end)
select x.patient_id,x.employer_id,x.enrolled_on as period_start,min(y.enrolled_on) as period_end
from enrollment_period_detail x
join
enrollment_period_detail y
on x.patient_id=y.patient_id and x.employer_id=y.employer_id
where
x.sflag=1 and y.eflag=1 and y.enrolled_on>=x.enrolled_on
group by x.patient_id,x.employer_id,x.enrolled_on

update a
set period_end=b.max_period_end
from
enrollment_period_collapsed a
join
(
select patient_id,employer_id,max(isnull(period_end,'12/31/2100')) as max_period_end
from emma.dbo.enrollment_period
group by patient_id,employer_id) b
on a.patient_id=b.patient_id and a.employer_id=b.employer_id
where
a.period_end='2015-06-19'


alter table enrollment_period_detail
add
monthx int,
yearx int,
yearxmonthx int


update enrollment_period_detail
set monthx=datepart(mm,enrolled_on),
yearx=datepart(yyyy,enrolled_on)

update enrollment_period_detail
set yearxmonthx=(yearx*100)+monthx


select patient_id,employer_id,yearxmonthx,yearx,monthx,count(*) as number_of_days
into enrollment_period_monthwise
from enrollment_period_detail
group by patient_id,employer_id,yearxmonthx,yearx,monthx


create index i1_enrollment_period_detail on enrollment_period_detail(patient_id,employer_id,yearxmonthx,yearx,monthx)


select top 10 * from enrollment_period_monthwise

alter table enrollment_period_monthwise
add
ptID uniqueidentifier,
employerID uniqueidentifier


create clustered index c1_enrollment_period_monthwise on enrollment_period_monthwise(patient_id,employer_id)
create index i1_enrollment_period_monthwise on enrollment_period_monthwise(ptid)
create index i2_enrollment_period_monthwise on enrollment_period_monthwise(employerid)

set rowcount 1000
while exists(select top 1 1 from enrollment_period_monthwise where ptid is null)
begin
	update a
	set a.ptid=b.mbrID
	from enrollment_period_monthwise a
	join ql_patient_demographic b
	on a.patient_id=b.individual_id
	where a.ptid is null

end
while exists(select top 1 1 from enrollment_period_monthwise where employerid is null)
begin
	update a
	set a.employerid=b.ahemployer_id
	from enrollment_period_monthwise a
	join ql_employer b
	on a.employer_id=b.individual_id
	where a.employerid is null

end


set rowcount 0
/*
select count(*),count(employerID),count(employer_id) from enrollment_period_monthwise

select top 10 * from enrollment_period_monthwise

select yearxmonthx,patient_id,count(*)
from enrollment_period_monthwise
group by yearxmonthx,patient_id
having count(*)>1

select * from enrollment_period_monthwise
where yearxmonthx=201506 and patient_id=43099947

select * from ql_employer where ahemployer_id in
(
'EE8DD5C7-D5A6-4902-8D3D-F46A38E8DA61','C99EC437-5660-43BF-91CD-C076211CA3B7'
)

select a.patient_id,a.employer_id,b.employer_id,a.yearxmonthx
from
enrollment_period_monthwise a
join enrollment_period_monthwise b
on a.patient_id=b.patient_id and a.yearxmonthx=b.yearxmonthx
where
a.employer_id not in (43078932,43099864) and b.employer_id not in (43078932,43099864)
and a.number_of_days=b.number_of_days
and a.employer_id!=b.employer_id
*/

insert into clinical.dbo.stdbilling_new
(ptid,employerID,yearx,monthx,yearxmonthx,billed)
select
a.ptid,a.employerid,a.yearx,a.monthx,a.yearxmonthx,1
from
enrollment_period_monthwise a
join enrollment_period_monthwise b
on a.patient_id=b.patient_id and a.yearxmonthx=b.yearxmonthx
where
a.employer_id=43078932 and b.employer_id=43099864
and a.number_of_days=b.number_of_days


insert into clinical.dbo.stdbilling_new
(ptid,employerID,yearx,monthx,yearxmonthx,billed)
select
a.ptid,a.employerid,a.yearx,a.monthx,a.yearxmonthx,1
from
enrollment_period_monthwise a
join enrollment_period_monthwise b
on a.ptid=b.ptid and a.yearxmonthx=b.yearxmonthx
left join clinical.dbo.stdbilling_new c
on a.ptid=c.ptid and a.yearxmonthx=c.yearxmonthx
where
a.employer_id=43078932 and b.employer_id=43099864
and a.number_of_days=b.number_of_days
and c.ptid is null
insert into clinical.dbo.stdbilling_new
(ptid,employerID,yearx,monthx,yearxmonthx,billed)
select
a.ptid,a.employerid,a.yearx,a.monthx,a.yearxmonthx,1
from
enrollment_period_monthwise a
join enrollment_period_monthwise b
on a.ptid=b.ptid and a.yearxmonthx=b.yearxmonthx
left join clinical.dbo.stdbilling_new c
on a.ptid=c.ptid and a.yearxmonthx=c.yearxmonthx
where
a.employer_id=43078932 and b.employer_id!=43078932
and a.number_of_days=b.number_of_days
and c.ptid is null

insert into clinical.dbo.stdbilling_new
(ptid,employerID,yearx,monthx,yearxmonthx,billed)
select
distinct
a.ptid,a.employerid,a.yearx,a.monthx,a.yearxmonthx,1
from
enrollment_period_monthwise a
join enrollment_period_monthwise b
on a.ptid=b.ptid and a.yearxmonthx=b.yearxmonthx
left join clinical.dbo.stdbilling_new c
on a.ptid=c.ptid and a.yearxmonthx=c.yearxmonthx
where
a.employer_id is not null and b.employer_id is null
and a.number_of_days=b.number_of_days
and c.ptid is null
insert into clinical.dbo.stdbilling_new
(ptid,employerID,yearx,monthx,yearxmonthx,billed)
select
distinct
a.ptid,a.employerid,a.yearx,a.monthx,a.yearxmonthx,1
from
enrollment_period_monthwise a
join enrollment_period_monthwise b
on a.ptid=b.ptid and a.yearxmonthx=b.yearxmonthx
left join clinical.dbo.stdbilling_new c
on a.ptid=c.ptid and a.yearxmonthx=c.yearxmonthx
where
a.employer_id< b.employer_id 
and a.number_of_days=b.number_of_days
and c.ptid is null



insert into clinical.dbo.stdbilling_new
(ptid,employerID,yearx,monthx,yearxmonthx,billed)
select
distinct
a.ptid,a.employerid,a.yearx,a.monthx,a.yearxmonthx,1
from
enrollment_period_monthwise a
join
(
select
ptid,yearx,monthx,yearxmonthx,max(number_of_days) as maxDays
from
enrollment_period_monthwise 
group by ptid,yearx,monthx,yearxmonthx
) b
on 
a.ptid=b.ptid  and a.number_of_days=b.maxDays and a.yearxmonthx=b.yearxmonthx
left join clinical.dbo.stdbilling_new c
on a.ptid=c.ptid and a.yearxmonthx=c.yearxmonthx
where
c.ptid is null 


select * from enrollment_period_monthwise
where ptid='a0f2c99d-254d-42c4-a1fd-c20ce8aa951f' and yearxmonthx=201505


select * from ql_employer 
where ahemployer_id in (
'EE8DD5C7-D5A6-4902-8D3D-F46A38E8DA61','2D96F605-E237-4366-B3A6-CB89C1D07C16'
)

select a.*
from enrollment_period_monthwise a
left join clinical.dbo.stdbilling_new b
on a.ptid=b.ptid and a.yearxmonthx=b.yearxmonthx
where b.ptid is null
USE [Clinical]
GO

/****** Object:  Table [dbo].[stdbilling]    Script Date: 6/22/2015 3:02:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[stdbilling_new](
	[ptID] [uniqueidentifier] NOT NULL,
	[yearx] [varchar](10) NOT NULL,
	[monthx] [varchar](10) NOT NULL,
	[billed] [int] NULL,
	[billingEstimated] [int] NULL,
	[isscholarship] [int] NULL,
	[monthsInProgram] [int] NULL,
	[employerID] [uniqueidentifier] NULL,
	[pcpID] [uniqueidentifier] NULL,
	[invoiceAmt] [decimal](18, 2) NULL,
	[create_date] [datetime] NULL,
	[obsolete_date] [datetime] NULL,
	[audit_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[yearxmonthx] [int] NULL,
	[insurancePlan] [varchar](256) NULL,
	[insuranceCompany] [varchar](256) NULL,
	[companyOffice] [varchar](256) NULL,
 CONSTRAINT [PK_stdbilling_new] PRIMARY KEY CLUSTERED 
(
	[ptID] ASC,
	[yearx] ASC,
	[monthx] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[stdbilling_new] ADD  CONSTRAINT [DF__stdbillin_new_creat__4336F4B9]  DEFAULT (getdate()) FOR [create_date]
GO

ALTER TABLE [dbo].[stdbilling_new] ADD  CONSTRAINT [DF__stdbillin_new_audit__442B18F2]  DEFAULT (newid()) FOR [audit_id]
GO

ALTER TABLE [dbo].[stdbilling_new]  WITH CHECK ADD  CONSTRAINT [FK__stdbilling_new_ptID__4242D080] FOREIGN KEY([ptID])
REFERENCES [dbo].[patient] ([ptID])
GO

ALTER TABLE [dbo].[stdbilling_new] CHECK CONSTRAINT [FK__stdbilling_new_ptID__4242D080]
GO



update a
set a.pcpid=b.pcpid,a.insurancePlan=b.insurancePlan,
a.insuranceCompany=b.insuranceCompany,a.companyOffice=b.companyOffice
from
stdbilling_new a
join stdbilling b
on a.ptid=b.ptid and a.yearxmonthx=b.yearxmonthx and
a.employerID=b.employerID
