
update patientsToBeUnmerged
set unmergeComplete=1
where mbrID='D00EED1D-F939-4725-AA88-4FEC6F74DE4B'


select * from PatientsToBeUnmerged
where unmergecomplete is null


declare
	@mbrID uniqueidentifier

select @mbrID='2B2A68E5-FCF0-4101-959B-C1550E355884'
select * from metadb.dbo.mfldview
where personID=@mbrID

select * From ql_patient_demographic
where mbrID=@mbrID and obsolete_date is null

/*
update patientsToBeUnmerged
set claimsEHRmismatch=1
where mbrID='4E803F3F-90DD-4942-BBF2-BFCD2938BF6E'*/
/*
update patientsToBeUnmerged
set potentialDuplicate=1
where mbrID='EEF26E7F-6068-4E43-85C5-A907C65EE14A'*/
/*
update patientsToBeUnmerged
set claimsMixedup=1
where mbrID='4E803F3F-90DD-4942-BBF2-BFCD2938BF6E'*/

select * from claimsCoordinatedCare.dbo.rawelig
where mbrID=@mbrID
--U9001861701

select * from emma.dbo.patient
where individual_id in 
(
select individual_id from staging.dbo.ql_patient_Demographic where mbrID=@mbrID
)
select * from claimsAetnaExpedia.dbo.stdclaims where mbrID=@mbrID

select * from claimsCoordinatedCare.dbo.stdclaims
where mbrID=@mbrID
select * from claimsPremeraExpedia.dbo.rawclaims where mbrID=@mbrID
select * from claimsIBCComcast.dbo.rawclaims where mbrID=@mbrID
/*

update patientsToBeUnmerged
set individual_id1=43147999,individual_id2=43121934
where
mbrID='2B2A68E5-FCF0-4101-959B-C1550E355884'
*/
