--
--insert data to patient table
--
--ptID already populated in the Xref table
--insert the data which is not existing in the patient table
--also, insert the patient attribute, patient column map tables
--
insert into Clinical2.dbo.patient
(
	ptID,
	ptFirst,
	ptLast,
	ptMiddle,
	ptSuffix,
	ptStatus,
	birthDate,
	email,
	gender,
	pcpID,
	loadID
)
select  s.mbrID,
metadb.dbo.udfGetOneAttValue(s.mbrID,'fname'),--                 rtrim(ltrim(s.[Patient First Name])),
metadb.dbo.udfGetOneAttValue(s.mbrID,'lname'),--                 rtrim(ltrim(s.[Patient Last Name])),                  
metadb.dbo.udfGetOneAttValue(s.mbrID,'mname'),--                 rtrim(ltrim(s.[Patient Middle Initial])),
metadb.dbo.udfGetOneAttValue(s.mbrID,'suffix'),
rtrim(ltrim(s.[Patient Status])),                      
metadb.dbo.udfGetOneAttValue(s.mbrID,'dob'),--                 case when YEAR(s.[Patient Date of Birth]) > 1902 then s.[Patient Date of Birth] else NULL end, 
metadb.dbo.udfGetOneAttValue(s.mbrID,'email'),--                 rtrim(ltrim(s.[Patient Email])),
metadb.dbo.udfGetOneAttValue(s.mbrID,'gender'),--                 upper(left( s.[Patient Gender], 1)),  s.[Patient Deceased],
pcp.ahprovID,1
from 
(
select distinct mbrID,ahptID,[Patient Status] from patientsCurrent
where mbrID is not null
) s
left join 
(
select distinct ahptId,ahprovID from demoPcpCurrent
where ahprovID is not null) pcp on pcp.ahptID = s.ahPtID
left join Clinical2.dbo.patient c
on s.ahptID=c.ptID
where 
 c.ptID is null
and s.mbrID is not null

/*
From PersonXIdentifier
insert into Clinical2.dbo.patient
(
	ptID,
	ptFirst,
	ptLast,
	ptMiddle,
	ptSuffix,
	birthDate,
	email,
	gender,
	stdrel
)
select  s.personID,
metadb.dbo.udfGetOneAttValue(s.personID,'fname'),--                 rtrim(ltrim(s.[Patient First Name])),
metadb.dbo.udfGetOneAttValue(s.personID,'lname'),--                 rtrim(ltrim(s.[Patient Last Name])),                  
metadb.dbo.udfGetOneAttValue(s.personID,'mname'),--                 rtrim(ltrim(s.[Patient Middle Initial])),
metadb.dbo.udfGetOneAttValue(s.personID,'suffix'),
metadb.dbo.udfGetOneAttValue(s.personID,'dob'),--                 case when YEAR(s.[Patient Date of Birth]) > 1902 then s.[Patient Date of Birth] else NULL end, 
metadb.dbo.udfGetOneAttValue(s.personID,'email'),--                 rtrim(ltrim(s.[Patient Email])),
metadb.dbo.udfGetOneAttValue(s.personID,'gender'),--                 upper(left( s.[Patient Gender], 1)),  s.[Patient Deceased],
metadb.dbo.udfGetOneAttValue(s.personID,'stdrel')
from 
(
select distinct personID from PersonXidentifier where newPersonId is null
) s
left join Clinical2.dbo.patient c
on s.PersonID=c.ptID
where 
 c.ptID is null

insert into ptphone
(ptId,phonenumber)
select
a.personID,b.attvalue
from
metaDB.dbo.PersonXidentifier a
join metaDB.dbo.MattributeSetValue b
on a.identifier=b.identifier and b.attribute='phone'
left join ptphone c
on a.PersonID=c.ptID and
b.attvalue=c.phonenumber
where
c.ptid is null
and 
b.attvalue!=' '

update a
set a.email=c.attvalue
from
patient a
join metaDB.dbo.PersonXidentifier b
on a.ptID=b.PersonID
join metaDB.dbo.MattributeSetValue c
on b.identifier=c.identifier and c.attribute='email'
where
a.email is null
and c.attvalue!=' '	


begin transaction
declare
	@loadID int
select @loadID=1000
insert into Clinical2.dbo.patient
(
	ptID,
	ptFirst,
	ptLast,
	ptMiddle,
	ptStatus,
	birthDate,
	email,
	gender,
	deceased,
	pcpID,
	MRN,
	loadID
)select  s.ahPtID,
                 rtrim(ltrim(s.[Patient First Name])),
                 rtrim(ltrim(s.[Patient Last Name])),                  
                 rtrim(ltrim(s.[Patient Middle Initial])),
                 rtrim(ltrim(s.[Patient Status])),                      
                 case when YEAR(s.[Patient Date of Birth]) > 1902 then s.[Patient Date of Birth] else NULL end, 
                 rtrim(ltrim(s.[Patient Email])),
                 upper(left( s.[Patient Gender], 1)),  s.[Patient Deceased],
                 pcp.ahprovID,   s.[Patient Account Number], @loadID
from patientsCurrent s
left join refdb.dbo.ahrace r on s.[Patient Race]= r.description
left join demoPcp pcp on pcp.ahptID = s.ahPtID
left join Clinical2.dbo.patient c
on s.ahptID=c.ptID
join 
(
select ptId,min(srcptid) as firstid from Clinical2.dbo.ptmasterindex
group by ptID
) d
on s.ahptID=d.ptId and s.[Patient ID]=d.firstid
where not(s.[Patient Account Number] = 'A1231234')
and c.ptID is null


--left out
--eventually, no need for this
--
insert into Clinical2.dbo.patient
(
	ptID,
	ptFirst,
	ptLast,
	ptMiddle,
	ptStatus,
	birthDate,
	email,
	gender,
	deceased,
	pcpID,
	MRN,
	loadID
)
select
	distinct
	a.ptID,
	b.ptFirst,
	b.ptLast,
	b.ptMiddle,
	b.Status,
	convert(date,convert(varchar,b.birthDate),112),
	b.email,
	b.gender,
	b.deceased,
	b.pcpID,
	b.MRN,
	1
from
Clinical2.dbo.ptmasterindex a
join Clinical.dbo.patient b
on a.ptID=b.ptID
left join Clinical2.dbo.patient c
on a.ptID=c.ptID
where
c.ptID is null

--
--ptaddress
--

--
--ptaddress
--
insert into Clinical2.dbo.ptaddress
(
ptID,
address1,
address2,
city,
zip,
state
)
select
distinct
ahptID,
[Patient Address Line1],
[Patient Address Line2],
[Patient City],
[Patient Zip Code],
[Patient State]
from
patientsCurrent a
left join Clinical2.dbo.ptaddress b
on a.ahptId=b.ptId
where
b.ptId is null

--
--ptphone
--
insert into Clinical2.dbo.ptphone
(
ptID,
phonenumber,
phonetype
)
select
distinct
a.ahptID,
refdb.dbo.phoneInt(a.[Patient Phone Number]),
'work'
from
patientsCurrent a
left join Clinical2.dbo.ptphone b
on a.ahptID=b.ptID
and refdb.dbo.phoneInt(a.[Patient Phone Number])=b.phonenumber
where
b.ptId is null and
a.[Patient Phone Number] is not null

--
--ptattribute
--

insert into Clinical2.dbo.ptattribute
(
ptID,attribute_name,attribute_value
)
select
distinct
s.ahptId,'race',r.code
from
patientsCurrent s
join refdb.dbo.ahrace r on s.[Patient Race]= r.description
left join Clinical2.dbo.ptattribute pta
on s.ahptID=pta.ptID and r.code=pta.attribute_value
and pta.attribute_name='race'
where
pta.ptID is null 

insert into Clinical2.dbo.ptattribute
(
ptID,attribute_name,attribute_value
)
select
distinct
s.ahptId,'ptLang',rtrim(ltrim(s.[Patient Language]))
from
patientsCurrent s
left join Clinical2.dbo.ptattribute pta
on s.ahptID=pta.ptID and rtrim(ltrim(s.[Patient Language]))=pta.attribute_value
and pta.attribute_name='ptLang'
where
pta.ptID is null 
and s.[Patient Language] is not null

insert into Clinical2.dbo.ptattribute
(
ptID,attribute_name,attribute_value
)
select
distinct
s.ahptId,'studentStatus',rtrim(ltrim(s.[Patient Student Status]))
from
patientsCurrent s
left join Clinical2.dbo.ptattribute pta
on s.ahptID=pta.ptID and rtrim(ltrim(rtrim(ltrim(s.[Patient Student Status]))))=pta.attribute_value
and pta.attribute_name='studentStatus'
where
pta.ptID is null 
and s.[Patient Student Status] is not null


commit
*/