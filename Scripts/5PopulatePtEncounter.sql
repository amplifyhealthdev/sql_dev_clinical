declare
	@fileset_id uniqueidentifier,
	@sourceType varchar(256),
	@sourceSubtype varchar(256)

begin transaction

	select top 1 @fileset_id=fileset_id from apptsProviderCurrent 
	
	select @sourceType=sourceType,@sourceSubtype=sourceSubtype
	from metaDB.dbo.filesetDetail
	where fileset_id=@fileset_id
	
	

	insert into clinical.dbo.ptencounter
	(
		encounterID,
		ptID,
		provID,
		encounterType,
		encounterStatus,
		apptStart ,
		apptEnd,
		apptArrive,
		apptDepart,
		fileset_id
	)
	select 
	DISTINCT appts.ahencID, appts.ahptID, appts.ahprovID, 
	rtrim(ltrim(appts.[Visit Type])), rtrim(ltrim(appts.[Visit Status])), 
	appts.[Appointment Date]+ CAST(appts.[Appointment Start Time] as datetime),
	appts.[Appointment Date]+ CAST(appts.[Appointment End Time]as datetime),
	appts.[Appointment Date]+ CAST(appts.[Appointments Arrived Time]as datetime),
	appts.[Appointment Date]+ CAST(appts.[Appointments Departure Time]as datetime), @fileset_id
	from apptsProviderCurrent appts 
	left join refdb.dbo.ahvisittype v on rtrim(ltrim(appts.[Visit Type])) = rtrim(ltrim(v.code))
	left join refdb.dbo.ahvisitstatus st on rtrim(ltrim(appts.[Visit Status])) = rtrim(ltrim(st.code))
	left join clinical.dbo.ptencounter d
	on appts.ahencID=d.encounterID
	where 
	d.encounterID is null and appts.ahptID is not null
	and appts.ahencID is not null	
	/*
	insert into clinical.dbo.ptencounter
	(
		encounterID,
		ptID,
		provID,
		encounterType,
		encounterStatus,
		apptStart ,
		apptEnd,
		apptArrive,
		apptDepart,
		loadID
	)
	select 
	DISTINCT appts.ahencID, appts.ahptID, appts.ahprovID, 
	rtrim(ltrim(appts.[Visit Type])), rtrim(ltrim(appts.[Visit Status])), 
	appts.[Appointment Date]+ CAST(appts.[Appointment Start Time] as datetime),
	appts.[Appointment Date]+ CAST(appts.[Appointment End Time]as datetime),
	appts.[Appointment Date]+ CAST(appts.[Appointments Arrived Time]as datetime),
	appts.[Appointment Date]+ CAST(appts.[Appointments Departure Time]as datetime), 1
	from apptsProviderCurrent appts 
	left join refdb.dbo.ahvisittype v on rtrim(ltrim(appts.[Visit Type])) = rtrim(ltrim(v.code))
	left join refdb.dbo.ahvisitstatus st on rtrim(ltrim(appts.[Visit Status])) = rtrim(ltrim(st.code))
	left join clinical.dbo.ptencounter d
	on appts.ahencID=d.encounterID
	where 
	d.encounterID is null
	*/
	
	--encounter data only appearing on labs
	insert into clinical.dbo.ptencounter
	(
		encounterID,
		ptID,
		provID,
		encounterType,
		encounterStatus,
		apptStart ,
		apptEnd,
		apptArrive,
		apptDepart,
		apptLocation,
		fileset_id
	)
	select DISTINCT y.ahencID ,y.ahptID, y.ahprovID,  'LAB', 'COMPLETE',  
	                 y.[Encounter Date],y.[Encounter Date],y.[Encounter Date],y.[Encounter Date],
	                 y.[Facility Name], @fileset_id
	                 from labsCurrent y left join clinical.dbo.ptencounter s on y.ahencID =s.encounterID 
	where s.encounterID IS NULL and 
	y.ahptID is not null and
	y.[Encounter Date] >'1902/01/01'
	and y.ahencID is not null
	--encounter data only appearing on immunizations
	
	insert into clinical.dbo.ptencounter
	(
		encounterID,
		ptID,
		provID,
		encounterType,
		encounterStatus,
		apptStart ,
		apptEnd,
		apptArrive,
		apptDepart,
		apptLocation,
		fileset_id
	)
	select DISTINCT y.ahencID ,y.ahptID, y.ahprovID,  'IMM', 'COMPLETE', 
	                 y.[Given Date],  y.[Given Date],  y.[Given Date],  y.[Given Date],                  
	                  y.[Facility Name],  @fileset_id
	from immunizationsCurrent y 
	left join clinical.dbo.ptencounter pte on y.ahencID = pte.encounterID 
	where 
	pte.encounterID IS NULL and 
	y.ahptID is not null and
	y.[Given Date] >'1902/01/01'
	and y.ahencID is not null
	
	
	
	--encounter data appearing only in Visits
	--
	insert into clinical.dbo.ptencounter
	(
		encounterID,
		ptID,
		provID,
		encounterType,
		encounterStatus,
		apptStart ,
		apptEnd,
		apptArrive,
		apptDepart,
		apptLocation,
		fileset_id
	)
	select DISTINCT v.ahencID ,v.ahptID, v.ahprovID, v.[Visit Type], v.[Visit status], 
	                v.[Encounter Date], v.[Encounter Date],v.[Encounter Date],v.[Encounter Date],
	                v.[Facility Name],  @fileset_id
	from vitalsCurrent v left join clinical.dbo.ptencounter pte on v.ahencID = pte.encounterID 
	where pte.encounterID IS NULL AND
	v.ahptID is not null and
	v.[Encounter Date] >'1902/01/01' 
	and v.ahencID is not null
	--encounter data only available on diagCodes
	insert into clinical.dbo.ptencounter
	(
		encounterID,
		ptID,
		provID,
		encounterType,
		encounterStatus,
		apptStart ,
		apptEnd,
		apptArrive,
		apptDepart,
		apptLocation,
		fileset_id		
	)
	select DISTINCT d.ahEncID ,d.ahPtID, d.ahprovID, d.[Visit Type], d.[Visit Status], 
	                 d.[Encounter Date], d.[Encounter Date], d.[Encounter Date], d.[Encounter Date], 
	                  d.[Facility Name],  @fileset_id
	from diagCodesCurrent d left join clinical.dbo.ptencounter pte 
	on d.ahencID = pte.encounterID 
	where pte.encounterID IS NULL and 
	d.[Encounter Date] > '1902/01/01' and
	d.ahptID is not null and
	d.ahencID is not null
	
	--encounter data only available on procCodeAnalysis
	insert into clinical.dbo.ptencounter
	(
		encounterID,
		ptID,
		provID,
		encounterType,
		encounterStatus,
		apptStart ,
		apptEnd,
		apptArrive,
		apptDepart,
		apptLocation,
		fileset_id
	)
	select DISTINCT d.ahencID ,d.ahptID, d.ahprovID, d.[Visit Type], d.[Visit Status],  
	                 d.[Encounter Date], d.[Encounter Date], d.[Encounter Date], d.[Encounter Date], 
	                 d.[Facility Name], @fileset_id 
	from procCodeAnalysisCurrent d 
	left join clinical.dbo.ptencounter pte 
	on d.ahencID = pte.encounterID 
	where pte.encounterID IS NULL and 
	d.[Encounter Date] > '1902/01/01' and
	d.ahptID is not null
	and d.ahencID is not null

	--encounter data only available on procCodeAnalysis
	insert into clinical.dbo.ptencounter
	(
		encounterID,
		ptID,
		provID,
		encounterType,
		encounterStatus,
		apptStart ,
		apptEnd,
		apptArrive,
		apptDepart,
		apptLocation,
		fileset_id
	)
	select DISTINCT d.ahencID ,d.ahptID, d.ahprovID, d.[Visit Type], d.[Visit Status],  
	                 d.[Encounter Date], d.[Encounter Date], d.[Encounter Date], d.[Encounter Date], 
	                 d.[Facility Name], @fileset_id
	from procCodeAnalysisCurrent d 
	left join clinical.dbo.ptencounter pte 
	on d.ahencID = pte.encounterID 
	where pte.encounterID IS NULL and 
	d.[Encounter Date] > '1902/01/01' and
	d.ahptID is not null
	and d.ahencID is not null	
	
	UPDATE pte
	SET pte.apptLocation = mraw.[Facility Name] from clinical.dbo.ptencounter pte
	join procCodeAnalysisCurrent mraw on mraw.ahencID = pte.encounterID
	where 
	pte.apptLocation IS NULL 
	
	
	update p
	set p.encounterStatus = [Visit Status] 
	from clinical.dbo.ptencounter p
	join diagCodesCurrent m on  p.encounterid  = m.ahencID  
	join refdb.dbo.ahvisitstatus v on p.encounterStatus = v.code
	where m.[Visit Status] is not null and v.type != 'Completed'
	and p.encounterStatus is null
	update p
	set p.encounterStatus = [Visit Status] from clinical.dbo.ptencounter p
	join vitalsCurrent mv on  p.encounterid  = mv.ahencID  
	join refdb.dbo.ahvisitstatus v on p.encounterStatus = v.code
	where mv.[Visit Status] is not null and v.type != 'Completed'
	and p.encounterStatus is null
	
	update p
	set p.encounterStatus = [Lab Status] from clinical.dbo.ptencounter p
	join labsCurrent m on  p.encounterid  = m.ahencID  
	join refdb.dbo.ahvisitstatus v on p.encounterStatus = v.code
	where [Lab Status] is not null and v.type != 'Completed'
	and p.encounterStatus is null
	
	update p 
	set p.encounterStatus = [Visit Status] from clinical.dbo.ptencounter p
	join procCodeAnalysisCurrent m on  p.encounterid  = m.ahencID  
	join refdb.dbo.ahvisitstatus v on p.encounterStatus = v.code
	where [Visit Status] is not null and v.type != 'Completed'
	and p.encounterStatus is null
	
	-- apptsProvider has to be last in this set of updates because it was first to load ptencounter and is presumable the master
	update p 
	set p.encounterStatus = [Visit Status] from clinical.dbo.ptencounter p
	join apptsProviderCurrent m on  p.encounterid  = m.ahencID  
	join refdb.dbo.ahvisitstatus v on p.encounterStatus = v.code
	where [Visit Status] is not null and v.type != 'Completed'
	and p.encounterStatus is null
	

	update clinical.dbo.ptencounter
	set encounterStatus='Complete'
	where encounterType='TEL'
	and isnull(encounterStatus,'X')!='Complete'
	
	update clinical.dbo.ptencounter
	set apptLocation='Phone'
	where encounterType='TEL'
	and isnull(apptLocation,'X')!='Phone'
	
	
	--commit
	--rollback
	