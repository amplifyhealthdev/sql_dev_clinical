

set identity_insert apptCodes on
insert into apptCodes
([Visit Type Code ID],[Visit Type Name],[Visit Type Description],[Chart Title],[Visit Type],[Active],[rowid],[fileset_id]
)
select
[Visit Type Code ID],[Visit Type Name],[Visit Type Description],[Chart Title],[Visit Type],[Active],[rowid],[fileset_id]
from apptCodesCurrent
set identity_insert apptCodes off

set identity_insert apptsProvider on
insert into apptsProvider
([Appointment Provider ID],[Encounter ID],[Appointment Date],[Appointment Start Time],[Appointment End Time],[Appointments Arrived Time],[Appointments Departure Time],[Appointments Checked Out],[Appointments Checked In],[Notes Done Date],[Notes Done Time],[New Time],[Billable Appointments],[Chart Lock Status],[Encounter Type],[Interface Visit Number],[Marked Done Visit Count],[New Patient],[Not Marked Done Visit Count],[Code],[Visit Status],[Visit Type],[Lock Encounter Status],[Delete Flag],[Action Flag],[Payment Type],[Encounter Eligibility Status],[Appointment Duration],[Billable Visit Count],[New Patients Count (by Visit)],[Patient Count],[Time Difference],[Visit Count],[Difference],[Patient ID],[rowid],[ahPtID],[ahEncID],[ahprovID],[fileset_id]
)
select
[Appointment Provider ID],[Encounter ID],[Appointment Date],[Appointment Start Time],[Appointment End Time],[Appointments Arrived Time],[Appointments Departure Time],[Appointments Checked Out],[Appointments Checked In],[Notes Done Date],[Notes Done Time],[New Time],[Billable Appointments],[Chart Lock Status],[Encounter Type],[Interface Visit Number],[Marked Done Visit Count],[New Patient],[Not Marked Done Visit Count],[Code],[Visit Status],[Visit Type],[Lock Encounter Status],[Delete Flag],[Action Flag],[Payment Type],[Encounter Eligibility Status],[Appointment Duration],[Billable Visit Count],[New Patients Count (by Visit)],[Patient Count],[Time Difference],[Visit Count],[Difference],[Patient ID],[rowid],[ahPtID],[ahEncID],[ahprovID],[fileset_id]
from apptsProviderCurrent
set identity_insert apptsProvider off

set identity_insert demoPCP on
insert into demoPCP
([Patient ID],[Demographics PCP NPI],[Demographics PCP UPIN],[Demographics PCP Name],[Demographics PCP Last Name],[Demographics PCP First Name],[Demographics PCP Middle Initial],[Demographics PCP Full Name (First Name First)],[Demographics PCP Email],[Demographics PCP Address Line 1],[Demographics PCP Address Line 2],[Demographics PCP City],[Demographics PCP State],[Demographics PCP Zip Code],[Demographics PCP Phone Number],[Demographics PCP Fax No],[Demographics PCP Gender],[Demographics PCP Suffix],[Demographics PCP Prefix],[Demographics PCP Initials],[Demographics PCP DEA No],[Demographics PCP Speciality],[Demographics PCP Tax ID],[Demographics PCP Tax IDType],[rowid],[ahPtID],[ahProvID],[fileset_id]
)
select
[Patient ID],[Demographics PCP NPI],[Demographics PCP UPIN],[Demographics PCP Name],[Demographics PCP Last Name],[Demographics PCP First Name],[Demographics PCP Middle Initial],[Demographics PCP Full Name (First Name First)],[Demographics PCP Email],[Demographics PCP Address Line 1],[Demographics PCP Address Line 2],[Demographics PCP City],[Demographics PCP State],[Demographics PCP Zip Code],[Demographics PCP Phone Number],[Demographics PCP Fax No],[Demographics PCP Gender],[Demographics PCP Suffix],[Demographics PCP Prefix],[Demographics PCP Initials],[Demographics PCP DEA No],[Demographics PCP Speciality],[Demographics PCP Tax ID],[Demographics PCP Tax IDType],[rowid],[ahPtID],[ahProvID],[fileset_id]
from demoPCPCurrent
set identity_insert demoPCP off

set identity_insert demoRendPrvd on
insert into demoRendPrvd
([Demographics Rendering Provider Name],[Demographics Rendering Provider Last Name],[Demographics Rendering Provider First Name],[Demographics Rendering Provider Middle Initial],[Demographics Rendering Provider Full Name (first name first)],[Demographics Rendering Provider Email],[Demographics Rendering Provider Address Line 1],[Demographics Rendering Provider Address Line 2],[Demographics Rendering Provider City],[Demographics Rendering Provider State],[Demographics Rendering Provider Zip Code],[Demographics Rendering Provider Phone Number],[Demographics Rendering Provider Fax No],[Demographics Rendering Provider Gender],[Demographics Rendering Provider Suffix],[Demographics Rendering Provider Prefix],[Demographics Rendering Provider Initials],[Demographics Rendering Provider DEA No],[Demographics Rendering Provider Speciality],[Demographics Rendering Provider Tax ID],[Demographics Rendering Provider Tax IDType],[Demographics Rendering Provider NPI],[Demographics Rendering Provider UPIN],[Demographics Rendering Provider ID],[rowid],[ahProvID],[fileset_id]
)
select
[Demographics Rendering Provider Name],[Demographics Rendering Provider Last Name],[Demographics Rendering Provider First Name],[Demographics Rendering Provider Middle Initial],[Demographics Rendering Provider Full Name (first name first)],[Demographics Rendering Provider Email],[Demographics Rendering Provider Address Line 1],[Demographics Rendering Provider Address Line 2],[Demographics Rendering Provider City],[Demographics Rendering Provider State],[Demographics Rendering Provider Zip Code],[Demographics Rendering Provider Phone Number],[Demographics Rendering Provider Fax No],[Demographics Rendering Provider Gender],[Demographics Rendering Provider Suffix],[Demographics Rendering Provider Prefix],[Demographics Rendering Provider Initials],[Demographics Rendering Provider DEA No],[Demographics Rendering Provider Speciality],[Demographics Rendering Provider Tax ID],[Demographics Rendering Provider Tax IDType],[Demographics Rendering Provider NPI],[Demographics Rendering Provider UPIN],[Demographics Rendering Provider ID],[rowid],[ahProvID],[fileset_id]
from demoRendPrvdCurrent
set identity_insert demoRendPrvd off

set identity_insert diagCodes on
insert into diagCodes
([Encounter ID],[Provider ID],[Patient ID],[Encounter Date],[Diagnosis Encounter ID],[Diagnosis Item ID],[Distinct Prescription Name],[Diagnosis Description],[Patient Account Number],[Patient Age],[Patient Age Group],[Patient Gender],[Patient Name],[Patient Zip Code],[Patient State],[Patient City],[Patient Race],[Patient Ethnicity],[ICD-9 Code ID],[ICD-9 Code],[Facility Name],[Appointment Provider Name],[Prescription Name],[Facility Group Name],[Encounter Type],[Delete Flag],[Visit Status],[Visit Type],[Distinct ICD-9 Code],[Prescription Count],[rowid],[ahPtID],[ahEncID],[ahprovID],[fileset_id]
)
select
[Encounter ID],[Provider ID],[Patient ID],[Encounter Date],[Diagnosis Encounter ID],[Diagnosis Item ID],[Distinct Prescription Name],[Diagnosis Description],[Patient Account Number],[Patient Age],[Patient Age Group],[Patient Gender],[Patient Name],[Patient Zip Code],[Patient State],[Patient City],[Patient Race],[Patient Ethnicity],[ICD-9 Code ID],[ICD-9 Code],[Facility Name],[Appointment Provider Name],[Prescription Name],[Facility Group Name],[Encounter Type],[Delete Flag],[Visit Status],[Visit Type],[Distinct ICD-9 Code],[Prescription Count],[rowid],[ahPtID],[ahEncID],[ahprovID],[fileset_id]
from diagCodesCurrent
set identity_insert diagCodes off

set identity_insert immunizations on
insert into immunizations
([Given Date],[Encounter Date],[Encounter ID],[Billable],[CVX Code],[Date VIS Given],[Date On VIS],[Dosage],[Dosage No],[Due Date],[Delete Flag],[Facility Name],[Guarantor Name],[Appointment Provider],[Immunization ID],[Item Name],[Item Desc],[Lot Number],[MVX Code],[Patient Account No],[Patient Name],[Patient Age],[Patient Age as on Encounter Date],[Race Code],[Relation Code],[Vaccine Manufacturer],[Vaccine Administrator],[Vaccine Name],[VFC Code],[EncounterID],[rowid],[ahPtID],[ahEncID],[ahProvID],[fileset_id])
select
[Given Date],[Encounter Date],[Encounter ID],[Billable],[CVX Code],[Date VIS Given],[Date On VIS],[Dosage],[Dosage No],[Due Date],[Delete Flag],[Facility Name],[Guarantor Name],[Appointment Provider],[Immunization ID],[Item Name],[Item Desc],[Lot Number],[MVX Code],[Patient Account No],[Patient Name],[Patient Age],[Patient Age as on Encounter Date],[Race Code],[Relation Code],[Vaccine Manufacturer],[Vaccine Administrator],[Vaccine Name],[VFC Code],[EncounterID],[rowid],[ahPtID],[ahEncID],[ahProvID],[fileset_id]
from immunizationsCurrent
set identity_insert immunizations off

set identity_insert insurances on
insert into insurances
([Insurance Group ID],[Insurance Group Name],[Insurance ID],[Insurance Name],[Insurance Payor ID],[Insurance Type],[Insurance Class],[Insurance Address],[Insurance Address 2],[Insurance City],[Insurance State],[Insurance Zip Code],[insurance eMail],[Insurance Phone],[Insurance Fax],[Insurance Delete Flag],[rowid],[fileset_id]
)select
[Insurance Group ID],[Insurance Group Name],[Insurance ID],[Insurance Name],[Insurance Payor ID],[Insurance Type],[Insurance Class],[Insurance Address],[Insurance Address 2],[Insurance City],[Insurance State],[Insurance Zip Code],[insurance eMail],[Insurance Phone],[Insurance Fax],[Insurance Delete Flag],[rowid],[fileset_id]
from insurancesCurrent
set identity_insert insurances off

set identity_insert labs on
insert into labs
([Encounter ID],[Patient Account Number],[Appointment Provider ID],[Report ID],[Facility ID],[Encounter Date],[Result Date],[Lab Reviewed Date],[Lab Reviewed Time],[Lab Name],[Lab Descriptions],[Lab Attribute],[Lab Attribute Value],[Appointment Provider],[Diagnosis Name],[Facility Name],[Patient Name],[Patient State],[Patient Sex],[Patient City],[Patient ZipCode],[Patient Age],[Patient Language],[Patient Ethnicity],[Patient Race],[Patient Characterestic],[Patient Student Status],[Patient Employer Status],[Patient Sex Enumeration],[Lab Result],[Lab Status],[Lab Priority],[Lab Received],[Lab Billable],[Lab Type],[Lab Reason],[Visit Count],[Patient Count],[Lab Results Measure],[rowid],[ahPtID],[ahEncID],[ahProvID],[fileset_id]
)select
[Encounter ID],[Patient Account Number],[Appointment Provider ID],[Report ID],[Facility ID],[Encounter Date],[Result Date],[Lab Reviewed Date],[Lab Reviewed Time],[Lab Name],[Lab Descriptions],[Lab Attribute],[Lab Attribute Value],[Appointment Provider],[Diagnosis Name],[Facility Name],[Patient Name],[Patient State],[Patient Sex],[Patient City],[Patient ZipCode],[Patient Age],[Patient Language],[Patient Ethnicity],[Patient Race],[Patient Characterestic],[Patient Student Status],[Patient Employer Status],[Patient Sex Enumeration],[Lab Result],[Lab Status],[Lab Priority],[Lab Received],[Lab Billable],[Lab Type],[Lab Reason],[Visit Count],[Patient Count],[Lab Results Measure],[rowid],[ahPtID],[ahEncID],[ahProvID],[fileset_id]
from labsCurrent
set identity_insert labs off

insert into patients
([Patient ID],[Patient Account Number],[Patient Name],[Patient First Name],[Patient Last Name],[Patient Middle Initial],[Patient Full Name (First Name First)],[Patient Full Address],[Patient Address Line1],[Patient Address Line2],[Patient City],[Patient State],[Patient Zip Code],[Patient Phone Number],[Patient Email],[Patient Gender],[Patient Date of Birth],[Patient Age],[Patient Language],[Patient Characterestic],[Patient Race],[Patient Ethnicity],[Patient Misc Value],[Patient Misc Info],[Patient MRN],[Patient Status],[Patient Student Status],[Patient Deceased],[Patient Release Information],[Patient Signature Date],[Responsible Party Account Number],[Responsible Party Name],[Responsible Party Full Address],[Responsible Party Address Line1],[Responsible Party Address Line2],[Responsible Party City],[Responsible Party State],[Responsible Party Zip Code],[Patient Statement],[Patient Delete Flag],[mbrID],[employee_mbrID],[ahptID],[fileset_id]
)select
[Patient ID],[Patient Account Number],[Patient Name],[Patient First Name],[Patient Last Name],[Patient Middle Initial],[Patient Full Name (First Name First)],[Patient Full Address],[Patient Address Line1],[Patient Address Line2],[Patient City],[Patient State],[Patient Zip Code],[Patient Phone Number],[Patient Email],[Patient Gender],[Patient Date of Birth],[Patient Age],[Patient Language],[Patient Characterestic],[Patient Race],[Patient Ethnicity],[Patient Misc Value],[Patient Misc Info],[Patient MRN],[Patient Status],[Patient Student Status],[Patient Deceased],[Patient Release Information],[Patient Signature Date],[Responsible Party Account Number],[Responsible Party Name],[Responsible Party Full Address],[Responsible Party Address Line1],[Responsible Party Address Line2],[Responsible Party City],[Responsible Party State],[Responsible Party Zip Code],[Patient Statement],[Patient Delete Flag],[mbrID],[employee_mbrID],[ahptID],[fileset_id]
from patientsCurrent

set identity_insert practiceProviders on
insert into practiceProviders
([Practice Provider ID],[Lab ID],[Practice Provider First Name],[Practice Provider Middel Initial],[Practice Provider Last Name],[Practice Provider email],[Practice Provider Address],[Practice Provider City],[Practice Provider State],[Practice Provider Phone],[Practice Provider Initials],[Practice Provider Speciality],[Practice Provider Speciality Code],[Practice Provider NPI],[Practice Provider GrpNPI],[Practice Provider Is Resident],[Practice Provider UPIN],[Practice Provider Primary Facility],[Active],[Lab Name],[rowid],[ahprovID],[fileset_id]
)select
[Practice Provider ID],[Lab ID],[Practice Provider First Name],[Practice Provider Middel Initial],[Practice Provider Last Name],[Practice Provider email],[Practice Provider Address],[Practice Provider City],[Practice Provider State],[Practice Provider Phone],[Practice Provider Initials],[Practice Provider Speciality],[Practice Provider Speciality Code],[Practice Provider NPI],[Practice Provider GrpNPI],[Practice Provider Is Resident],[Practice Provider UPIN],[Practice Provider Primary Facility],[Active],[Lab Name],[rowid],[ahprovID],[fileset_id]
from practiceProvidersCurrent
set identity_insert practiceProviders Off

set identity_insert problemList on
insert into problemList
([Encounter ID],[Patient ID],[Appointment Provider ID],[Encounter Date],[Patient Account Number],[Patient Name],[Patient Last Name],[Patient Middle Initial],[Patient First Name],[Patient Address],[Patient City],[Patient State],[Patient DOB],[Patient Gender],[Patient Race],[Patient Deceased],[New Patient],[Employer Name],[Employer Address],[Employer Address 2],[Employer City],[Employer State],[Employer Zipcode],[Employer Phone Number],[Problem List Item ID],[Assessment Code],[Specify],[Onset Date],[User ID],[Log Date],[Serial Number],[Inactive Flag],[Insurance Name],[Visit Type],[Visit Status],[rowid],[ahptID],[ahprovID],[ahencID],[fileset_id]
)select
[Encounter ID],[Patient ID],[Appointment Provider ID],[Encounter Date],[Patient Account Number],[Patient Name],[Patient Last Name],[Patient Middle Initial],[Patient First Name],[Patient Address],[Patient City],[Patient State],[Patient DOB],[Patient Gender],[Patient Race],[Patient Deceased],[New Patient],[Employer Name],[Employer Address],[Employer Address 2],[Employer City],[Employer State],[Employer Zipcode],[Employer Phone Number],[Problem List Item ID],[Assessment Code],[Specify],[Onset Date],[User ID],[Log Date],[Serial Number],[Inactive Flag],[Insurance Name],[Visit Type],[Visit Status],[rowid],[ahptID],[ahprovID],[ahencID],[fileset_id]
from problemListCurrent
set identity_insert problemList off

set identity_insert proccodeAnalysis on
insert into proccodeAnalysis
([Encounter ID],[encounter ID1],[Doctor ID],[Patient ID],[Encounter Date],[Patient Name],[Patient Account #],[Patient Last Name],[Patient First Name],[Patient Middle Initial],[Patient DOB],[Patient Gender],[Patient Age],[Patient Race],[Patient Address],[Patient Address 2],[Patient City],[Patient State],[Patient Zipcode],[Patient Phone Number],[Patient e-Mail],[Patient Dob1],[Patient Language],[Patient Age as of Encounter],[Patient Deceased],[New Patient],[Employer Name],[Employer Address],[Employer Address 2],[Employer State],[Employer Zipcode],[Employer Phone Number],[Time In],[Time Out],[Arrived Time],[Departure Time],[Facility ID],[Facility Name],[Department ID],[Modifier 1],[Modifier 2],[Modifier 3],[ICD 1],[ICD 2],[ICD 3],[ICD-9 Code 1],[ICD-9 Code 2],[ICD-9 Code 3],[ICD-9 Code 4],[Prop ID],[CPT Code Description],[CPT Code],[Visit Type],[Visit Status],[Place of Service],[Enc Eligibility Status],[Eligibility Message],[Ethnicity Name],[rowid],[ahPtID],[ahEncID],[ahProvID],[fileset_id]
)select
[Encounter ID],[encounter ID1],[Doctor ID],[Patient ID],[Encounter Date],[Patient Name],[Patient Account #],[Patient Last Name],[Patient First Name],[Patient Middle Initial],[Patient DOB],[Patient Gender],[Patient Age],[Patient Race],[Patient Address],[Patient Address 2],[Patient City],[Patient State],[Patient Zipcode],[Patient Phone Number],[Patient e-Mail],[Patient Dob1],[Patient Language],[Patient Age as of Encounter],[Patient Deceased],[New Patient],[Employer Name],[Employer Address],[Employer Address 2],[Employer State],[Employer Zipcode],[Employer Phone Number],[Time In],[Time Out],[Arrived Time],[Departure Time],[Facility ID],[Facility Name],[Department ID],[Modifier 1],[Modifier 2],[Modifier 3],[ICD 1],[ICD 2],[ICD 3],[ICD-9 Code 1],[ICD-9 Code 2],[ICD-9 Code 3],[ICD-9 Code 4],[Prop ID],[CPT Code Description],[CPT Code],[Visit Type],[Visit Status],[Place of Service],[Enc Eligibility Status],[Eligibility Message],[Ethnicity Name],[rowid],[ahPtID],[ahEncID],[ahProvID],[fileset_id]
from proccodeAnalysisCurrent
set identity_insert proccodeAnalysis off

set identity_insert referrals on
insert into referrals
([Referral Date],[Referral Start Date],[Referral End Date],[Received Date],[Appointment Date],[Referral ID],[Patient ID],[Referral Encounter ID],[Referral Assigned To ID],[Referral Assigned Provider ID],[Referral To Provider ID],[Referral From Provider ID],[Referral To Facility ID],[Referral From Facility ID],[Referral Authorization Number],[Visits Allowed Code],[Visits Used],[Delete Flag],[Referral Priority],[Referral Assigned To],[Referral Status],[Authorization Type],[Referral Procedures],[Speciality],[Place Of Service],[Unit Type],[Front Office Authorization],[Referral Number],[Appointment Time],[Referral From Doctor Provider ID],[Referral To Doctor Provider ID],[Referring To Provider Name],[Patient Account Number],[Patient Name],[Referral Type],[Patient Employer Name],[Patient Employer Address],[Patient Employer Address 2],[Patient Employer City],[Patient Employer State],[Patient Employer Zipcode],[Patient Address],[Patient Address 2],[Patient Gender],[Patient City],[Patient State],[Patient Zipcode],[Patient Race],[Patient Date of Birth],[Referral To Facility Name],[Referral From Facility Name],[Referral From Provider Speciality],[Referral From Provider Speciality Code],[Referral Fax No],[Referral To Speciality],[Referral To SpecialityCode],[Referral To Provider Fax No],[Referral From Provider Name],[rowid],[ahPtID],[ahToProvID],[ahProvID],[ahEncID],[fileset_id]
)select
[Referral Date],[Referral Start Date],[Referral End Date],[Received Date],[Appointment Date],[Referral ID],[Patient ID],[Referral Encounter ID],[Referral Assigned To ID],[Referral Assigned Provider ID],[Referral To Provider ID],[Referral From Provider ID],[Referral To Facility ID],[Referral From Facility ID],[Referral Authorization Number],[Visits Allowed Code],[Visits Used],[Delete Flag],[Referral Priority],[Referral Assigned To],[Referral Status],[Authorization Type],[Referral Procedures],[Speciality],[Place Of Service],[Unit Type],[Front Office Authorization],[Referral Number],[Appointment Time],[Referral From Doctor Provider ID],[Referral To Doctor Provider ID],[Referring To Provider Name],[Patient Account Number],[Patient Name],[Referral Type],[Patient Employer Name],[Patient Employer Address],[Patient Employer Address 2],[Patient Employer City],[Patient Employer State],[Patient Employer Zipcode],[Patient Address],[Patient Address 2],[Patient Gender],[Patient City],[Patient State],[Patient Zipcode],[Patient Race],[Patient Date of Birth],[Referral To Facility Name],[Referral From Facility Name],[Referral From Provider Speciality],[Referral From Provider Speciality Code],[Referral Fax No],[Referral To Speciality],[Referral To SpecialityCode],[Referral To Provider Fax No],[Referral From Provider Name],[rowid],[ahPtID],[ahToProvID],[ahProvID],[ahEncID],[fileset_id]
from referralsCurrent
set identity_insert referrals off

set identity_insert resProvider on
insert into resProvider
([Resource Provider Name],[Resource Provider ID],[Resource Provider Last Name],[Resource Provider First Name],[Resource Provider Middle Initial],[Resource Provider Full Name (First Name First)],[Resource Provider Full Address],[Resource Provider Address Line 1],[Resource Provider Address Line 2],[Resource Provider City],[Resource Provider State],[Resource Provider Phone],[Resource Provider Zip Code],[Resource Provider Gender],[Resource Provider Suffix],[Resource Provider Prefix],[Resource Provider Initials],[Resource Provider DEA No],[Resource Provider Fax No],[Resource Provider Speciality],[Resource Provider Tax ID],[Resource Provider Tax IDType],[Resource Provider NPI],[Resource Provider UPIN],[Resource Provider Is Provider],[Resource Provider UserType],[rowid],[ahprovId],[fileset_id]
)select
[Resource Provider Name],[Resource Provider ID],[Resource Provider Last Name],[Resource Provider First Name],[Resource Provider Middle Initial],[Resource Provider Full Name (First Name First)],[Resource Provider Full Address],[Resource Provider Address Line 1],[Resource Provider Address Line 2],[Resource Provider City],[Resource Provider State],[Resource Provider Phone],[Resource Provider Zip Code],[Resource Provider Gender],[Resource Provider Suffix],[Resource Provider Prefix],[Resource Provider Initials],[Resource Provider DEA No],[Resource Provider Fax No],[Resource Provider Speciality],[Resource Provider Tax ID],[Resource Provider Tax IDType],[Resource Provider NPI],[Resource Provider UPIN],[Resource Provider Is Provider],[Resource Provider UserType],[rowid],[ahprovId],[fileset_id]
from resProviderCurrent
set identity_insert resProvider off

set identity_insert vitals on
insert into vitals
([Encounter ID],[Patient ID],[Doctor ID],[Vital ID],[Encounter Date],[Weight],[Updated By],[Vital Name],[Visit Type],[Visit Status],[Facility ID],[Facility Name],[BP],[BMI],[Height],[Patient Name],[Patient Account #],[Patient Last Name],[Patient First Name],[Patient Middle Initial],[Patient Sex],[Patient DOB],[Patient Age],[Patient SSN],[Patient Race],[Patient Ethnicity],[Patient Address],[Patient Address 2],[Patient City],[Patient State],[Patient Zipcode],[rowid],[ahPtID],[ahEncID],[ahProvID],[fileset_id]
)select
[Encounter ID],[Patient ID],[Doctor ID],[Vital ID],[Encounter Date],[Weight],[Updated By],[Vital Name],[Visit Type],[Visit Status],[Facility ID],[Facility Name],[BP],[BMI],[Height],[Patient Name],[Patient Account #],[Patient Last Name],[Patient First Name],[Patient Middle Initial],[Patient Sex],[Patient DOB],[Patient Age],[Patient SSN],[Patient Race],[Patient Ethnicity],[Patient Address],[Patient Address 2],[Patient City],[Patient State],[Patient Zipcode],[rowid],[ahPtID],[ahEncID],[ahProvID],[fileset_id]
from vitalsCurrent
set identity_insert vitals off
