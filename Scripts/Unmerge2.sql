
select b.*,c.*
from
staging.dbo.patientsTobeUnmerged a
join personXidentifier b
on a.mbrID=b.personID
join personXidentifier c
on a.mbrID2=c.personID
where
b.sourceType in ('medicalClaims','RxClaims','Eligibility')
and
c.sourceType in ('medicalClaims','RxClaims','Eligibility')
order by b.create_date

begin transaction
update personXidentifier
set personID='BD3AB65F-29CC-4E45-98B6-B674BD2A621A'
where identifier='78D79612-F907-4B30-8B5E-5801B691C98E'

commit


begin transaction
insert into clinical.dbo.ptemployer
(ptid,employerID)
select
distinct 
a.ptid,'2D96F605-E237-4366-B3A6-CB89C1D07C16'
from
clinical.dbo.patient a
join personXidentifier b on a.ptid=b.PersonID
left join clinical.dbo.ptemployer c
on a.ptid=c.ptid
where
c.ptid is null
and
b.sourceSubtype in ('aetna')

commit

insert into clinical.dbo.ptidref2(ptid)
select
a.ptid 
from clinical.dbo.patient a
left join clinical.dbo.ptidref2 b
on a.ptid=b.ptid
where b.ptid is null