/*create table possibleDuplicate
(
personID1 uniqueidentifier,
personID2 uniqueidentifier,
reason varchar(256),
create_date datetime default getdate(),
obsolete_date datetime,
audit_id uniqueidentifier not null default newid() ROWGUIDCOL
)

*/
delete from possibleDuplicate
go
insert into possibleDuplicate
(personID1,personID2,reason)
select
a.ptid,b.ptid,'name,dob match'
from 
clinical.dbo.patient a 
join clinical.dbo.patient b
on a.ptFirst=b.ptFirst
and a.ptLast=b.ptLast
and a.birthDate=b.birthDate
join metadb.dbo.personXidentifier c
on a.ptid=c.personID and c.sourceType='ehr'
join metadb.dbo.personXidentifier d
on b.ptid=d.personId and d.sourceType='ehr'
where
a.ptStatus=b.ptStatus
and a.ptStatus='Active'
and a.ptid>b.ptid

insert into possibleDuplicate
(personID1,personID2,reason)
select
a.ptid,b.ptid,'fuzzy match 1'
		from
		clinical.dbo.patient a
		join clinical.dbo.patient b
		on 
		a.birthDate=b.birthDate
		left join possibleDuplicate c
		on a.ptid=c.personID1 and b.ptid=c.personID2
		where (soundex(a.ptFirst)=soundex(b.ptFirst) or soundex(a.ptLast)=soundex(b.ptlast)
		) and
		DIFFERENCE(a.ptFirst,b.ptFirst)=4 and
		DIFFERENCE(reverse(a.ptFirst),reverse(b.ptFirst))=4 and
		soundex(a.ptLast)=soundex(b.ptLast) and
		DIFFERENCE(a.ptLast,b.ptLast)=4 and
		DIFFERENCE(reverse(a.ptLast),REVERSE(b.ptLast))=4 
		and c.personID1 is null
		and a.mrn is not null and b.mrn is not null
		and a.ptstatus=b.ptstatus
		and a.ptstatus='Active'
		and a.ptid>b.ptid
		
		/*
		fuzzy match 2
		*/
		insert into possibleDuplicate
		(personID1,personID2,reason)
		select
		a.ptid,b.ptid,'Fuzzy Match 2'
		from
		clinical.dbo.patient a
		join clinical.dbo.patient b
		on 
		a.birthDate=b.birthDate
		left join possibleDuplicate c
		on a.ptid=c.personID1 and b.ptid=c.personID2
		where 
		LEFT(b.ptLast,case when LEN(b.ptLast)>len(a.ptLast) then len(a.ptLast) else len(b.ptLast) end)
		=
		LEFT(a.ptLast,case when LEN(b.ptLast)>len(a.ptLast) then len(a.ptLast) else len(b.ptLast) end)
		and
		LEFT(b.ptFirst,case when LEN(b.ptFirst)>len(a.ptFirst) then len(a.ptFirst) else len(b.ptFirst) end)
		=
		LEFT(a.ptFirst,case when LEN(b.ptFirst)>len(a.ptFirst) then len(a.ptFirst) else len(b.ptFirst) end)
		and c.personID1 is null
		and a.MRN is not null 
		and b.MRN is not null 
and a.ptstatus=b.ptstatus and a.ptstatus='Active'
		and a.ptid>b.ptid				
		/*
		fuzzy match 3
		*/
		
insert into possibleDuplicate
(personID1,personID2,reason)
select
a.ptid,b.ptid,'Fuzzy Match 4'
from
clinical.dbo.patient a
join Clinical.dbo.patient b
on 
a.birthDate=b.birthDate and a.ptlast=b.ptLast 
left join possibleDuplicate c
on a.ptid=c.personID1 and b.ptid=c.personID2
where 
b.ptFirst is null 
and c.personID1 is null
and a.ptstatus=b.ptstatus and a.ptstatus='Active'
and a.mrn is not null and b.mrn is not null
and a.ptid>b.ptid	

/*
remove space/-/. and compare fname
*/
		
insert into possibleDuplicate
(personID1,personID2,reason)
select
a.ptid,b.ptid,'Fuzzy Match 5'
from
Clinical.dbo.patient a
join Clinical.dbo.patient b
on 
a.birthDate=b.birthDate and a.ptLast=b.ptLast 
left join possibleDuplicate c
on a.ptid=c.personID1 and b.ptid=c.personID2
where 
replace(replace(replace(a.ptFirst,' ',''),'-',''),'.','')=replace(replace(replace(b.ptFirst,' ',''),'-',''),'.','')
and c.personID1 is null
and a.ptstatus=b.ptstatus and a.ptstatus='Active'
and a.MRN is not null and b.MRN is not null	
and a.ptid>b.ptid
/*
		remove space/-/. and compare lname
		*/
		

insert into possibleDuplicate
(personID1,personID2,reason)
select
a.ptid,b.ptid,'Fuzzy Match 6'
from
clinical.dbo.patient a
join clinical.dbo.patient b
on 
a.birthDate=b.birthDate and a.ptFirst=b.ptFirst 
left join possibleDuplicate c
on a.ptid=c.personID1 and b.ptid=c.personID2
where 
replace(replace(replace(a.ptLast,' ',''),'-',''),'.','')=replace(replace(replace(b.ptLast,' ',''),'-',''),'.','')
and c.personID1 is null
and b.mrn is not null and a.mrn is not null	
and a.ptstatus=b.ptstatus and a.ptstatus='Active'
and a.ptid>b.ptid
/*
write to PersonXIdentifier
*/



