declare
	@yearx int,
	@monthx int,
	@sdt datetime,
	@edt datetime,
	@curmonth int,
	@nextyear int

create table #temp
(
tempid int identity(1,1),
patient_id int,
charge_to_id int,
company_name varchar(256),
monthx int,
yearx int,
sdt datetime,
edt datetime,
sdate date,
edate date,
edays int,
yearxmonthx int,
pcp_id int,
ptid uniqueidentifier,
ahemployer_id uniqueidentifier,
pcpID uniqueidentifier,
insuranceCompany varchar(256),
insurancePlan varchar(256),
companyOffice varchar(256)
)
select @curmonth=datepart(mm,getdate())
select @nextyear=datepart(yyyy,getdate())+1

select @yearx=2010

while @yearx<@nextyear
begin
	select @monthx=1
	while @monthx<=12
	begin

		select @sdt=CAST(cast(@yearx as varchar(4))+'/'+right('00'+cast(@monthx as varchar),2)+'/01' as DATE)
		select @edt=dateadd(ss,-1,DATEADD(mm,1,@sdt))

		insert into #temp
		(patient_id,charge_to_id,company_name,monthx,yearx,sdt,edt,sdate,edate,pcp_id,insuranceCompany,insurancePlan,companyOffice)
		select 
		a.patient_id,a.employer_id,c.company_name,@monthx,@yearx,@sdt,@edt,a.period_start,isnull(a.period_end,@edt),a.primary_care_provider_id,g.insurance_company_name,h.insurance_plan_name,i.subgroup_name
		from
		emma.dbo.enrollment_period a
		left join emma.dbo.company c
		on a.employer_id=c.individual_id
		join staging.dbo.ql_patient_demographic e
		on a.patient_id=e.individual_id and e.obsolete_date is null
		left join clinical.dbo.stdbilling f
		on e.mbrID=f.ptid and f.monthx=@monthx and f.yearx=@yearx
		left join emma.dbo.insurance_company g
		on a.insurance_company_id=g.individual_id
		left join emma.dbo.insurance_plan h
		on a.insurance_company_id=h.insurance_company_id and a.insurance_plan_id=h.insurance_plan_id
		left join emma.dbo.vwValidSubgroups i
		on a.employer_id=i.company_id and a.insurance_sub_group_id=i.subgroup_id
		where
		a.period_start<@edt and
		(a.period_end is null or a.period_end>=@sdt) 
		and f.ptid is null



		select @monthx=@monthx+1

		if @yearx=@nextyear-1 and @monthx>@curmonth
			select @monthx=13
	end
	select @yearx=@yearx+1
end

delete a
from
#temp a join #temp b
on a.patient_id=b.patient_id and a.monthx=b.monthx and a.yearx=b.yearx
where
a.company_name is null and b.company_name is not null


update #temp
set edays=datediff(d,case when sdate<sdt then sdt else sdate end,case when edate>edt then edt else edate end)+1


delete a
from #temp a
join #temp b
on a.patient_id=b.patient_id and a.monthx=b.monthx and a.yearx=b.yearx 
where
a.edays<b.edays


update #temp
set yearxmonthx=(yearx*100)+monthx

update #temp
set ahemployer_id='69F2FFBC-96C8-4272-A174-1DDB699C1C42',company_name='Unknown'
where charge_to_id is null


update a
set ptid=b.mbrID
from #temp a
join staging.dbo.ql_patient_demographic b
on a.patient_id=b.individual_id
where
b.obsolete_date is null

update a
set ahemployer_id=b.ahemployer_id
from
#temp a
join staging.dbo.ql_employer b
on a.charge_to_id=b.individual_id

update a
set a.pcpID=b.ahprovider_id
from #temp a join staging.dbo.ql_provider_demographic b
on a.pcp_id=b.individual_id

/*

delete a
from #temp a 
join #temp b
on a.ptid=b.ptid and a.yearx=b.yearx and a.ahemployer_id!=b.ahemployer_id and 
a.ahemployer_id='69F2FFBC-96C8-4272-A174-1DDB699C1C42'


delete a
from #temp a
join #temp b
on a.patient_id=b.patient_id and a.monthx=b.monthx and a.yearx=b.yearx 
join emma.dbo.patient c
on b.patient_id=c.individual_id and b.charge_to_id=c.company_id
where
a.charge_to_id!=b.charge_to_id


delete a
from #temp a
join #temp b
on a.ptid=b.ptid and a.monthx=b.monthx and a.yearx=b.yearx 
where
a.charge_to_id>b.charge_to_id


delete a
from #temp a
join #temp b
on a.ptid=b.ptid and a.monthx=b.monthx and a.yearx=b.yearx 
where
a.tempid>b.tempid
*/
drop table staging.dbo.stdbilling_bkup
go
select * into staging.dbo.stdbilling_bkup from clinical.dbo.stdbilling
go
delete from clinical.dbo.stdbilling
go
declare
	@yearx int,
	@monthx int,
	@sdt datetime,
	@edt datetime,
	@curmonth int,
	@nextyear int

create table #temp
(
tempid int identity(1,1),
patient_id int,
charge_to_id int,
company_name varchar(256),
monthx int,
yearx int,
sdt datetime,
edt datetime,
sdate date,
edate date,
edays int,
yearxmonthx int,
pcp_id int,
ptid uniqueidentifier,
ahemployer_id uniqueidentifier,
pcpID uniqueidentifier,
insuranceCompany varchar(256),
insurancePlan varchar(256),
companyOffice varchar(256)
)
select @curmonth=datepart(mm,getdate())
select @nextyear=datepart(yyyy,getdate())+1

select @yearx=2010

while @yearx<@nextyear
begin
	select @monthx=1
	while @monthx<=12
	begin

		select @sdt=CAST(cast(@yearx as varchar(4))+'/'+right('00'+cast(@monthx as varchar),2)+'/01' as DATE)
		select @edt=dateadd(ss,-1,DATEADD(mm,1,@sdt))

		insert into #temp
		(patient_id,charge_to_id,company_name,monthx,yearx,sdt,edt,sdate,edate,pcp_id,insuranceCompany,insurancePlan,companyOffice)
		select 
		a.patient_id,a.employer_id,c.company_name,@monthx,@yearx,@sdt,@edt,a.period_start,isnull(a.period_end,@edt),a.primary_care_provider_id,g.insurance_company_name,h.insurance_plan_name,i.subgroup_name
		from
		emma.dbo.enrollment_period a
		left join emma.dbo.company c
		on a.employer_id=c.individual_id
		join staging.dbo.ql_patient_demographic e
		on a.patient_id=e.individual_id and e.obsolete_date is null
		left join emma.dbo.insurance_company g
		on a.insurance_company_id=g.individual_id
		left join emma.dbo.insurance_plan h
		on a.insurance_company_id=h.insurance_company_id and a.insurance_plan_id=h.insurance_plan_id
		left join emma.dbo.vwValidSubgroups i
		on a.employer_id=i.company_id and a.insurance_sub_group_id=i.subgroup_id
		where
		a.period_start<@edt and
		(a.period_end is null or a.period_end>=@sdt) 



		select @monthx=@monthx+1

		if @yearx=@nextyear-1 and @monthx>@curmonth
			select @monthx=13
	end
	select @yearx=@yearx+1
end

delete a
from
#temp a join #temp b
on a.patient_id=b.patient_id and a.monthx=b.monthx and a.yearx=b.yearx
where
a.company_name is null and b.company_name is not null


update #temp
set edays=datediff(d,case when sdate<sdt then sdt else sdate end,case when edate>edt then edt else edate end)+1


delete a
from #temp a
join #temp b
on a.patient_id=b.patient_id and a.monthx=b.monthx and a.yearx=b.yearx 
where
a.edays<b.edays


update #temp
set yearxmonthx=(yearx*100)+monthx

update #temp
set ahemployer_id='69F2FFBC-96C8-4272-A174-1DDB699C1C42',company_name='Unknown'
where charge_to_id is null


update a
set ptid=b.mbrID
from #temp a
join staging.dbo.ql_patient_demographic b
on a.patient_id=b.individual_id
where
b.obsolete_date is null

update a
set ahemployer_id=b.ahemployer_id
from
#temp a
join staging.dbo.ql_employer b
on a.charge_to_id=b.individual_id

update a
set a.pcpID=b.ahprovider_id
from #temp a join staging.dbo.ql_provider_demographic b
on a.pcp_id=b.individual_id

delete a
from #temp a 
join #temp b
on a.ptid=b.ptid and a.yearx=b.yearx and a.monthx=b.monthx
where
a.tempid<b.tempid


insert into clinical.dbo.stdbilling
(ptID,monthx,yearx,yearxmonthx,billed,employerId,loadID,pcpID,insuranceCompany,insurancePlan,companyOffice)
select
distinct
ptID,monthx,yearx,yearxmonthx,1,ahemployer_id,1,pcpID,insuranceCompany,insurancePlan,companyOffice
from
#temp


update a
set a.pcpID=b.pcpID
from 
clinical.dbo.stdbilling a
join clinical.dbo.patient b
on a.ptid=b.ptid
where
a.pcpid is null and b.pcpid is not null



update a
set a.insurancePlan=c.insurance_plan_name
from
clinical.dbo.stdbilling a
join staging.dbo.ql_patient_demographic b
on a.ptID=b.mbrID  
join emma.dbo.insurance_plan c
on b.insurance_plan_id=c.insurance_plan_id
where
(a.insurancePlan is null or
a.insurancePlan='Unknown')
and b.obsolete_date is null

update a
set a.insuranceCompany=c.insurance_company_name
from
clinical.dbo.stdbilling a
join Staging.dbo.ql_patient_demographic b
on a.ptID=b.mbrID
join emma.dbo.insurance_company c
on b.insurance_company_id=c.individual_id
where 
(
a.insuranceCompany is null or a.insuranceCompany='Unknown')
and b.obsolete_date is null

update a
set a.companyOffice=c.office_name
from
clinical.dbo.stdbilling a
join Staging.dbo.ql_patient_demographic b
on a.ptID=b.mbrID
join emma.dbo.company_office c
on b.company_office_id=c.individual_id
where
(a.companyOffice is null  or
a.companyOffice='Unknown')
and b.obsolete_date is null

update clinical.dbo.stdbilling
set companyOffice='Unknown'
where companyOffice is null

update clinical.dbo.stdbilling
set insuranceCompany='Unknown'
where insuranceCompany is null

update clinical.dbo.stdbilling
set insurancePlan='Unknown'
where insurancePlan is null



