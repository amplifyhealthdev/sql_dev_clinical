--
--Update ptID to the table when the table has the encounterid and patient id is null
--
/*
apptsProviderCurrent
diagCodesCurrent
vitalsCurrent
referralsCurrent
procCodeAnalysisCurrent
problemListCurrent
labsCurrent
immunizationsCurrent
*/
--
--logic
--
--the ahptID exists for all records in apptsProviderCurrent
--but may not be the case on other tables
--
begin transaction
update a
set a.ahptID=b.ptID
from
diagCodesCurrent a
join Clinical2.dbo.ptencounter b
on a.ahencID=b.encounterID
where
a.ahptID is null

update a
set a.ahptID=b.ptID
from
vitalsCurrent a
join Clinical2.dbo.ptencounter b
on a.ahencID=b.encounterID
where
a.ahptID is null

update a
set a.ahptID=b.ptID
from
referralsCurrent a
join Clinical2.dbo.ptencounter b
on a.ahencID=b.encounterID
where
a.ahptID is null

update a
set a.ahptID=b.ptID
from
procCodeAnalysisCurrent a
join Clinical2.dbo.ptencounter b
on a.ahencID=b.encounterID
where
a.ahptID is null

update a
set a.ahptID=b.ptID
from
problemListCurrent a
join Clinical2.dbo.ptencounter b
on a.ahencID=b.encounterID
where
a.ahptID is null

update a
set a.ahptID=b.ptID
from
labsCurrent a
join Clinical2.dbo.ptencounter b
on a.ahencID=b.encounterID
where
a.ahptID is null

update a
set a.ahptID=b.ptID
from
immunizationsCurrent a
join Clinical2.dbo.ptencounter b
on a.ahencID=b.encounterID
where
a.ahptID is null


commit

