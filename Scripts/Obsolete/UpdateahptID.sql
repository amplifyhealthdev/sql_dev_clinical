--update ahptID
--
update a
set a.ahptID=b.ptID
from
patientsCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient ID]=b.srcptid

update a
set a.ahptID=b.ptID
from
apptsProviderCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient ID]=b.srcptid


--demoPCP

update a
set a.ahptID=b.ptID
from
demoPCPCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient ID]=b.srcptid



--diagcodes

update a
set a.ahptID=b.ptID
from
diagcodesCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient ID]=b.srcptid

--immunizations
--

update a
set a.ahptID=b.ptID
from
immunizationsCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient Account No]=b.srcptid


--
--labs

update a
set a.ahptID=b.ptID
from
labsCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient Account Number]=b.srcptid


--
--problemList
--

update a
set a.ahptID=b.ptID
from
problemListCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient ID]=b.srcptid

--
--proccodeanalysis
--

update a
set a.ahptID=b.ptID
from
procCodeAnalysisCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient Account #]=b.srcptid


--
--referral
--

update a
set a.ahptID=b.ptID
from
referralsCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient ID]=b.srcptid


--
--vitals
--
update a
set a.ahptID=b.ptID
from
vitalsCurrent a,Clinical2.dbo.ptmasterindex b
where
a.[Patient ID]=b.srcptid

