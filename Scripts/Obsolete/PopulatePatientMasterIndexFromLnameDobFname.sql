--match based on first name, last name, dob
--eliminate those which have more than one ptID
--
declare
	@loadID int
	
select @loadID=1000

begin transaction

begin
	--adding monovie ID clause to return one row. to be revisited
	select a.[Patient Account Number],c.ptID
	into #ttab1
	from RawEHR_MonaVie.dbo.patientsCurrent a 
	left join Clinical2.dbo.ptmasterindex b
	on a.[Patient ID]=b.srcptid and src_type='Monavie'
	join Clinical.dbo.srcptmap c
	on a.[Patient Last Name]=c.last
	and CONVERT(varchar,a.[Patient Date of Birth],112)=c.dob
	and a.[Patient First Name]=c.first
	where
	b.ptmasterindexid is null and c.ecwid_monavie is not null
	
	--insert to master index based on this
	
	insert into ptmasterindex
	(ptID,src_type,loadID,match_type,srcptid)
	select
	distinct ptID,'Monavie',@loadID,'Lname,Fname,Dob',[Patient Account Number]
	from #ttab1
	
	
	insert into ptmasterindexcol
	(ptmasterindexID,loadID,colName,colOrder,colVal)
	select
	a.ptmasterindexID,1,'Patient ID',1,a.srcptid
	from
	ptmasterindex a,#ttab1 b
	where
	a.ptID=b.ptID and a.srcptid=b.[Patient Account Number] 
end


