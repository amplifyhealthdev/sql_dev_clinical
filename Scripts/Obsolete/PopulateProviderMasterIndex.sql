declare
	@loadID int
	
select @loadID=1000

begin transaction

insert into Clinical2.dbo.provmasterindex
(
provID,srcprovid,src_type,loadID
)
select
b.provID,a.[Practice Provider ID],'Monavie',@loadID
from
practiceProvidersCurrent a
join Clinical.dbo.srcprovmap b
on a.[Practice Provider ID]=b.ecwid_monavie
left join Clinical2.dbo.provmasterindex c
on b.provID=c.provID
where c.provID is null

insert into  Clinical2.dbo.provmasterindex
(
provID,srcprovid,src_type,loadID
)
select
b.provID,a.[Demographics Rendering Provider ID],'Monavie',@loadID
from
demoRendPrvdCurrent a
join Clinical.dbo.srcprovmap b
on a.[Demographics Rendering Provider ID]=b.ecwid_monavie
left join Clinical2.dbo.provmasterindex c
on b.provID=c.provID
where
c.provID is null

update a
set a.ahprovID=b.provID
from
demoRendPrvdCurrent a
join Clinical2.dbo.provmasterindex b
on a.[Demographics Rendering Provider ID]=b.srcprovid

update a
set a.ahprovID=b.provID
from practiceProvidersCurrent a
join Clinical2.dbo.provmasterindex b
on a.[Practice Provider ID]=b.srcprovid






commit
