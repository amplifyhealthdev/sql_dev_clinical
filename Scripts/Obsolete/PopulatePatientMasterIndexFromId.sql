--load incremental patient master index
--
declare
	@loadID int
	
select @loadID=1000

begin transaction

create table #ptxref
(
ptID uniqueidentifier,
srcptid varchar(20)
)
insert into #ptxref
(ptID,srcptid)
select
b.ptID,a.[Patient ID]
from
patientsCurrent a
join Clinical.dbo.srcptmap b
on a.[Patient ID]=b.ecwid_monavie
left join Clinical2.dbo.ptmasterindex c
on a.[Patient ID]=c.srcptid and b.ptID=c.ptID
where c.ptID is null

update b
set b.ptID=c.ptID
from
patientsCurrent a
join #ptxref b
on a.[Patient ID]=b.srcptid
join #ptxref c
on a.[Responsible Party Account Number]=c.srcptid
where
a.[Patient Delete Flag]=1 and
a.[Patient ID] != [Responsible Party Account Number]

insert into #ptxref
(ptID,srcptid)
select
c.ptID,a.[Patient ID]
from
patientsCurrent a
left join Clinical.dbo.srcptmap b
on a.[Patient ID]=b.ecwid_monavie
join Clinical.dbo.srcptmap c
on a.[Responsible Party Account Number]=c.ecwid_monavie
left join #ptxref d
on a.[Patient ID]=d.srcptid
where
b.ptID is null
and
d.ptID is null


insert into Clinical2.dbo.ptmasterindex
(ptID,src_type,srcptid,loadID)
select
distinct ptID,'Monavie',srcptid,@loadID
from #ptxref


insert into Clinical2.dbo.ptmasterindexcol
(ptmasterindexID,loadID,colName,colOrder,colVal)
select
a.ptmasterindexID,@loadID,'Patient ID',1,b.srcptid
from
Clinical2.dbo.ptmasterindex a,#ptxref b
where
a.ptID=b.ptID and a.srcptid=b.srcptid

--rollback

commit


