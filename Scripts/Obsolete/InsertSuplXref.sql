declare
	@loadID int
select @loadID=1000

begin transaction
insert into supl_encounterXref
(
encounterID,srcid,src_data,src_col,load_id
)
select distinct newid(),a.[Vital ID],'Vitals','Vital ID',@loadID
from
vitalsCurrent a
left join encounterXref b
on a.[Encounter ID]=b.srcencounterid
where
b.encounterID is null

update a
set a.ahEncID=b.encounterID
from
vitalsCurrent a
join supl_encounterXref b
on a.[Vital ID]=b.srcid
and b.src_data='Vitals'
where
a.ahEncID is null


insert into supl_encounterXref
(
encounterID,srcid,src_data,src_col,load_id
)
select distinct newid(),a.[Referral ID],'Referrals','Referral ID',@loadID
from
referralsCurrent a
left join encounterXref b
on a.[Referral Encounter ID]=b.srcencounterid
where
b.encounterID is null


update a
set a.ahEncID=b.encounterID
from
referralsCurrent a
join supl_encounterXref b
on a.[Referral ID]=b.srcid
and b.src_data='Referrals'
where
a.ahEncID is null

insert into supl_encounterXref
(
encounterID,srcid,src_data,src_col,load_id
)
select distinct newid(),a.[Encounter ID],'ProblemListCurrent','Encounter ID',@loadID
from
problemListCurrent a
left join encounterXref b
on a.[Encounter ID]=b.srcencounterid
where
b.encounterID is null


update a
set a.ahEncID=b.encounterID
from
problemListCurrent a
join supl_encounterXref b
on a.[Encounter ID]=b.srcid
and b.src_data='ProblemListCurrent'
where
a.ahEncID is null

insert into supl_encounterXref
(
encounterID,srcid,src_data,src_col,load_id
)
select distinct newid(),a.[Encounter ID],'labsCurrent','Encounter ID',@loadID
from
labsCurrent a
left join encounterXref b
on a.[Encounter ID]=b.srcencounterid
where
b.encounterID is null


update a
set a.ahEncID=b.encounterID
from
labsCurrent a
join supl_encounterXref b
on a.[Encounter ID]=b.srcid
and b.src_data='labsCurrent'
where
a.ahEncID is null

insert into supl_encounterXref
(
encounterID,srcid,src_data,src_col,load_id
)
select distinct newid(),a.[Immunization ID],'immunizationsCurrent','Immunization ID',@loadID
from
immunizationsCurrent a
left join encounterXref b
on a.[Encounter ID]=b.srcencounterid
where
b.encounterID is null



update a
set a.ahEncID=b.encounterID
from
immunizationsCurrent a
join supl_encounterXref b
on a.[Immunization ID]=b.srcid
and b.src_data='immunizationsCurrent'
where
a.ahEncID is null


commit
