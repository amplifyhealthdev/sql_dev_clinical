
update a
set a.ahEncID=b.encounterID
from
diagCodesCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid
and a.ahEncID is null

update a
set a.ahEncID=b.encounterID
from
VitalsCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid
and a.ahEncID is null

update a
set a.ahEncID=b.encounterID
from
procCodeAnalysisCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid
and a.ahEncID is null

update a
set a.ahEncID=b.encounterID
from
problemListCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid
and a.ahEncID is null

update a
set a.ahEncID=b.encounterID
from
labsCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid
and a.ahEncID is null

update a
set a.ahEncID=b.encounterID
from
referralsCurrent a,encounterXref b
where
a.[Referral Encounter ID]=b.srcencounterid
and a.ahEncID is null
