--update ahID
--
--provider
update a
set a.ahprovID=b.provID
from
practiceProvidersCurrent a,ProviderXref b
where
a.[Practice Provider ID]=b.srcprovid


--apptsProvider
update a
set a.ahencID=b.encounterID
from
apptsProviderCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid

update a
set a.ahptID=b.ptID
from
apptsProviderCurrent a,PatientXref b
where
a.[Patient ID]=b.srcptid

update a
set a.ahprovID=b.provID
from
apptsProviderCurrent a,ProviderXref b
where
a.[Appointment Provider ID]=b.srcprovid

--demoPCP


update a
set a.ahprovID=b.providerID
from
demoPCPCurrent a,Clinical.dbo.provider b
where
a.[Demographics PCP NPI]=b.NPI

--demoRendPrvd
update a
set a.ahprovID=b.provID
from
demoRendPrvdCurrent a,ProviderXref b
where
a.[Demographics Rendering Provider ID]=b.srcprovid

--diagcodes
update a
set a.ahencID=b.encounterID
from
diagcodesCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid


update a
set a.ahprovID=b.provID
from
diagcodesCurrent a,ProviderXref b
where
a.[Provider ID]=b.srcprovid

--immunizations
--
update a
set a.ahencID=b.encounterID
from
immunizationsCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid


update a
set a.ahprovID=b.provID
from
immunizationsCurrent a,Clinical2.dbo.provmasterindex b
where
a.[Appointment Provider]=b.srcprovid

--
--labs
update a
set a.ahencID=b.encounterID
from
labsCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid

update a
set a.ahprovID=b.provID
from
labsCurrent a,Clinical2.dbo.provmasterindex b
where
a.[Appointment Provider ID]=b.srcprovid

--
--problemList
--
update a
set a.ahencID=b.encounterID
from
problemListCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid

update a
set a.ahprovID=b.provID
from
problemListCurrent a,Clinical2.dbo.provmasterindex b
where
a.[Appointment Provider ID]=b.srcprovid
--
--proccodeanalysis
--
update a
set a.ahencID=b.encounterID
from
procCodeAnalysisCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid

update a
set a.ahprovID=b.provID
from
procCodeAnalysisCurrent a,Clinical2.dbo.provmasterindex b
where
a.[Doctor ID]=b.srcprovid

--
--referral
--

update a
set a.ahencID=b.encounterID
from
referralsCurrent a,encounterXref b
where
a.[Referral Encounter ID]=b.srcencounterid

update a
set a.ahprovID=b.provID
from
referralsCurrent a,Clinical2.dbo.provmasterindex b
where
a.[Referral From Doctor Provider ID]=b.srcprovid

update a
set a.ahToprovID=b.provID
from
referralsCurrent a,Clinical2.dbo.provmasterindex b
where
a.[Referral To Doctor Provider ID]=b.srcprovid

--
--resProvider
--
update a
set a.ahprovID=b.provID
from
resProviderCurrent a,Clinical2.dbo.provmasterindex b
where
a.[Resource Provider ID]=b.srcprovid

--
--vitals
--
update a
set a.ahencID=b.encounterID
from
vitalsCurrent a,encounterXref b
where
a.[Encounter ID]=b.srcencounterid

update a
set a.ahprovID=b.provID
from
vitalsCurrent a,Clinical2.dbo.provmasterindex b
where
a.[Doctor ID]=b.srcprovid
--update ahID
--
------------------------------------
