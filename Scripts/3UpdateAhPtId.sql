--update ahptID
--
declare
	@fileset_id uniqueidentifier,
	@sourceType varchar(256),
	@sourceSubtype varchar(256)
	
	

begin transaction
	select top 1 @fileset_id=fileset_id from labsCurrent 
	
	select @sourceType=sourceType,@sourceSubtype=sourceSubtype
	from metaDB.dbo.filesetDetail
	where fileset_id=@fileset_id
	
	
update patientsCurrent 
set ahptId=mbrID


	


--update patient ID in encounter table where it is null and in others it is not

	update a
	set a.[Patient ID]=b.[Patient ID]
	from apptsProviderCurrent a
	join diagCodesCurrent b
	on a.[Encounter ID]=b.[Encounter ID]
	where
	a.[Patient ID] is null and b.[Patient ID] is not null
	
	update a
	set a.[Patient ID]=b.[Patient Account No]
	from apptsProviderCurrent a
	join immunizationsCurrent b
	on a.[Encounter ID]=b.[Encounter ID]
	where
	a.[Patient ID] is null and b.[Patient Account No] is not null
	
	update a
	set a.[Patient ID]=b.[Patient Account Number]
	from apptsProviderCurrent a
	join labsCurrent b
	on a.[Encounter ID]=b.[Encounter ID]
	where
	a.[Patient ID] is null and b.[Patient Account Number] is not null

	update a
	set a.[Patient ID]=b.[Patient ID]
	from apptsProviderCurrent a
	join vitalsCurrent b
	on a.[Encounter ID]=b.[Encounter ID]
	where
	a.[Patient ID] is null and b.[Patient ID] is not null
	


update a
set a.ahptID=b.personID
from
apptsProviderCurrent a,metaDB.dbo.ptkeyset b
where
a.[Patient ID]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null
	

--demoPCP

update a
set a.ahptID=b.personId
from
demoPCPCurrent a,metaDB.dbo.ptkeyset b
where
a.[Patient ID]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null
	


--diagcodes

update a
set a.ahptID=b.personId
from
diagcodesCurrent a,metaDB.dbo.ptkeyset b
where
a.[Patient ID]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null
	

--immunizations
--

update a
set a.ahptID=b.personId
from
immunizationsCurrent a,metaDB.dbo.ptkeyset b
where
a.[Patient Account No]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null


--
--labs

update a
set a.ahptID=b.personId
from
labsCurrent a,metaDB.dbo.ptkeyset  b
where
a.[Patient Account Number]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null


--
--problemList
--

update a
set a.ahptID=b.personId
from
problemListCurrent a,metaDB.dbo.ptkeyset b
where
a.[Patient ID]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null

--
--proccodeanalysis
--

update a
set a.ahptID=b.personId
from
procCodeAnalysisCurrent a,metaDB.dbo.ptkeyset  b
where
a.[Patient Account #]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null


--
--referral
--

update a
set a.ahptID=b.personId
from
referralsCurrent a,metaDB.dbo.ptkeyset  b
where
a.[Patient ID]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null


--
--vitals
--
update a
set a.ahptID=b.personId
from
vitalsCurrent a,metaDB.dbo.ptkeyset  b
where
a.[Patient ID]=b.key1 and b.sourceSubtype=@sourceSubtype and b.sourceType=@sourcetype
and a.ahPtID is null

--commit
