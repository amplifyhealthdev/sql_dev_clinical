declare
	@fileset_id uniqueidentifier,
	@sourceType varchar(256),
	@sourceSubtype varchar(256)

begin transaction

	select top 1 @fileset_id=fileset_id from apptsProviderCurrent 
	
	select @sourceType=sourceType,@sourceSubtype=sourceSubtype
	from metaDB.dbo.filesetDetail
	where fileset_id=@fileset_id
	
-- ptencdiagnosis: encdiagID encounterID diag loadID
insert into clinical.dbo.ptencdiagnosis
(
encounterID,
diag,
fileset_id
)
select  d.ahencID, rtrim(ltrim(d.[ICD-9 Code])), @fileset_id
from diagCodesCurrent d 
left join clinical.dbo.ptencdiagnosis encd
on d.ahencID=encd.encounterId
where 
encd.encounterID is null
and d.ahencID is not null
and d.ahptId is not null
--
-- from ProcCodeAnalysis
--

create table #pcac
(
ahencID uniqueidentifier,
ahptID uniqueidentifier,
diagcd varchar(20)
)

insert into #pcac
(ahencID,ahptID,diagcd)
select
ahencID,ahptID,[ICD-9 Code 1]
from procCodeAnalysisCurrent
where
len([ICD-9 Code 1])>0


insert into #pcac
(ahencID,ahptID,diagcd)
select
ahencID,ahptID,[ICD-9 Code 2]
from procCodeAnalysis
where
len([ICD-9 Code 2])>0


insert into #pcac
(ahencID,ahptID,diagcd)
select
ahencID,ahptID,[ICD-9 Code 3]
from procCodeAnalysis
where
len([ICD-9 Code 3])>0


insert into #pcac
(ahencID,ahptID,diagcd)
select
ahencID,ahptID,[ICD-9 Code 4]
from procCodeAnalysis
where
len([ICD-9 Code 4])>0


	insert into clinical.dbo.ptencdiagnosis
	(encounterID,diag,fileset_id)
	select
	distinct a.ahencID,a.diagcd,@fileset_id
	from #pcac a 
	join refdb.dbo.icd9_ama c
	on a.diagcd=c.icd9Code
	join clinical.dbo.ptencounter d
	on a.ahencID=d.encounterID
	left join clinical.dbo.ptencdiagnosis b
	on a.ahencID=b.encounterID
	and a.diagcd=b.diag
	where
	b.encdiagID is null
	


--
--immunizations
--
update immunizationsCurrent
set Dosage=null
where len(Dosage)=0 and Dosage is not null

update immunizationsCurrent
set [Dosage No]=null
where len([Dosage No])=0 and [Dosage No] is not null

insert clinical.dbo.ptimmunization 
(
ptID,
immunType,
immunDate,
provID,
dosage,
doseNumber,
encounterID,
fileset_id
)
select 
distinct
imm.ahptID,
imm.[Item Name],
coalesce(imm.[Given Date],pte.apptStart),
imm.ahprovID, 
imm.Dosage, 
imm.[Dosage No], 
imm.ahencID, 
@fileset_id
from immunizationsCurrent imm 
join clinical.dbo.ptencounter pte
on imm.ahencID=pte.encounterID
left join clinical.dbo.ptimmunization ptie
on imm.ahEncID=ptie.encounterID and
imm.[Item Name]=ptie.immunType
where
ptie.encounterID is null
and imm.ahencID is not null
and imm.ahptId is not null

--lab
--
update labsCurrent
set [Lab Attribute]=null
where
len([Lab Attribute])=0 and [Lab Attribute] is not null

update labsCurrent
set [Lab Attribute Value]=null
where
len([Lab Attribute Value])=0 and [Lab Attribute Value] is not null

update LabsCurrent
set [Lab Result]=null
where
len([Lab Result])=0 and [Lab Result] is not null

insert into clinical.dbo.ptlab
(
ptID,
provID,
encounterID,
testID,
labName,
labAttribute,
datePerformed,
labValue,
labResult,
highLowNormal,
fileset_id
)
select 
distinct
s.ahptID, 
s.ahprovID, 
s.ahencID, 
s.[Report ID], 
s.[Lab Name], 
s.[Lab Attribute],  
pte.apptStart,  
s.[Lab Attribute Value], 
s.[Lab Result], 
s.[Lab Priority],  
@fileset_id
from 
labsCurrent  s 
join clinical.dbo.ptencounter pte
on s.ahencID=pte.encounterID
left join clinical.dbo.ptlab ptl
on s.ahencID=ptl.encounterID and s.[Lab Attribute]=ptl.labAttribute
where 
ptl.encounterID is null
and s.ahencID is not null
and s.ahptId is not null

--referral
--
insert clinical.dbo.ptreferral
(
ptID,
referrerID,
referredToID,
dateReferred,
referralStatus,
referralSpecialty,
referralType,
encounterID,
fileset_id
)
select 
distinct
s.ahptID, 
s.ahprovID, 
s.ahtoprovID, 
coalesce(s.[Referral Date],pte.apptStart), 
upper(LEFT(s.[Referral Status],1))+RIGHT(s.[Referral Status],len(s.[Referral Status])-1), 
s.Speciality, 
s.[Unit Type], 
s.ahencid, 
@fileset_id 
from referralsCurrent s
join clinical.dbo.ptencounter pte
on s.ahencID=pte.encounterID
left join clinical.dbo.ptreferral ptr
on s.ahencID=ptr.encounterID
and s.ahprovID=ptr.referrerID
and s.ahtoprovID=ptr.referredToID
where
ptr.encounterID is null
and s.ahencID is not null
and s.ahptId is not null
--
--vitals
--
declare
	@t1 table 
	(
	encId uniqueidentifier,
	systolic varchar(256),
	diastolic varchar(256),
	ctr int
	)
declare
	@encID uniqueidentifier,
	@bp varchar(256)
	
declare c1 cursor for select ahencID,bp from vitalsCurrent where [Vital Name]='vital_bp' and bp is not null

open c1
fetch c1 into @encID,@bp
while @@fetch_status=0
begin
	insert into @t1
	select encID,systolic,diastolic,ctr from refdb.dbo.udfSplitBP(@encID,@bp)
	
	fetch c1 into @encID,@bp
end
close c1
deallocate c1

insert into clinical.dbo.ptvital
(
ptID,
encounterID,
encounterDate,
weight,
height,
diastolicBP,
systolicBP,
vitalDate,
bmi,
fileset_id
)
select 
DISTINCT  h.ahptID, h.ahencID, h.[Encounter Date], 
refdb.dbo.weightToLbs(w.Weight), refdb.dbo.heightToInches(h.height), 
s.systolic, s.diastolic, h.[Encounter Date], b.bmi, 
@fileset_id
 from vitalsCurrent x
 left join vitalsCurrent h on x.ahencid = h.ahencid and h.[Vital Name] = 'vital_ht' 
 left join @t1 s on x.ahencid = s.encId
 left join vitalsCurrent w on x.ahencid = w.ahencid and w.[Vital Name] = 'vital_wt' 
 left join vitalsCurrent b on x.ahencid = b.ahencid and b.[Vital Name] = 'vital_bmi'
 join clinical.dbo.ptencounter pte
 on x.ahencId=pte.encounterID
 left join clinical.dbo.ptvital ptv
 on x.ahencID=ptv.encounterID
 where 
 ptv.encounterID is null
 and h.ahencID is not null
 --
 --
 --ptdiagnosis
 --

--already existing
--
update a
set a.diagDate=case when isnull(
	case when isdate(b.[Onset Date])=0 then null else b.[onset Date] end,b.[Encounter Date]) < b.[Encounter Date] then
							isnull(case when isdate(b.[Onset Date])=0 then null else b.[onset Date] end,b.[Encounter Date])
							else b.[Encounter Date] end
from
clinical.dbo.ptdiagnosis a
join problemlistCurrent b
on a.ptID=b.ahptId and a.diagCode=b.[Assessment Code]
where
a.diagDate>case when isnull(case when isdate(b.[Onset Date])=0 then null else b.[onset Date] end,b.[Encounter Date]) < b.[Encounter Date] then
							isnull(case when isdate(b.[Onset Date])=0 then null else b.[onset Date] end,b.[Encounter Date])
							else b.[Encounter Date] end
							
update a
set a.dateResolved=b.[Encounter Date]
from
clinical.dbo.ptdiagnosis a
join problemlistCurrent b
on a.ptID=b.ahptId and a.diagCode=b.[Assessment Code]
where
a.dateResolved is null and
b.[Inactive Flag]=1

--new ones
--
insert into clinical.dbo.ptdiagnosis
(ptID,diagCode,diagDate)
select
distinct
ahptId,[Assessment Code],case when isnull(case when isdate(a.[Onset Date])=0 then null else a.[onset Date] end,a.[Encounter Date]) < a.[Encounter Date] then
							isnull(case when isdate(a.[Onset Date])=0 then null else a.[onset Date] end,a.[Encounter Date])
							else a.[Encounter Date] end
from
problemlistCurrent a
left join clinical.dbo.ptdiagnosis b
on a.ahptId=b.ptID and a.[Assessment Code]=b.diagCode
where
b.ptID is null

	update a
	set a.diagDate=c.apptStart
	from
	clinical.dbo.ptdiagnosis a
	join clinical.dbo.ptencdiagnosis b
	on a.diagCode=b.diag
	join clinical.dbo.ptencounter c
	on b.encounterID=c.encounterID
	and a.ptID=c.ptID
	where
	a.diagDate<c.apptStart
	
	insert into clinical.dbo.ptdiagnosis
	(ptID,diagCode,diagDate)
	select
	a.ptID,a.diag,startDate
	from
	(
	select x.ptID,y.diag,MIN(x.apptStart) as startDate
	from
	clinical.dbo.ptencounter x
	join clinical.dbo.ptencdiagnosis y
	on x.encounterID=y.encounterID
	group by x.ptID,y.diag
	) a
	left join clinical.dbo.ptdiagnosis b
	on a.ptID=b.ptID and a.diag=b.diagCode
	where
	b.ptID is null
	
create table #pcac1
(
ahencID uniqueidentifier,
code varchar(20),
code_type varchar(20)
)
	
	
	insert into #pcac1
	(ahencID,code)
	select
	distinct ahencID,[CPT Code]
	from
	procCodeAnalysisCurrent
	where LEN([CPT Code])>0
	
	
	update a
	set a.code_type='custom'
	from
	#pcac1 a
	join refdb.dbo.phcustomcpt b
	on a.code=b.phcode
	
	
	update a
	set a.code_type='CPT'
	from
	#pcac1 a
	join refdb.dbo.ahtagsCPT b
	on a.code=b.cptcode
	
	
	update a
	set a.code_type='HCPCS'
	from
	#pcac1 a
	join refdb.dbo.ahtagsHCPCS b
	on a.code=b.code


	insert into clinical.dbo.ptencprocedure
	(encounterID,cpt)
	select
	a.ahencID,a.code 
	from
	#pcac1 a
	join clinical.dbo.ptencounter c
	on a.ahencID=c.encounterID
	left join clinical.dbo.ptencprocedure b
	on a.ahencID=b.encounterID and a.code=b.cpt
	where
	a.code_type='CPT' and b.encounterID is null
	
	
	insert into clinical.dbo.ptencprocedure
	(encounterID,hcpcs)
	select
	a.ahencID,a.code 
	from
	#pcac1 a
	join clinical.dbo.ptencounter c
	on a.ahencID=c.encounterID
	left join clinical.dbo.ptencprocedure b
	on a.ahencID=b.encounterID and a.code=b.hcpcs
	where
	a.code_type='HCPCS' and b.encounterID is null
	

	insert into clinical.dbo.ptencprocedure
	(encounterID,customproc)
	select
	a.ahencID,a.code 
	from
	#pcac1 a
	join clinical.dbo.ptencounter c
	on a.ahencID=c.encounterID
	left join clinical.dbo.ptencprocedure b
	on a.ahencID=b.encounterID and a.code=b.customproc
	where
	a.code_type='custom' and b.encounterID is null
		
	
	
		
	
 
/* 
insert into clinical.dbo.employer
(
employerID,
employerName,
address1,
address2,
city,
state,
zip,
ExtZip,
fileset_id
)
select 
NEWID(),
pr.EmployerName,
pr.EmployerAddress,
pr.EmployerAddress2,
pr.EmployerCity,
pr.EmployerState,
pr.EmployerZip,
pr.EmployerZipExt,  
@fileset_id
from 
(
select
rtrim(ltrim([Employer Name])) as EmployerName,  
rtrim(ltrim([Employer Address])) as EmployerAddress, 
rtrim(ltrim([Employer Address 2])) as EmployerAddress2,
rtrim(ltrim([Employer City])) as EmployerCity, 
substring(rtrim(ltrim([Employer State])), 1, 2) as EmployerState, 
rtrim(ltrim([Employer Zipcode])) as EmployerZip,                         
rtrim(ltrim([Employer Zipcode])) as EmployerZipExt
from problemListCurrent
where [Employer Name] is not null
union
select
DISTINCT 
rtrim(ltrim([Employer Name])) as EmployerName,  
rtrim(ltrim([Employer Address])) as EmployerAddress, 
rtrim(ltrim([Employer Address 2])) as EmployerAddress2,
null as EmployerCity, 
substring(rtrim(ltrim([Employer State])), 1, 2) as EmployerState, 
rtrim(ltrim([Employer Zipcode])) as EmployerZip,                         
rtrim(ltrim([Employer Zipcode])) as EmployerZipExt
from procCodeAnalysisCurrent
where [Employer Name] is not null
)  pr 
left join clinical.dbo.employer emp
on pr.EmployerName=emp.employerName
where 
 emp.employerName is null

insert into clinical.dbo.ptemployer
(
ptEmplId,
ptID,
employerID,
fileset_id
)
select
newid(),
com.ahptID,
emp.employerID,
@fileset_id
from
(
select ahptID,rtrim(ltrim([Employer Name])) as EmployerName
from problemListCurrent 
union
select ahptId,rtrim(ltrim([Employer Name])) as EmployerName
from proccodeAnalysisCurrent
) as com
join clinical.dbo.employer emp
on com.employerName=emp.employerName
left join clinical.dbo.ptemployer pte
on com.ahptID=pte.ptID and
emp.employerID=pte.employerID
where
pte.employerID is null

*/

update a
set a.ptStatus=ltrim(rtrim(b.[Patient Status]))
from
clinical.dbo.patient a
join patientsCurrent b
on a.ptID=b.ahptID
where
a.ptStatus is null
or 
a.ptStatus!=ltrim(rtrim(b.[Patient Status]))

	update a
	set a.encounterStatus='Complete'
	from
	clinical.dbo.ptencounter a
	join clinical.dbo.ptlab b
	on a.encounterID=b.encounterID 
	where
	a.encounterType='LAB' and 
	(a.encounterStatus is null or a.encounterStatus='Pending') and
	(b.labResult is not null or b.labValue is not null)
	
	update clinical.dbo.ptencounter
	set encounterStatus='Pending'
	where encounterStatus is null and encounterType='LAB'
	


--rollback


--commit
