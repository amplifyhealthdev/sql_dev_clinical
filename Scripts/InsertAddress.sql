
insert into ptaddress
(ptID,address1,address2,city,state,zip)
select
a.personID,
b.address1,
b.address2,
b.city,
b.state,
b.zip
from
metaDB.dbo.PersonXidentifier a
join
(
select
u.identifier,
v.attvalue as address1,
w.attvalue as address2,
x.attvalue as city,
y.attvalue as state,
z.attvalue as zip
from
(select distinct identifier,fileset_id
from metaDB.dbo.MattributeSetValue
where attribute in ('address1','address2','city','state','zip')
) u
left join metaDB.dbo.MattributeSetValue v
on u.identifier=v.identifier and u.fileset_id=v.fileset_id and v.attribute='address1'
left join metaDB.dbo.MattributeSetValue w
on u.identifier=w.identifier and u.fileset_id=w.fileset_id and w.attribute='address2'
left join metaDB.dbo.MattributeSetValue x
on u.identifier=x.identifier and u.fileset_id=x.fileset_id and x.attribute='city'
left join metaDB.dbo.MattributeSetValue y
on u.identifier=y.identifier and u.fileset_id=y.fileset_id and y.attribute='state'
left join metaDB.dbo.MattributeSetValue z
on u.identifier=z.identifier and u.fileset_id=z.fileset_id and z.attribute='zip'
) b
on a.identifier=b.identifier
