begin transaction

delete a
from
 clinical.dbo.stdbilling a
 join staging.dbo.patientsToBeUnmerged b
 on a.ptid=b.mbrID
 join clinical.dbo.patient c
 on b.mbrID2=c.ptid
 where b.unmergeComplete is null
 
delete a
from
 clinical.dbo.ptqualitymeasure a
 join staging.dbo.patientsToBeUnmerged b
 on a.ptid=b.mbrID
 join clinical.dbo.patient c
 on b.mbrID2=c.ptid
 where b.unmergeComplete is null
 
delete a
from
 clinical.dbo.ptidref2 a
 join staging.dbo.patientsToBeUnmerged b
 on a.ptid=b.mbrID
 join clinical.dbo.patient c
 on b.mbrID2=c.ptid
 where b.unmergeComplete is null
 
delete a
from
 clinical.dbo.ptcontinuousEnrollment a
 join staging.dbo.patientsToBeUnmerged b
 on a.ptid=b.mbrID
 join clinical.dbo.patient c
 on b.mbrID2=c.ptid
 where b.unmergeComplete is null
 
delete a
from
 clinical.dbo.ptemployer a
 join staging.dbo.patientsToBeUnmerged b
 on a.ptid=b.mbrID
 join clinical.dbo.patient c
 on b.mbrID2=c.ptid
 where b.unmergeComplete is null
 
delete a
from
 clinical.dbo.stdbilling_new a
 join staging.dbo.patientsToBeUnmerged b
 on a.ptid=b.mbrID
 join clinical.dbo.patient c
 on b.mbrID2=c.ptid
 where b.unmergeComplete is null
 
delete a
from
 clinical.dbo.ptrisk a
 join staging.dbo.patientsToBeUnmerged b
 on a.ptid=b.mbrID
 join clinical.dbo.patient c
 on b.mbrID2=c.ptid
 where b.unmergeComplete is null
 -- commit

 
 

--begin transaction
update a
set a.ahpatient_id=b.mbrID
from ql_patient_encounter a
join ql_patient_demographic b
on a.Patient_ID=b.individual_id
where
a.ahpatient_id!=b.mbrid and b.obsolete_date is null


update a
set a.ahpatient_id=b.mbrID
from ql_patient_encounter_diagnosis a
join ql_patient_demographic b
on a.Patient_ID=b.individual_id
where
a.ahpatient_id!=b.mbrid and b.obsolete_date is null

update a
set a.ahpatient_id=b.mbrID
from ql_patient_immunization a
join ql_patient_demographic b
on a.Patient_ID=b.individual_id
where
a.ahpatient_id!=b.mbrid and b.obsolete_date is null



update a
set a.ahpatient_id=b.mbrID
from ql_patient_vital1 a
join ql_patient_demographic b
on a.Patient_ID=b.individual_id
where
a.ahpatient_id!=b.mbrid and b.obsolete_date is null


update a
set a.ahpatient_id=b.mbrID
from ql_patient_vital3 a
join ql_patient_demographic b
on a.Patient_ID=b.individual_id
where
a.ahpatient_id!=b.mbrid and b.obsolete_date is null


update a
set a.ahpatient_id=b.mbrID
from ql_patient_lab a
join ql_patient_demographic b
on a.Patient_ID=b.individual_id
where
a.ahpatient_id!=b.mbrid and b.obsolete_date is null


update a
set a.ahpatient_id=b.mbrID
from ql_patient_referral a
join ql_patient_demographic b
on a.Patient_ID=b.individual_id
where
a.ahpatient_id!=b.mbrid and b.obsolete_date is null


update a
set a.ptid=b.mbrID
from
ql_patient_address a
join ql_patient_demographic b
on a.address_id=b.address_id
where
a.ptID!=b.mbrid and b.obsolete_date is null


update a
set a.ahpatient_id=b.mbrID
from ql_appointment a
join ql_patient_demographic b
on a.patient_id=b.patient_id
where a.ahpatient_id!=b.mbrid and b.obsolete_date is null



update a
set a.ahpatient_id=b.mbrID
from ql_document a
join ql_patient_demographic b
on a.patientid=b.patient_id
where a.ahpatient_id is null



update a
set a.ahpatient_id=b.mbrID
from ql_patient_external_data a
join ql_patient_demographic b
on a.patient_id=b.patient_id
where 
a.ahpatient_id!=b.mbrid and b.obsolete_date is null



 
 
 
 