select top 10 * from clinical.dbo.ptencounter

insert into orphanEncounter
select encounterID from clinical.dbo.ptencounter
where encounterType='History Update'

create clustered index c1OrphanEncounter on orphanEncounter(encounterID)

select top 10 a.name from clinical..sysobjects a join clinical..syscolumns b
on a.id=b.id and a.xtype='U' and b.name='encounterID'
where a.name not in ('ptencounter','tmpSFFids')
--171541

declare 
	@tab varchar(256),
	@estr nvarchar(2000)
	
declare c1 cursor for 
select a.name from clinical..sysobjects a join clinical..syscolumns b
on a.id=b.id and a.xtype='U' and b.name='encounterID'
where a.name not in ('ptencounter','tmpSFFids')
open c1
fetch c1 into @tab
while @@fetch_status=0
begin
	set @estr=N'delete a from orphanEncounter a join clinical.dbo.'+@tab+' b on a.encounterID=b.encounterID'

	exec sp_executesql @estr

	fetch c1 into @tab
end
close c1
deallocate c1

select count(*) from orphanEncounter



update a
set a.orphanType='Future Encounter'
from 
orphanEncounter a 
join clinical.dbo.ptencounter b
on a.encounterId=b.encounterID
where
a.activeEncounterID is null and
b.apptStart>getdate()

--142 --future encounter

update a
set a.orphanType='non HU Encounter Within 24 hours'
from 
orphanEncounter a 
join clinical.dbo.ptencounter b
on a.encounterId=b.encounterID
join clinical.dbo.ptencounter c
on b.ptid=c.ptid
where
a.activeEncounterID is null and
c.apptStart between dateadd(d,-1,b.apptStart) and dateadd(d,1,b.apptStart)
and c.encounterType!='History Update'
and a.orphanType is null
--2217 --future encounter

update a
set a.orphanType='Other History Update Encounter Within 24 hours'
from 
orphanEncounter a 
join clinical.dbo.ptencounter b
on a.encounterId=b.encounterID
join clinical.dbo.ptencounter c
on b.ptid=c.ptid
left join orphanEncounter d
on c.encounterID=d.encounterID
where
a.activeEncounterID is null and
c.apptStart between dateadd(d,-1,b.apptStart) and dateadd(d,1,b.apptStart)
and c.encounterType='History Update'
and d.encounterID is null
and a.orphanType is null
--9667

select * from orphanEncounter
where 
orphanType is null and activeEncounterID is null

alter table orphanEncounter
add
ptid uniqueidentifier
update a
set a.ptid=b.ptid
from orphanEncounter a
join clinical.dbo.ptencounter b
on a.encounterID=b.encounterID


select * from clinical.dbo.ptencounter
where ptid='94081D0C-87DA-4AF3-BE51-0061F1D03A93'
order by apptStart
--C5615B9A-02A2-40CE-80DF-005346477F4D
--93EFD67F-E174-46E8-BE78-D514D0DE049E
select * from ql_patient_encounter where emr2_encounter_id=1818405
select * from clinica

alter table orphanEncounter
add
patient_id int,
encounter_date datetime

update a
set a.patient_id=b.patient_id
from orphanEncounter a
join ql_patient_demographic b
on a.ptid=b.mbrID and b.obsolete_date is null

update a
set a.encounter_date=b.apptStart
from orphanEncounter a
join clinical.dbo.ptencounter b
on a.encounterID=b.encounterID

alter table orphanEncounter
add
emr2_encounter_id int

update a
set a.emr2_encounter_id=b.emr2_encounter_id
from
orphanEncounter a
join 
(

select 
    o.observation_id,
	s.patient_id,	
	eo1.placed_by_id as provider_id,
	s.service_name, 
	s.service_code,
	cast(lr.ordered as datetime) as dateordered,
	cast(s.observation_date as datetime) as observation_date,
	s.status,
	o.observation_name,
	o.observation_value,
	o.units,
	o.abnormal_flag,
	o.result_status,
	oaf.description,
	coalesce(sub.emr2_encounter_id,sub1.emr2_encounter_id) as emr2_encounter_id
	from emma.dbo.observation_set s 
inner join emma.dbo.observation o on s.observation_set_id = o.observation_set_id
join emma.dbo.lab_request lr on s.lab_request_id = lr.lab_request_id
left join 
(
select
eo.emr2_order_id,min(en.emr2_encounter_id) as emr2_encounter_id
from
emma.dbo.emr2_note en
join emma.dbo.emr2_note_entry ene
on en.emr2_note_id=ene.emr2_note_id
join emma.dbo.emr2_procedure ep
on ene.emr2_note_entry_id=ep.emr2_note_entry_id
join emma.dbo.emr2_scheduled_service ess
on ep.emr2_schedule_id=ess.emr2_schedule_id
join emma.dbo.emr2_order eo
on ess.emr2_order_id=eo.emr2_order_id 
join emma.dbo.emr2_encounter enc
on eo.placed_by_id=enc.practitioner_id
and en.emr2_encounter_id=enc.emr2_encounter_id
group by eo.emr2_order_id
) sub
on lr.emr2_order_id=sub.emr2_order_id
left join 
(
select
eo.emr2_order_id,min(en.emr2_encounter_id) as emr2_encounter_id
from
emma.dbo.emr2_note en
join emma.dbo.emr2_note_entry ene
on en.emr2_note_id=ene.emr2_note_id
join emma.dbo.emr2_procedure ep
on ene.emr2_note_entry_id=ep.emr2_note_entry_id
join emma.dbo.emr2_scheduled_service ess
on ep.emr2_schedule_id=ess.emr2_schedule_id
join emma.dbo.emr2_order eo
on ess.emr2_order_id=eo.emr2_order_id
group by eo.emr2_order_id
) sub1
on lr.emr2_order_id=sub1.emr2_order_id
left join emma.dbo.observation_abnormal_flag oaf
on o.abnormal_flag=oaf.abnormal_flag
left join emma.dbo.emr2_order eo1
on lr.emr2_order_id=eo1.emr2_order_id
where s.status='A'
) b
on a.patient_id=b.patient_id and a.encounter_date=b.dateordered


select * from orphanEncounter where activeEncounterID is null
--and orphanType is null
--and emr2_encounter_id is null
order by ptid,encounter_date


update a 
set a.activeEncounterID=b.encounterID
from orphanEncounter a
join clinical.dbo.ptlab b
on a.ptid=b.ptid and a.encounter_date=b.dateWritten
where
a.activeEncounterID is null


select count(*) from orphanEncounter a join orphanEncounter b
on a.activeEncounterID=b.encounterID
