begin transaction

--already existing
--
update a
set a.diagDate=case when isnull(
	case when isdate(b.[Onset Date])=0 then null else b.[onset Date] end,b.[Encounter Date]) < b.[Encounter Date] then
							isnull(case when isdate(b.[Onset Date])=0 then null else b.[onset Date] end,b.[Encounter Date])
							else b.[Encounter Date] end
from
clinical2.dbo.ptdiagnosis a
join problemlist b
on a.ptID=b.ahptId and a.diagCode=b.[Assessment Code]
where
a.diagDate>case when isnull(case when isdate(b.[Onset Date])=0 then null else b.[onset Date] end,b.[Encounter Date]) < b.[Encounter Date] then
							isnull(case when isdate(b.[Onset Date])=0 then null else b.[onset Date] end,b.[Encounter Date])
							else b.[Encounter Date] end
							
update a
set a.dateResolved=b.[Encounter Date]
from
clinical2.dbo.ptdiagnosis a
join problemlist b
on a.ptID=b.ahptId and a.diagCode=b.[Assessment Code]
where
a.dateResolved is null and
b.[Inactive Flag]=1

--new ones
--
insert into clinical2.dbo.ptdiagnosis
(ptID,diagCode,diagDate)
select
distinct
ahptId,[Assessment Code],case when isnull(case when isdate(a.[Onset Date])=0 then null else a.[onset Date] end,a.[Encounter Date]) < a.[Encounter Date] then
							isnull(case when isdate(a.[Onset Date])=0 then null else a.[onset Date] end,a.[Encounter Date])
							else a.[Encounter Date] end
from
problemList a
left join clinical2.dbo.ptdiagnosis b
on a.ahptId=b.ptID and a.[Assessment Code]=b.diagCode
where
b.ptID is null

