if exists(select 1 from sysobjects where xtype='P' and name='uspIncSummary')
	drop procedure uspIncSummary
go
create procedure uspIncSummary
AS
begin
	--
	--populate ptIncidentSummary
	--
	declare
		@incident_id uniqueidentifier,
		@diag varchar(3000),
		@estr nvarchar(4000),
		@db varchar(256)
			
	delete from ptIncidentSummary
	
	create table #ttab1
	(
	mbrID uniqueidentifier,
	recent_incident_date datetime,
	inpcount int,
	incident_id uniqueidentifier
	)
	create table #ttabinterim
	(
	mbrID uniqueidentifier,
	recent_incident_date datetime,
	inpcount int,
	incident_id uniqueidentifier
	)

	declare c1 cursor for
	select claimsDB from refdb.dbo.ahconfig
	where analysisType='int' and active=2
	and
	clinicaldb='clinical'

	open c1
	fetch c1 into @db
	while @@fetch_status=0
	begin
		--group by and incert for inpcount
		set @estr=N'insert into #ttabinterim
					(mbrID,recent_incident_date,inpcount)
					select
					mbrId,max(start_date),sum(case AHFacility when ''INP'' then 1 else 0 end)
					from
					'+@db+'.dbo.ahincident2
					group by mbrID'
					
		exec sp_executesql @estr
		
		
	
		set @estr=N'update a
		set a.incident_id=c.incident_id
		from
		#ttabinterim a
		join
		(select x.mbrId,x.recent_incident_date,max(y.total_covered_amt) as max_covered_amt
		from
		#ttabinterim x
		join '+@db+'.dbo.ahincident2 y
		on x.mbrID=y.mbrID and x.recent_incident_date=y.start_date
		group by x.mbrID,x.recent_incident_date) b
		on a.mbrID=b.mbrID and a.recent_incident_date=b.recent_incident_date
		join '+@db+'.dbo.ahincident2 c
		on b.mbrID=c.mbrID and b.recent_incident_date=c.start_date and b.max_covered_amt=c.total_covered_amt'
		
		exec sp_executesql @estr

	
		fetch c1 into @db
	end
	close c1
	
	insert into #ttab1
	(mbrID,recent_incident_date,inpcount)
	select
	mbrID,max(recent_incident_date),sum(inpcount)
	from #ttabinterim
	group by mbrID
	
	update a
	set a.incident_id=b.incident_id
	from #ttab1 a
	join #ttabinterim b
	on a.mbrID=b.mbrID and a.recent_incident_date=b.recent_incident_date
	
	
	insert into ptIncidentSummary
	(mbrID,recent_incident_date,inpcount,incident_id)
	select
	mbrId,recent_incident_date,inpcount,incident_id
	from #ttab1
	--update disease 
	
	update ptIncidentSummary
	set disease=dbo.udfGetDisease(mbrID)
	--build diag

	create table #diag
	(
	incident_id uniqueidentifier,
	diag_desc varchar(256),
	rownum int identity(1,1),
	slno int
	)
	create clustered index c1_diag on #diag(incident_id,diag_desc)

	open c1
	fetch c1 into @db
	while @@fetch_status=0
	begin
		--update from ahincident2 and ref table
		set @estr=N'update a
		set a.diag=b.maxDiag,a.total_covered_amt=b.total_covered_amt,a.diagShortDesc=c.shortDesc
		from
		ptIncidentSummary a
		join '+@db+'.dbo.ahincident2 b
		on a.mbrId=b.mbrID and a.incident_id=b.incident_id
		left join refdb.dbo.icd9_ama c
		on b.maxDiag=c.icd9Code '
		
		exec sp_executesql @estr

		--populate diag
		set @estr=N'insert into #diag(incident_id,diag_desc)
		select 
		distinct
		a.incident_id,
		d.[CCS CATEGORY DESCRIPTION] ccs_cat
		from
		'+@db+'.dbo.ahincident2 a
		join '+@db+'.dbo.ahincident2_data b
		on a.incident_id=b.incident_id
		join '+@db+'.dbo.stdclaims c
		on b.rowID=c.rowID
		join refdb.dbo.ahcondition d
		on c.diagcd1=d.icd9code
		join ptIncidentSummary e
		on a.incident_id=e.incident_id'
		
		exec sp_executesql @estr
		
			set @estr=N'insert into #diag
	(incident_id,diag_desc)
	select 
	distinct
	a.incident_id,
	d.[CCS CATEGORY DESCRIPTION] ccs_cat
	from
	'+@db+'.dbo.ahincident2 a
	join '+@db+'.dbo.ahincident2_data b
	on a.incident_id=b.incident_id
	join '+@db+'.dbo.stdclaims c
	on b.rowID=c.rowID
	join refdb.dbo.ahcondition d
	on c.diagcd2=d.icd9code
	left join #diag e
	on a.incident_id=e.incident_id and d.[CCS CATEGORY DESCRIPTION]=diag_desc
		join ptIncidentSummary f
		on a.incident_id=f.incident_id
	where
	e.incident_id is null'
	
			set @estr=N'insert into #diag
	(incident_id,diag_desc)
	select 
	distinct
	a.incident_id,
	d.[CCS CATEGORY DESCRIPTION] ccs_cat
	from
	'+@db+'.dbo.ahincident2 a
	join '+@db+'.dbo.ahincident2_data b
	on a.incident_id=b.incident_id
	join '+@db+'.dbo.stdclaims c
	on b.rowID=c.rowID
	join refdb.dbo.ahcondition d
	on c.diagcd3=d.icd9code
	left join #diag e
	on a.incident_id=e.incident_id and d.[CCS CATEGORY DESCRIPTION]=diag_desc
		join ptIncidentSummary f
		on a.incident_id=f.incident_id
	where
	e.incident_id is null'
	
			set @estr=N'insert into #diag
	(incident_id,diag_desc)
	select 
	distinct
	a.incident_id,
	d.[CCS CATEGORY DESCRIPTION] ccs_cat
	from
	'+@db+'.dbo.ahincident2 a
	join '+@db+'.dbo.ahincident2_data b
	on a.incident_id=b.incident_id
	join '+@db+'.dbo.stdclaims c
	on b.rowID=c.rowID
	join refdb.dbo.ahcondition d
	on c.diagcd4=d.icd9code
	left join #diag e
	on a.incident_id=e.incident_id and d.[CCS CATEGORY DESCRIPTION]=diag_desc
		join ptIncidentSummary f
		on a.incident_id=f.incident_id
	where
	e.incident_id is null'
	
		fetch c1 into @db
	end
	close c1
		

	
	
	
	declare
		@cnt int,
		@max int
		
	select @cnt=1	
	while exists (select top 1 1 from #diag where slno is null)
	begin
		update a
		set a.slno=@cnt
		from
		#diag a join (select incident_id,min(rownum) as minrow from #diag where slno is null group by incident_id) b
		on a.incident_id=b.incident_id and a.rownum=b.minrow
		
		select @cnt=@cnt+1
	end
		
	--assign
	select @cnt=1
	select @max=max(slno) from #diag
	--update diagnosis
	while @max>=@cnt
	begin			
		update a
		set a.RecentDiag=isnull(a.RecentDiag+'; ','')+b.diag_desc
		from ptIncidentSummary a
		join #diag b
		on a.incident_id=b.incident_id
		where b.slno=@cnt
		
		select @cnt=@cnt+1
	end
	
	delete from #diag
	delete from #ttab1
	
	deallocate c1
	
end

go