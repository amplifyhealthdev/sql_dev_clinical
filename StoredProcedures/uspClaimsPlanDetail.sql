USE [Clinical]
GO

/****** Object:  StoredProcedure [dbo].[uspClaimsPlanDetail]    Script Date: 12/12/2014 9:40:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[uspClaimsPlanDetail]
as
begin
	
	declare
	@estr nvarchar(2000),
	@db varchar(256)
	
	declare c1 cursor for select claimsDB from refdb.dbo.ahconfig
	where analysisType='INT'
	and active=2
	
	--delete the existing data
	
	delete a
	from ptplandetail a
	join planmeasure b
	on a.planMeasureID=b.planMeasureID
	where
	b.planMeasureSource='Claims'
	
	delete a
	from ptplanmeasure a
	join planmeasure b
	on a.planMeasureID=b.planMeasureId
	where
	b.planMeasureSource='Claims'
	open c1
	fetch c1 into @db
	
	while @@FETCH_STATUS=0
	begin
		set @estr=N'
		insert into ptplandetail
		(
		ptID,
		planMeasureID,
		ptPlanDetailScore,
		ptPlanDetailSeverity,
		ptplanDetailDate,
		totalCoveredAmt,
		totalPaidAmt
		)
		select distinct mbrID,1,100,''Red'',start_date,total_covered_amt,total_paid_amt
		from '+@db+'.dbo.ahincident2 a
		join clinical.dbo.patient b
		on a.mbrID=b.ptid
		where Reason in (''urg'')'
		
		exec sp_executesql @estr
		
		set @estr=N'
		insert into ptplandetail
		(
		ptID,
		planMeasureID,
		ptPlanDetailScore,
		ptPlanDetailSeverity,
		ptplanDetailDate,
		totalCoveredAmt,
		totalPaidAmt
		)
		select distinct mbrID,2,100,''Red'',start_date,total_covered_amt,total_paid_amt
		from '+@db+'.dbo.ahincident2 a
		join clinical.dbo.patient b
		on a.mbrID=b.ptid
		where Reason in (''er'')'
		
		exec sp_executesql @estr
		set @estr=N'
		insert into ptplandetail
		(
		ptID,
		planMeasureID,
		ptPlanDetailScore,
		ptPlanDetailSeverity,
		ptplanDetailDate,
		totalCoveredAmt,
		totalPaidAmt
		)
		select distinct mbrID,3,100,''Red'',start_date,total_covered_amt,total_paid_amt
		from '+@db+'.dbo.ahincident2 a
		join clinical.dbo.patient b
		on a.mbrID=b.ptid
		where Reason in (''urg'')'
		
		exec sp_executesql @estr
		
		set @estr=N'
		insert into ptplandetail
		(
		ptID,
		planMeasureID,
		ptPlanDetailScore,
		ptPlanDetailSeverity,
		ptplanDetailDate,
		totalCoveredAmt,
		totalPaidAmt
		)
		select distinct mbrID,4,100,''Red'',start_date,total_covered_amt,total_paid_amt
		from '+@db+'.dbo.ahincident2 a
		join clinical.dbo.patient b
		on a.mbrID=b.ptid
		where Reason in (''er'')'
		
		exec sp_executesql @estr	
		
		fetch c1 into @db
	end
	close c1
	deallocate c1
	
	
	
	--
	--Post Insert
	--
	
	update a
	set a.beforeFirstVisit=1
	from
	ptplandetail a
	join vw_patientCompleteIPencounter b
	on a.ptID=b.ptID
	where
	a.ptplanDetailDate<=isnull(b.FirstIPEncounter,a.ptPlanDetailDate)
	
	
	update a
	set a.afterFirstVisit=1
	from
	ptplandetail a
	join vw_patientCompleteIPencounter b
	on a.ptID=b.ptID
	where
	a.ptplanDetailDate>b.FirstIPEncounter
	
	update a
	set a.afterRecentVisit=1
	from
	ptplandetail a
	join vw_patientCompleteIPencounter b
	on a.ptID=b.ptID
	where
	a.ptplanDetailDate>b.MostRecentIPEncounter
	
	update ptplandetail
	set beforeFirstVisit=1
	where beforeFirstVisit=0 and afterFirstVisit=0 and afterRecentVisit=0
	
	--
	--delete irrelevant
	--
	delete a
	from ptplandetail a
	join planmeasure b
	on a.planMeasureId=b.planMeasureId
	where
	b.planMeasureCategory!='Engage'
	and
	a.beforeFirstVisit=1
	
	delete a
	from ptplandetail a
	join planmeasure b
	on a.planMeasureId=b.planMeasureId
	where
	b.planMeasureCategory='Engage'
	and
	a.afterFirstVisit=1
	
end

GO


