if exists(select 1 from sysobjects where xtype='P' and name='uspRunQAcode')
	drop procedure uspRunQAcode
go
create procedure uspRunQAcode
AS
BEGIN
declare
    @proc varchar(256),
    @estr nvarchar(2000)
    
declare @ttab table
(
pcd varchar(256)
)

insert into @ttab (pcd) select name from sysobjects where xtype='P' and name like 'uspQA%'

while exists(select 1 from @ttab)
begin
    select top 1 @proc=pcd from @ttab
    
    set @estr=N'exec '+@proc 
    
    exec sp_executesql @estr
    
    delete from @ttab where pcd=@proc
end

end
go