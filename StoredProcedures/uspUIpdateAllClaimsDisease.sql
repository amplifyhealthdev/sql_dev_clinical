create procedure uspUpdateAllClaimsDisease
AS
begin
	declare
		@db varchar(256)

	declare @tab table
	(db varchar(256))

	insert into @tab(db)
	select claimsDB from refdb.dbo.ahconfig
	where clinicalDB='clinical'
	and analysisType='int'
	and active=2
	and companyName not like '%demo%'

	while exists(select 1 from @tab)
	begin
		select top 1 @db=db from @tab

		exec uspUpdateClaimsDisease @db

		delete from @tab where db=@db
	end

end
