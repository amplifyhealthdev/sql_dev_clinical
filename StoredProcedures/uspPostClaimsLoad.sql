if exists(select 1 from sysobjects where xtype='P' and name='uspPostClaimsLoad')
	drop procedure uspPostClaimsLoad
go
create procedure uspPostClaimsLoad
AS
BEGIN

	exec uspUpdateAllClaimsDisease
	
	exec metadb.dbo.uspUpdateSeDataClaims
	
	exec uspComputeClaimsQM
	
	exec qadb.dbo.uspRunQAcode
end
go