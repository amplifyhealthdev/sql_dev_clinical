create procedure uspUpdateRptGroup
AS
begin
	--Expedia - Address record does not exist
	update a
	set a.rptGroup='Unknown'
	from
	patient a
	join ptemployer b
	on a.ptID=b.ptID
	join employer c
	on b.employerID=c.employerID and c.employerName='Expedia'
	left join ptaddress d
	on a.ptID=d.ptID
	where
	d.ptID is null
	and
	(a.rptGroup is null or a.rptGroup!='Unknown')


	--Expedia - 
	update a
	set a.rptGroup='In area'
	from
	patient a
	join ptemployer b
	on a.ptID=b.ptID
	join employer c
	on b.employerID=c.employerID and c.employerName='Expedia'
	join ptaddress d
	on a.ptID=d.ptID
	join refdb.dbo.zipusa e
	on d.zip=e.ZIP_CODE
	join ptencounter f
	on a.ptID=f.ptID
	where
	e.county in ('King', 'Kitsap', 'Pierce', 'Thurston', 'Snohomish')
	and e.STATE='WA'
	and (a.rptGroup is null or a.rptGroup !='In area')

	update a
	set a.rptGroup='In area'
	from
	patient a
	join ptemployer b
	on a.ptID=b.ptID
	join employer c
	on b.employerID=c.employerID and c.employerName='Expedia'
	join ptaddress d
	on a.ptID=d.ptID
	join refdb.dbo.zipusa e
	on d.city=e.ll_city and d.state=e.state
	join ptencounter f
	on a.ptID=f.ptID
	where
	e.county in ('King', 'Kitsap', 'Pierce', 'Thurston', 'Snohomish')
	and e.STATE='WA'
	and (a.rptGroup is null or a.rptGroup !='In area')


	update a
	set a.rptGroup='In area/not seen'
	from
	patient a
	join ptemployer b
	on a.ptID=b.ptID
	join employer c
	on b.employerID=c.employerID and c.employerName='Expedia'
	join ptaddress d
	on a.ptID=d.ptID
	join refdb.dbo.zipusa e
	on d.zip=e.ZIP_CODE
	left join ptencounter f
	on a.ptID=f.ptID
	where
	e.county in ('King', 'Kitsap', 'Pierce', 'Thurston', 'Snohomish')
	and e.STATE='WA'
	and (a.rptGroup is null or a.rptGroup !='In area/not seen')
	and f.ptID is null

	update a
	set a.rptGroup='In area/not seen'
	from
	patient a
	join ptemployer b
	on a.ptID=b.ptID
	join employer c
	on b.employerID=c.employerID and c.employerName='Expedia'
	join ptaddress d
	on a.ptID=d.ptID
	join refdb.dbo.zipusa e
	on d.city=e.ll_city and d.state=e.state
	left join ptencounter f
	on a.ptID=f.ptID
	where
	e.county in ('King', 'Kitsap', 'Pierce', 'Thurston', 'Snohomish')
	and e.STATE='WA'
	and (a.rptGroup is null or a.rptGroup !='In area/not seen')
	and f.ptID is null


	update a
	set rptGroup='Out of area'
	from
	patient a
	join ptemployer b
	on a.ptID=b.ptID
	join employer c
	on b.employerID=c.employerID and c.employerName='Expedia'
	join ptaddress d
	on a.ptid=d.ptid
	where a.rptGroup is null or a.rptGroup='Unknown'


	---employer other than expedia

	update a
	set a.rptGroup='In area'
	from
	patient a
	join ptemployer b
	on a.ptID=b.ptID
	join employer c
	on b.employerID=c.employerID and c.employerName!='Expedia'
	join ptencounter f
	on a.ptID=f.ptID
	where
	 (a.rptGroup is null or a.rptGroup !='In area')

	update a
	set a.rptGroup='In area/not seen'
	from
	patient a
	join ptemployer b
	on a.ptID=b.ptID
	join employer c
	on b.employerID=c.employerID and c.employerName!='Expedia'
	left join ptencounter f
	on a.ptID=f.ptID
	where
	 (a.rptGroup is null or a.rptGroup !='In area/not seen')
	and f.ptID is null

end
go
