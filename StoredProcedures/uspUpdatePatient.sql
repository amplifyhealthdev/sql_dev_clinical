create procedure uspUpdatePatient
AS
begin
	insert into Clinical2.dbo.patient
	(
		ptID,
		ptFirst,
		ptLast,
		ptMiddle,
		ptSuffix,
		birthDate,
		email,
		gender,
		stdrel
	)
	select  s.personID,
	metadb.dbo.udfGetOneAttValue(s.personID,'fname'),--                 rtrim(ltrim(s.[Patient First Name])),
	metadb.dbo.udfGetOneAttValue(s.personID,'lname'),--                 rtrim(ltrim(s.[Patient Last Name])),                  
	metadb.dbo.udfGetOneAttValue(s.personID,'mname'),--                 rtrim(ltrim(s.[Patient Middle Initial])),
	metadb.dbo.udfGetOneAttValue(s.personID,'suffix'),
	metadb.dbo.udfGetOneAttValue(s.personID,'dob'),--                 case when YEAR(s.[Patient Date of Birth]) > 1902 then s.[Patient Date of Birth] else NULL end, 
	metadb.dbo.udfGetOneAttValue(s.personID,'email'),--                 rtrim(ltrim(s.[Patient Email])),
	metadb.dbo.udfGetOneAttValue(s.personID,'gender'),--                 upper(left( s.[Patient Gender], 1)),  s.[Patient Deceased],
	metadb.dbo.udfGetOneAttValue(s.personID,'stdrel')
	from 
	(
	select distinct personID from PersonXidentifier where newPersonId is null
	) s
	left join Clinical2.dbo.patient c
	on s.PersonID=c.ptID
	where 
	 c.ptID is null
	
	insert into Clinical2.dbo.ptphone
	(ptId,phonenumber)
	select
	a.personID,b.attvalue
	from
	metaDB.dbo.PersonXidentifier a
	join metaDB.dbo.MattributeSetValue b
	on a.identifier=b.identifier and b.attribute='phone'
	left join Clinical2.dbo.ptphone c
	on a.PersonID=c.ptID and
	b.attvalue=c.phonenumber
	where
	c.ptid is null
	and 
	b.attvalue!=' '
	
	update a
	set a.email=c.attvalue
	from
	Clinical2.dbo.patient a
	join metaDB.dbo.PersonXidentifier b
	on a.ptID=b.PersonID
	join metaDB.dbo.MattributeSetValue c
	on b.identifier=c.identifier and c.attribute='email'
	where
	a.email is null
	and c.attvalue!=' '
	
end
go
		