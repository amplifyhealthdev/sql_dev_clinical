if exists(select 1 from sysobjects where xtype='P' and name='uspMergePatientRecords')
	drop procedure uspMergePatientRecords
go
create procedure uspMergePatientRecords
	@newPt uniqueidentifier,
	@oldPt uniqueidentifier
AS
begin
	declare @error int
	begin transaction
	--merge old and new patient records
	--
	--MetaDB changes
	update metaDB.dbo.PersonXIdentifier
	set newPersonID=@newPt
	where PersonId=@oldPt and newPersonId is null
	
	set @error=@@error
	if @error!=0
		goto Failure
		
	insert into metaDB.dbo.PersonXIdentifier
	(PersonID,identifier,rlogic)
	select
	@newPT,identifier,'Patient Records Merged from '+cast(@OldPt as varchar(36))
	from
	metaDB.dbo.PersonXIdentifier 
	where PersonID=@oldPt 
	
	set @error=@@error
	if @error!=0
		goto Failure
	--Clinical DB Changes
	--
	--ptAddress
	--if old has data and new does not have the same, copy
	--else, delete old
	--
	if exists(select 1 from ptaddress where ptID=@oldPt)
	begin
		if not exists(select 1 from ptaddress a join ptaddress b on isnull(a.address1,'X')=isnull(b.address1,'X')
					and isnull(a.address2,'X')=isnull(b.address2,'X') and isnull(a.city,'X')=isnull(b.city,'X')
					and isnull(a.state,'X')=isnull(b.state,'X') and isnull(a.zip,'X')=isnull(b.zip,'X') 
					where a.ptId=@oldPt and b.ptID=@newPt)
		begin			
			insert into ptaddress
			(ptID,address1,address2,city,state,zip)
			select 
			@newPt,a.address1,a.address2,a.city,a.state,a.zip
			from ptaddress a left join ptaddress b on isnull(a.address1,'X')=isnull(b.address1,'X')
			and isnull(a.address2,'X')=isnull(b.address2,'X') and isnull(a.city,'X')=isnull(b.city,'X')
			and isnull(a.state,'X')=isnull(b.state,'X') and isnull(a.zip,'X')=isnull(b.zip,'X') and b.ptID=@newPt
			where a.ptId=@oldPt 
			and b.ptId is null
	
			set @error=@@error
			if @error!=0
				goto Failure
		end
		delete from ptaddress
		where ptID=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
			
	end
	--
	--ptAllergy
	--
	if exists(select 1 from ptallergy where ptId=@oldPt)
	begin
		if not exists(select 1 from ptallergy a join ptallergy b on isnull(a.allergyID,0)=isnull(b.allergyId,0)
				and isnull(a.reaction,'X')=isnull(b.reaction,'X') and isnull(a.allergyStatus,'X')=isnull(b.allergyStatus,'X')
				where a.ptId=@oldPt and b.ptId=@newPt)
		begin		
			insert into ptallergy
			(ptId,allergyID,reaction,allergyStatus)
			select @newPt,a.allergyId,a.reaction,a.allergyStatus
			 from ptallergy a left join ptallergy b on isnull(a.allergyID,0)=isnull(b.allergyId,0)
				and isnull(a.reaction,'X')=isnull(b.reaction,'X') and isnull(a.allergyStatus,'X')=isnull(b.allergyStatus,'X')
				and b.ptId=@newPt
				where a.ptId=@oldPt  and b.ptId is null
	
			set @error=@@error
			if @error!=0
				goto Failure
		end				
		delete from ptallergy
		where ptID=@oldPt	
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	--
	--ptattribute
	--
	if exists(select 1 from ptattribute where ptID=@oldPt)
	begin
		if not exists(select 1 from ptattribute a join ptattribute b on a.attribute_name=b.attribute_name
					and isnull(a.attribute_value,'X')=isnull(b.attribute_value,'X')
					where a.ptId=@oldPt and b.ptId=@newPt)
		begin		
			insert into ptattribute
			(ptId,attribute_name,attribute_value)
			select
			@newPt,a.attribute_name,a.attribute_value
			from ptattribute a left join ptattribute b on a.attribute_name=b.attribute_name
					and isnull(a.attribute_value,'X')=isnull(b.attribute_value,'X')
					and b.ptId=@newPt		
					where a.ptId=@oldPt 
					and b.ptId is null
	
			set @error=@@error
			if @error!=0
				goto Failure
		end				
		delete from ptattribute
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	
	--
	--ptdiagnosis
	--
	if exists(select 1 from ptdiagnosis where ptID=@oldPt)
	begin
		update a
		set a.diagDate=b.diagDate
		from
		ptdiagnosis a,ptdiagnosis b
		where a.ptID=@newPt 
		and b.ptId=@oldPt
		and a.diagCode=b.diagCode
		and b.diagDate<a.diagDate
	
		set @error=@@error
		if @error!=0
			goto Failure
					
		update a
		set a.ptId=@newPt
		from
		ptdiagnosis a
		left join ptdiagnosis b
		on a.diagcode=b.diagcode and a.diagDate=b.diagDate
		and b.ptId=@newPt
		where
		a.ptId=@oldPt
		and b.ptId is null
	
		set @error=@@error
		if @error!=0
			goto Failure
		
		delete from ptDiagnosis
		where ptID=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	--
	--ptdisease
	--
	if exists(select 1 from ptdisease where ptID=@oldPt)
	begin
		update a
		set a.inClaims=1
		from
		ptdisease a join ptdisease b
		on a.disease=b.disease 
		where a.ptId=@newPt and a.inClaims is null
		and b.ptId=@oldPt and b.inClaims=1
	
		set @error=@@error
		if @error!=0
			goto Failure
		
		update a
		set a.inEHR=1
		from
		ptdisease a join ptdisease b
		on a.disease=b.disease 
		where a.ptId=@newPt and a.inEHR is null
		and b.ptId=@oldPt and b.inEHR=1
	
		set @error=@@error
		if @error!=0
			goto Failure
				
		update a
		set a.inSurvey=1
		from
		ptdisease a join ptdisease b
		on a.disease=b.disease 
		where a.ptId=@newPt and a.inEHR is null
		and b.ptId=@oldPt and b.inSurvey=1
	
		set @error=@@error
		if @error!=0
			goto Failure
		
		insert into ptdisease
		(ptId,disease,inClaims,inEHR,inSurvey)
		select
		@newPt,a.disease,a.inClaims,a.inEHR,a.inSurvey
		from
		ptdisease a
		left join ptdisease b
		on a.disease=b.disease
		and b.ptID=@newPt
		where
		a.ptID=@oldPt and b.ptId is null
	
		set @error=@@error
		if @error!=0
			goto Failure
		
		delete from ptdisease
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	
	--
	--ptemployer
	if not exists(select 1 from ptemployer where ptID=@newPT)
	begin
		update ptemployer
		set ptId=@newPt
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end		
	else
	begin
		delete from ptemployer
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end		

	--ptImmunization
	
	if exists(select 1 from ptimmunization where ptId=@oldPt)
	begin
		update a
		set a.ptId=@newPt
		from
		ptImmunization a
		left join ptImmunization b
		on a.immunType=b.immunType
		and a.immunDate=b.immunDate
		and b.ptId=@newPt
		where
		a.ptId=@oldPt and b.ptId is null
	
		set @error=@@error
		if @error!=0
			goto Failure
		
		delete from ptImmunization
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	
	--ptlab
	--
	if exists(select 1 from ptlab where ptId=@oldPt)
	begin
		update a
		set a.ptId=@newPt
		from 
		ptLab a
		left join ptLab b
		on a.testId=b.testId and a.labName=b.labName 
		and isnull(a.labAttribute,'X')=isnull(b.labAttribute,'X')
		and a.datePerformed=b.datePerformed
		and a.labValue=b.labValue
		and a.highLowNormal=b.highLowNormal
		and b.ptId=@newPt
		where
		a.ptId=@oldPt and b.ptId is null
	
		set @error=@@error
		if @error!=0
			goto Failure
		
		delete from ptlab
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	
	--ptpayer
	--no need to do now. no records
	--
	if exists(select 1 from ptphone where ptId=@oldPt)
	begin
		update a
		set a.ptId=@newPt
		from
		ptphone a
		left join ptphone b
		on a.phonenumber=b.phonenumber
		and b.ptId=@newPt
		where
		a.ptId=@oldPt and b.ptId is null
	
		set @error=@@error
		if @error!=0
			goto Failure
	
		delete from ptphone
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	
	--ptreferral
	if exists(select 1 from ptreferral where ptId=@oldPt)
	begin
		update a
		set a.ptId=@newPt
		from
		ptreferral a
		left join ptreferral b
		on a.dateReferred=b.dateReferred
		and a.referralStatus=b.referralStatus
		and a.referralSpecialty=b.referralSpecialty
		and a.referralType=b.referralType
		and b.ptId=@newPt
		where
		a.ptId=@oldPt and b.ptId is null
	
		set @error=@@error
		if @error!=0
			goto Failure
	
		delete from ptreferral
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	
	--ptvital
	if exists(select 1 from ptvital where ptId=@oldPt)
	begin
		update a
		set a.ptId=@newPt
		from 
		ptvital a
		left join ptvital b
		on a.encounterDate=b.encounterDate
		and isnull(a.weight,0)=isnull(b.weight,0)
		and isnull(a.height,0)=isnull(b.height,0)
		and isnull(a.diastolicBP,0)=isnull(b.diastolicBP,0)
		and isnull(a.systolicBP,0)=isnull(b.systolicBP,0)
		and a.vitalDate=b.vitalDate
		and isnull(a.bmi,0)=isnull(b.bmi,0)
		and b.ptId=@newPt
		where
		a.ptId=@oldPt and b.ptId is null
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	
	--
	--ptencounter
	--
	if exists(select 1 from ptencounter where ptId=@oldPt)
	begin
		update a
		set a.ptId=@newPt
		from
		ptencounter a
		left join ptencounter b
		on a.encounterType=b.encounterType 
		and isnull(a.encounterStatus,'X')=isnull(b.encounterStatus,'X')
		and isnull(a.apptStart,'2000/01/01')=isnull(b.apptStart,'2000/01/01')
		and isnull(a.apptEnd,'2000/01/01')=isnull(b.apptEnd,'2000/01/01')
		and isnull(a.apptArrive,'2000/01/01')=isnull(b.apptArrive,'2000/01/01')
		and isnull(a.apptDepart,'2000/01/01')=isnull(b.apptDepart,'2000/01/01')
		and isnull(a.apptLocation,'X')=isnull(b.apptLocation,'X')
		and isnull(a.direction,'X')=isnull(b.direction,'X')
		and b.ptId=@newPt
		where
		a.ptId=@oldPt and b.ptId is null
	
		set @error=@@error
		if @error!=0
			goto Failure
		
		delete from ptencounter
		where ptId=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end

	--stdbilling
	--
	if exists(select 1 from stdbilling where ptID=@oldPt)
	begin
		update a
		set a.ptId=@newPt
		from stdbilling a
		left join stdbilling b
		on a.monthx=b.monthx
		and a.yearx=b.yearx	
		and b.ptID=@newPt
		where 
		a.ptID=@oldPt and b.ptID is null
	
		set @error=@@error
		if @error!=0
			goto Failure
		
		delete from stdbilling
		where ptID=@oldPt
	
		set @error=@@error
		if @error!=0
			goto Failure
	end
	--
	--patient
	--
	update a
	set a.isDPCPatient=1
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.isDPCPatient is null and b.isDPCPatient=1
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	update a
	set a.ptStatus='Active'
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.ptStatus is null and b.ptStatus='Active'
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	update a
	set a.email=b.email
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.email is null and b.email is not null
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	update a
	set a.gender=b.gender
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.gender is null and b.gender is not null
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	update a
	set a.SSN=b.SSN
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.SSN is null and b.SSN is not null
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	update a
	set a.pcpId=b.pcpId
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.pcpId is null and b.pcpId is not null
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	update a
	set a.MRN=b.MRN
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.MRN is null and b.MRN is not null
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	update a
	set a.rptGroup=b.rptGroup
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.rptGroup is null and b.rptGroup is not null
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	
	update a
	set a.stdrel=b.stdrel
	from patient a,patient b
	where a.ptId=@newPT and b.ptId=@oldPt
	and a.stdrel is null and b.stdrel is not null
	
	set @error=@@error
	if @error!=0
		goto Failure
		
	insert into DeletedPatient
	(ptID,ptFirst,ptLast,isDPCPatient,ptMiddle,ptTitle,ptSuffix,ptStatus,birthDate,email,gender,deceased,dateOfDeath,SSN,pcpID,MRN,loadID,rptGroup,DeletedReason,newPtID)
	select
	ptID,ptFirst,ptLast,isDPCPatient,ptMiddle,ptTitle,ptSuffix,ptStatus,birthDate,email,gender,deceased,dateOfDeath,SSN,pcpID,MRN,loadID,rptGroup,'Merged with '+CAST(@newPT as varchar(36)),@newPt
	from patient
	where ptid=@oldPt
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	delete from ptqualitymeasure
	where ptID=@oldPt
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	delete from patient
	where ptID=@oldPt
	
	set @error=@@error
	if @error!=0
		goto Failure
	
	goto Success
	
	Failure:
		rollback
		return
	Success:
		commit
end
			