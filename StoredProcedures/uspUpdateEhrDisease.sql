create procedure uspUpdateEhrDisease
AS
begin
	insert into ptdisease
	(ptid, disease, inehr)
	select distinct a.ptid, 'Diabetes', 1 
	from ptdiagnosis a
	left join ptdisease b
	on a.ptID=b.ptID and b.disease='Diabetes'
	where 
	b.ptID is null
	and
	(
	a.diagcode = '250'  or
	(a.diagcode between '250.00' and '250.99') or a.diagcode in ('357.2', '362.0', '366.42', '648.0')
	)
	
	insert into ptdisease (ptid, disease, inehr)
	select  distinct a.ptID, 'COPD/Asthma', 1   
	from ptdiagnosis a
	left join ptdisease b
	on a.ptID=b.ptID and b.disease='COPD/Asthma'
	where 
	b.ptID is null
	and
	(
	a.diagcode in ('496.00', '491.20', '491.21', '491.22', '491.9', '492.8', '493.00', '493.01', '493.02', 
	'493.10', '493.11', '493.12', '493.20', '493.21', '493.22', '493.81', '493.82', '493.90', '493.91', '493.92', 
	'492.0', '492.8', '506.4', '518.2', '518.1', '518.83') 
	)
	
	
	insert into ptdisease (ptid, disease, inehr)
	select  distinct a.ptID,  'Obesity', 1  
	from ptdiagnosis a
	left join ptdisease b
	on a.ptID=b.ptID and b.disease='Obesity'
	where 
	b.ptID is null
	and
	a.diagcode between '278.00' and '278.99' 
	
	
	insert into ptdisease (ptid, disease, inehr)
	select  distinct a.ptid,  'Kidney Disease', 1 
	from ptdiagnosis  a
	left join ptdisease b
	on a.ptID=b.ptID and b.disease='Kidney Disease'
	where 
	b.ptID is null
	and
	a.diagcode between '585.00' and '589.99' 
	
	insert into ptdisease (ptid, disease, inehr)
	select  distinct a.ptID,  'Hypertension' ,1 from 
	ptdiagnosis  a
	left join ptdisease b
	on a.ptID=b.ptID and b.disease='Hypertension'
	where 
	b.ptId is null
	and
	a.diagcode between '401.00' and '404.99' 
	
	---- now update the rows that were there, but not marked as ehr
	update a
	set a.inEHR=1
	from
	ptdisease a
	join ptdiagnosis b
	on a.ptId=b.ptID
	where
	a.inEHR is null and
	(  
	b.diagcode = '250'  or
	(b.diagcode between '250.00' and '250.99') or b.diagcode in ('357.2', '362.0', '366.42', '648.0'))

	and a.disease = 'Diabetes' 
	
	update a
	set a.inEHR=1
	from
	ptdisease a
	join ptdiagnosis b
	on a.ptId=b.ptID
	where
	a.inEHR is null and
	b.diagcode in ('496.00', '491.20', '491.21', '491.22', '491.9', '492.8', '493.00', '493.01', '493.02', 
	'493.10', '493.11', '493.12', '493.20', '493.21', '493.22', '493.81', '493.82', '493.90', '493.91', '493.92', 
	'492.0', '492.8', '506.4', '518.2', '518.1', '518.83') 
	and a.disease = 'COPD/Asthma'	
	
	update a
	set a.inEHR=1
	from
	ptdisease a
	join ptdiagnosis b
	on a.ptId=b.ptID
	where
	a.inEHR is null and
	b.diagcode  between '278.00' and '278.99'
	and a.disease = 'Obesity'	
		  
	update a
	set a.inEHR=1
	from
	ptdisease a
	join ptdiagnosis b
	on a.ptId=b.ptID
	where
	a.inEHR is null and
	b.diagcode between '585.00' and '589.99'
	and a.disease = 'Kidney Disease'	
	
	update a
	set a.inEHR=1
	from
	ptdisease a
	join ptdiagnosis b
	on a.ptId=b.ptID
	where
	a.inEHR is null and
	b.diagcode between '401.00' and '404.99' 
	and a.disease = 'Hypertension'	
	
end
