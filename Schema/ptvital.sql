
CREATE TABLE dbo.ptvital(
	vitalID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
	encounterID uniqueidentifier NOT NULL REFERENCES ptencounter(encounterID),
	encounterDate datetime NULL,
	weight decimal(7, 2) NULL,
	height decimal(7, 2) NULL,
	diastolicBP int NULL,
	systolicBP int NULL,
	pulse int NULL,
	glucose int NULL,
	vitalDate datetime NULL,
	bmi float NULL,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go