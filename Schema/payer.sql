
CREATE TABLE dbo.payer(
	payerID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED ,
	payerName varchar(100) NULL,
	payerType varchar(10) NULL,
	address1 varchar(100) NULL,
	address2 varchar(100) NULL,
	city varchar(150) NULL,
	zip varchar(9) NULL,
	state varchar(5) NULL,
	payerExtZip varchar(9) NULL,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
