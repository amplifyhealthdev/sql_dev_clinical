create table dbo.ptmapcols(
	ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
	colName varchar(256),
	colVal varchar(256),
	src_type varchar(40),
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go