
CREATE TABLE dbo.ptdiagnosis(
	diagnosisID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NULL REFERENCES patient(ptID),
	diagCode varchar(10) NULL,
	diagDate datetime NULL,
	diagStatus varchar(10) NULL,
	dateResolved datetime NULL,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
