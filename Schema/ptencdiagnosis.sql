
CREATE TABLE dbo.ptencdiagnosis(
	encdiagID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED,
	encounterID uniqueidentifier NOT NULL REFERENCES ptencounter(encounterID),
	diag varchar(10) NULL,
	loadID int NOT NULL,	
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
	