
CREATE TABLE dbo.ptlab(
	ptLabID uniqueidentifier NOT NULL  default newid() PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
	provID uniqueidentifier NOT NULL REFERENCES provider(provID),
	encounterID uniqueidentifier NOT NULL REFERENCES ptencounter(encounterID),
	testID int NULL,
	labName varchar(100) NULL,
	labAttribute varchar(100) NULL,
	dateWritten datetime NULL,
	datePerformed datetime NULL,
	dateResult datetime NULL,
	labValue varchar(200) NULL,
	labResult varchar(500) NULL,
	highLowNormal varchar(10) NULL,
	labInstructions varchar(50) NULL,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
