
CREATE TABLE dbo.ptimmunization(
	immunID uniqueidentifier NOT NULL  default newid() PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NULL REFERENCES patient(ptID),
	immunType varchar(50) NULL,
	immunDate datetime NULL,
	provID uniqueidentifier NULL REFERENCES provider(provID),
	dosage varchar(50) NULL,
	doseNumber int NULL,
	encounterID uniqueidentifier NULL,
	loadID int NOT NULL,	
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go

