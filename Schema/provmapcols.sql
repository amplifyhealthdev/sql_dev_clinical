create table dbo.provmapcols(
	provID uniqueidentifier NOT NULL REFERENCES provider(provID),
	colName varchar(256),
	colVal varchar(256),
	src_type varchar(40),
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go	