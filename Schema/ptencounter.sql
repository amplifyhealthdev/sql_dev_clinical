

CREATE TABLE dbo.ptencounter(
	encounterID uniqueidentifier NOT NULL  default newid() PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NULL REFERENCES patient(ptID),
	provID uniqueidentifier NULL REFERENCES provider(provID),
	provType varchar(20) NULL,
	provStatus varchar(50) NULL,
	apptBooked datetime NULL,
	apptStart datetime NULL,
	apptEnd datetime NULL,
	apptArrive datetime NULL,
	apptDepart datetime NULL,
	apptReason varchar(50) NULL,
	apptLocation varchar(100) NULL,
	direction varchar(10) NULL,
	nMatches int NULL,
	loadID int NOT NULL,	
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
