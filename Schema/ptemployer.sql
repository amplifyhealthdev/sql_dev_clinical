
CREATE TABLE dbo.ptemployer(
	ptEmplId uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED ,
	ptID uniqueidentifier NULL REFERENCES patient(ptID),
	employerID uniqueidentifier NULL REFERENCES employer(employerID),
	eligStart datetime NULL,
	eligEnd datetime NULL,
	loadid int NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
	