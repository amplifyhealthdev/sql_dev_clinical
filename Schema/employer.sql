
CREATE TABLE dbo.employer(
	employerID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED ,
	employerName varchar(100) NULL,
	employerType varchar(10) NULL,
	address1 varchar(100) NULL,
	address2 varchar(100) NULL,
	city varchar(150) NULL,
	zip varchar(10) NULL,
	state varchar(5) NULL,
	ExtZip varchar(10) NULL,
	loadid int NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go

	
