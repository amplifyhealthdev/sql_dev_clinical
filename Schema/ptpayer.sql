
CREATE TABLE dbo.ptpayer(
	ptPayerID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
	payerID uniqueidentifier NOT NULL REFERENCES payer(payerID),
	payerRank int NULL,
	ptAccountNum varchar(30) NULL,
	groupNum varchar(20) NULL,
	responsPartyID int NULL,
	paymentAssigned varchar(4) NULL,
	copayReq varchar(4) NULL,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go