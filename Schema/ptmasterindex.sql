CREATE TABLE dbo.ptmasterindex(
	ptmasterindexID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NOT NULL,
	srcptid varchar(256),
	src_type varchar(40),
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go

CREATE TABLE dbo.ptmasterindexcol(
	ptmasterindexID uniqueidentifier REFERENCES ptmasterindex(ptmasterindexID),
	colName varchar(256),
	colVal varchar(256),
	colOrder int,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
	

/*	
	name varchar(200) NULL,
	status varchar(100) NULL,
	phone varchar(100) NULL,
	cell varchar(100) NULL,
	email varchar(100) NULL,
	employerID uniqueidentifier NULL,
	first varchar(50) NULL,
	last varchar(50) NULL,
	dob int NULL,
	gender varchar(10) NULL,
	elig_subscriber varchar(30) NULL,
	elig_depid varchar(20) NULL,
	elig_rel varchar(20) NULL,
	elig_zip varchar(10) NULL,
	netsuiteid varchar(100) NULL,
	origSource varchar(20) NULL,
	state varchar(5) NULL,
	empID uniqueidentifier NULL,
	stdrel varchar(5) NULL,
	ecwid_monavie varchar(20) NULL,
	ecwid_serigraph varchar(20) NULL,
	ecwid_enterprise varchar(20) NULL,
	matchProblems varchar(50) NULL,
	loadid int NOT NULL,
	nsgender varchar(20) NULL,
	nsdob int NULL,
	nsstartdate int NULL,
	nsenddate int NULL,
	middle varchar(50) NULL,
	*/