CREATE TABLE dbo.ptreferral(
	referralID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
	referrerID uniqueidentifier NULL REFERENCES provider(provID),
	referredToID uniqueidentifier NULL REFERENCES provider(provID),
	dateReferred datetime NULL,
	referralAppt datetime NULL,
	referralStatus varchar(20) NULL,
	referralSpecialty varchar(50) NULL,
	referralType varchar(10) NULL,
	referralReason varchar(200) NULL,
	encounterID uniqueidentifier NULL REFERENCES ptencounter(encounterID),
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go