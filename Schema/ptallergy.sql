
CREATE TABLE dbo.ptallergy(
	ptallergyID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED ,
	ptID uniqueidentifier NULL REFERENCES patient(ptID),
	allergyID int NULL,
	reaction varchar(50) NULL,
	allergyStatus varchar(10) NULL,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
