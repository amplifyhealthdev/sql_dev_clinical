insert into AuditDatabase
(DBName,Description,ServerName)
values
('Clinical2','clinical2 database',@@SERVERNAME)



insert into AuditTable
(ServerName,DBName,TableName,IsAudited)
select @@SERVERNAME,'Clinical2',a.name,1 from Clinical2.dbo.sysobjects a
join Clinical2.sys.columns b
on a.id=b.object_id
where xtype='U'
and a.name not in ('sysdiagrams')
and b.is_rowguidcol=1

insert into AuditColumn
(ServerName,DBName,TableId,ColumnName,IsAudited)
select
@@SERVERNAME,'Clinical2',c.TableId,a.name,1
from
Clinical2.dbo.syscolumns a
join Clinical2.dbo.sysobjects b
on a.id=b.id 
join AuditTable c
on b.name=c.TableName
join Clinical2.dbo.systypes d
on a.xusertype=d.xusertype
where
b.xtype='U' and
d.name not in ('image','varbinary','text','ntext','geometry','geography','timestamp')

