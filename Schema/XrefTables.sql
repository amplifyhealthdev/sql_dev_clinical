create table patientXref
(
ptID uniqueidentifier,
srcptid varchar(40)
)
go
create table providerXref
(
provID uniqueidentifier,
srcprovid varchar(40)
)
go
create table encounterXref
(
encounterID uniqueidentifier,
srcencounterid varchar(40)
)
go