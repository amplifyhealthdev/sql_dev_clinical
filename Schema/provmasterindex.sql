drop table provmasterindexcol
drop table provmasterindex

CREATE TABLE dbo.provmasterindex(
	provmasterindexID uniqueidentifier NOT NULL default newid() PRIMARY KEY CLUSTERED,
	provID uniqueidentifier NOT NULL ,
	srcprovid varchar(256),
	src_type varchar(40),
	match_type varchar(256),
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go

CREATE TABLE dbo.provmasterindexcol(
	provmasterindexID uniqueidentifier REFERENCES provmasterindex(provmasterindexID),
	colName varchar(256),
	colVal varchar(256),
	colOrder int,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go


create table colmatch(
	record_type varchar(40),
	colName varchar(256),
	match_type varchar(40),
	match_score int
)

create table srcmatch(
	source_type varchar(40),
	record_type varchar(40),
	match_pct_threshold decimal(10,2)
)	
/*	
	name varchar(200) NULL,
	status varchar(100) NULL,
	phone varchar(100) NULL,
	cell varchar(100) NULL,
	email varchar(100) NULL,
	first varchar(50) NULL,
	last varchar(50) NULL,
	dob int NULL,
	gender varchar(10) NULL,
	zip varchar(10) NULL,
	ecwid_monavie varchar(20) NULL,
	netsuiteid varchar(100) NULL,
	origSource varchar(20) NULL,
	loadid int NOT NULL,
	ecwid_serigraph varchar(20) NULL,
	ecwid_enterprise varchar(20) NULL,
	
	
	*/