
CREATE TABLE dbo.patient(
	ptID uniqueidentifier  NOT NULL default newid() PRIMARY KEY CLUSTERED ,
	ptFirst varchar(100) NULL,
	ptLast varchar(100) NULL,
	isDPCPatient int NULL,
	ptMiddle varchar(50) NULL,
	ptTitle varchar(10) NULL,
	ptSuffix varchar(10) NULL,
	ptStatus varchar(10) NULL,
	birthDate date NULL,
	email varchar(100) NULL,
	gender varchar(7) NULL,
	deceased varchar(5) NULL,
	dateOfDeath datetime NULL,
	SSN varchar(10) NULL,
	pcpID uniqueidentifier NULL REFERENCES provider(provID),
	MRN varchar(20) NULL,
	loadID int NULL,
	rptGroup varchar(20) NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
create table ptaddress
(
ptaddressID uniqueidentifier default newid() PRIMARY KEY CLUSTERED,
ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
address1 varchar(100) NULL,
address2 varchar(100) NULL,
city varchar(100) NULL,
zip varchar(5) NULL,
state varchar(5) NULL,
isPrimary bit default 0,
create_date datetime default getdate(),
obsolete_date datetime,
audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
create table phonetyperef
(
phonetype varchar(20) NOT NULL PRIMARY KEY CLUSTERED,
create_date datetime default getdate(),
obsolete_date datetime,
audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
create table ptphone
(
ptPhoneID uniqueidentifier default newid() PRIMARY KEY CLUSTERED,
ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
phonenumber varchar(20),
phonetype varchar(20) REFERENCES phonetyperef(phonetype),
isPrimary bit default 0,
create_date datetime default getdate(),
obsolete_date datetime,
audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
create table ptattributeref
(
attribute_name varchar(256) NOT NULL PRIMARY KEY CLUSTERED,
create_date datetime default getdate(),
obsolete_date datetime,
audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
create table ptattribute
(
ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
attribute_name varchar(256) NOT NULL REFERENCES ptattributeref(attribute_name),
attribute_value varchar(256),
create_date datetime default getdate(),
obsolete_date datetime,
audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go



/*
	phone varchar(100) NULL,
	cell varchar(100) NULL,
	education varchar(4) NULL,
	race varchar(50) NULL,
	ptLang varchar(50) NULL,
	maritalStatus varchar(10) NULL,
	homeStatus varchar(10) NULL,
	employmentStatus varchar(10) NULL,
	studentStatus varchar(10) NULL,
	hearingStatus varchar(10) NULL,
	visualStatus varchar(10) NULL,
	memberType varchar(20) NULL,
	ehrID varchar(20) NULL,
	isScholarship int NULL,

