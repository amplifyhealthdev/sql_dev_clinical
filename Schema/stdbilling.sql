CREATE TABLE dbo.stdbilling(
	ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
	yearx varchar(50) NULL,
	monthx varchar(50) NULL,
	billed int NULL,
	isscholarship int NULL,
	loadID int NOT NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go
