CREATE TABLE dbo.ptqualitymeasure(
	ptPrevMeasureID int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	ptID uniqueidentifier NOT NULL REFERENCES patient(ptID),
	measureID int NOT NULL,
	measureName varchar(30) NULL,
	dueDate datetime NULL,
	doneDate datetime NULL,
	metMeasure int NULL,
	updatedDate datetime NULL,
	monthX int NULL,
	yearX int NULL,
	create_date datetime default getdate(),
	obsolete_date datetime,
	audit_id uniqueidentifier NOT NULL default newid() ROWGUIDCOL
)
go